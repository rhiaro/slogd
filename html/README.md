# Slog'd: **S**emantic B**log** from Mark**d**own

With Slog'd you will be able to:

* Write posts in markdown
* Add tags and categories
* Upload them to the server (somehow)

Then when someone loads your blog:

* Any new MD is parsed and turned into triples and put in the store.
* They can browse through your frontend, with posts sorted however you like, within the layout you created.

Or something like that.  I don't yet have a real plan.

Doing it in PHP and MySQL so it'll be more reusable by people who use generic shared hosting.

## Decisions

- [x] Store post content as markdown in the store, and convert to html on the way out.
- [x] Ditched types (BlogPost) in URLs and no camelCasing.

## TODO

### Fixes

- [ ] Fix use of as2:location (should not be a string)

### Rendering

- [ ] Create stream of everything
- [x] List posts sorted by todo/doing/done.
- [x] Display post content and metadata on individual post pages.
- [x] Make things look nice.
- [x] Make all post-blogger posts in the triplestore markdown instead of shitty html.
- [ ] Exclude drafts from display (remove pubdate and don't display posts with no pubdate)
- [ ] Display comments already in store.
- [x] Filtering by tag.
- [x] Filtering by list.
- [ ] Filtering by date (yyyy/mm).
  - [~] Modify .htaccess (need to handle trailing slash).
  - [ ] Get post lists by date ranges.
- [ ] Output RDF when requested.
- [ ] Display locations on posts / display checkins.
- [x] Display SPARQL queries used to get each page / aggregate of posts.
- [ ] Fix markup and general messiness in posts.

### URIs / Slugs

- [x] Lose the date-string from unique id part of URIs.
- [ ] Change blogger comment URIs to use the post-### part of bloggerid instead of date and content string.

### Interactions

- [ ] Accept comments by URI.
- [ ] Accept comments by input.
- [x] Receive Webmentions.
- [ ] Send Webmentions.
- [ ] Display interactions.

### Ingesting posts

- [x] Convert all Blogger posts to triples and store.
- [x] Parse markdown posts on queue (by blog last updated date) directory,  and store in triplestore. 
  - [x] Parse
  - [x] Store
- [ ] Accept email posts, parse and store.
- [x] Micropub endpoint
- [ ] Accept SMS posts via twilio?
- [ ] Publish ActivityStreams JSON.
- [x] Update sorter to allow removal of tags and lists.

**Note to self:** `LOAD <../etc/allposts.ttl>`

### POSSE

- [ ] Twitter

### PESOS

- [ ] Runkeeper
- [ ] SleepAsAndroid
- [ ] Bitbucket / Github
- [ ] Lastfm

### Legacy

- [ ] Redirect old blogger URIs to the new posts.

### Philosophising

Should there be a URI difference between 'raw' posts and page with post plus other stuff on (like comments). (/BlogPost/.. vs /blog/ or /post/?). Maybe post URIs are to the raw md in /posts/ and the rendered version is whatever it is.

Update Nov/Dec '14: 

- [ ] 'raw' post URIs: `blog.rhiaro.co.uk/unique-post-slug` returns triples where that URI is the subject.
- [x] rhiaro.co.uk/post/unique-post-slug.md will give you md in browser?
- [x] blog itself works with `rhiaro.co.uk/yyyy/mm/unique-post-slug` in browser, to show rendered view including comments, webmentions etc. 
  - [~] But people are surely going to use this with in-reply-to? Maybe I should just sameAs them.....
  - [x] Need some kind of 'renderedAt' property to link the two? Presumably there's already something I can use.. foaf:primaryTopic?
  - [ ] Maybe rel="source" plus conneg is what I need here...

## TO FIX

- [x] sioc:Category used as a property OOPS
- [ ] URIs to unmerge: `http://blog.rhiaro.co.uk/notes-odd-2`
- [ ] Images aren't rendered. Switch from PHPMarkdown to Parsedown?

## Further future

- [ ] Restructure everything so SPARQL queries are stored in a directory and a templating language is used to fill in variables, rather than hardcoding them in the lib.
