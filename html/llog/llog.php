<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('../lib/storesetup.php');
require_once('../lib/Slogd_Renderer.php');

if(isset($_GET['slug']) && $_GET['slug'] != null){ 
    $uri = "http://llog.rhiaro.co.uk/".$_GET['slug'];
    $q = "DESCRIBE <$uri>";
    
    $res = $ep->query($q);
    if(!$ep->getErrors()){
        
        if(isset($_GET['format']) && $_GET['format'] != "md"){
            if($_GET['format'] == "xml"){
                $ser = ARC2::getRDFXMLSerializer();
            }elseif($_GET['format'] == "json"){
                $ser = ARC2::getRDFJSONSerializer();
            }elseif($_GET['format'] == "html"){
                echo "<pre>";
                echo htmlentities($res['result'][$uri]['http://rdfs.org/sioc/types#content'][0]['value']);
                echo "</pre>";
                exit;
            }else{
                $ser = ARC2::getTurtleSerializer();
            }
            $out = $ser->getSerializedIndex($res['result']);
        }else{
            echo $res['result'][$uri]['http://rdfs.org/sioc/types#content'][0]['value'];
        }

        echo $out;

    }else{
        var_dump($ep->getErrors());
    }

}else{
    header("Location: http://rhiaro.co.uk/llog");
}
?>