<dl class="h-entry h-x-extension" id="follow-of">
  <h2><dt class="p-name">follow-of</dt></h2>
  <dd class="p-summary">Indicates the author of the post followed (or subscribed to) the person or feed represented by the link value.</dd>
  <h3>Example</h3>
  <pre>
    ..
  </pre>
  <time class="dt-published" datetime=""><a href="http://rhiaro.co.uk/terms#follow-of">March 2015</a></time>
</dl>

<dl class="h-entry h-x-extension" id="extension">
  <h2><dt class="p-name">extension</dt></h2>
  <dd class="p-summary">An entry or post that serves as documentation for a Microformats extension.</dd>
  <h3>Example</h3>
  <p><a href="view-source:http://rhiaro.co.uk/terms">view source</a></p>
  <time class="dt-published" datetime="2015-07-01T23:12:00+01:00"><a href="http://rhiaro.co.uk/terms#extension">1st July 2015, 23:12 BST</a></time>
</dl>