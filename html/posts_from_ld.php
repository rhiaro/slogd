<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/ARC2/ARC2.php');
require_once('lib/storesetup.php');

function get_by_list($list){
	global $ep;

	$q = "PREFIX dct: <http://purl.org/dc/terms/>
	PREFIX sioc: <http://rdfs.org/sioc/types#>
	CONSTRUCT {?p dct:title ?title . ?p sioc:topic ?tag . ?p dct:created ?d }
	WHERE { 
		?p dct:title ?title . 
		?p dct:created ?d .
		OPTIONAL { ?p sioc:topic ?tag } .
		?p sioc:Category \"".$list."\" .
	}
	ORDER BY DESC(?d)";

	//var_dump($q);
	$res = $ep->query($q);
	if(!$ep->getErrors()){
		$posts = array();
		foreach($res['result'] as $uri=>$post){
			$posts[$uri]['title'] = $post['http://purl.org/dc/terms/title'][0]['value'];
			$posts[$uri]['pubdate'] = $post['http://purl.org/dc/terms/created'][0]['value'];
			$posts[$uri]['tags'] = array();
			if($post['http://rdfs.org/sioc/types#topic']){
				foreach($post['http://rdfs.org/sioc/types#topic'] as $tags){
					array_push($posts[$uri]['tags'], $tags['value']);
				}
			}
		}
	}else{
		var_dump($ep->getErrors());
	}
	return $posts;
}

function make_list_html($list){
	$posts = get_by_list($list);
	if(empty($posts)){
		$html = "<p class=\"fail\">No posts in this list</p>";
	}else{
		$html = "<ul>";
		foreach($posts as $uri => $post){
			$stamp = strtotime($post['pubdate']);
			$date = date("jS F Y, G:i", $stamp);
			$link = split("rhiaro.co.uk/", $uri)[1];
			$html .= "<li><a href=\"$link\">".$post['title']."</a> (".$date."), tags: ";
			foreach($post['tags'] as $tag){
				$html .= $tag.", ";
			}
			$html = substr($html, 0, -2);
			$html .= "</li>";
		}
		$html .= "</ul>";
	}
	return $html;
}

?>
<html>
<head><title>Posts from triplestore</title></head>
<body>
	<h1>Stuff</h1>
	<h2>To Do</h2>
	<?=make_list_html("todo")?>
	<h2>Doing</h2>
	<?=make_list_html("doing")?>
	<h2>Done</h2>
	<?=make_list_html("done")?>
</body>
</html>