<?
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');

$m = date("m");
$y = date("Y");
$after = date(DATE_ATOM, strtotime($y."-".$m."-01 00:00"));
$nextm = $m+1;
if(strlen($nextm) == 1) { $nextm = "0".$nextm; }
$before = date(DATE_ATOM, strtotime($y."-".$nextm."-01"));

$param = array("mentions"=>array("before"=>$before, "after"=>$after));

$postlist = new Slogd_Renderer($ep);
if($postlist->render_expanded_list($param)){
	$items = $postlist->get_output();
	$c = $postlist->get_count();
}

$prev = strtotime('first day of previous month');
$prevm = date("m",$prev);
$prevy = date("Y",$prev);

$h = "h-feed";
$hchild = "h-cite";
$template = "post_mini";

$title = ": mentions";
include("templates/home_top.php");
?>
<div class="w1of1 color3-bg clearfix">
  <div class="w1of5">
    <? include 'templates/h-card.php'; ?>
  </div>
  <div class="w4of5 lighter-bg"><div class="inner">
    <? include("templates/list.php"); ?>
  </div></div>
</div>
<?
include("templates/end.php");
?>