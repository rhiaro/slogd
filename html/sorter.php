<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/ARC2/ARC2.php');
require_once('lib/storesetup.php');

if(isset($_POST['sub'])){
	$list = $_POST['list'];
	$tag = $_POST['tag'];
	$sioc = "topic" ;
	if($list && !empty($list)) {
		$topic = "<".$list.">";
	}elseif($tag && !empty($tag)) {
		$topic = "\"".$tag."\"";
	}
	if($_POST['del']){
		$qt = "DELETE FROM";
	}else{
		$qt = "INSERT INTO";
	}
	var_dump($qt);
	$ires = array("Nuffink happened");
	if($_POST['t']){
		foreach($_POST['t'] as $uri){
			$iq = "PREFIX sioc: <http://rdfs.org/sioc/types#>
					".$qt." <http://blog.rhiaro.co.uk#> { <".$uri."> sioc:".$sioc." ".$topic." }";
			array_push($ires, $ep->query($iq));
		}
	}
	echo "Results: <pre style=\"max-height: 64px; overflow-y: scroll; border: 1px solid silver;\">";
	var_dump($ires);
	echo "</pre>";
}

$q_posts = "PREFIX dct: <http://purl.org/dc/terms/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX sioc: <http://rdfs.org/sioc/types#>
CONSTRUCT {?p dct:title ?title . ?p sioc:topic ?tag }
WHERE { 
	{?p rdf:type sioc:BlogPost} UNION {?p rdf:type sioc:MicroblogPost} .
	?p dct:title ?title . 
	?p dct:created ?d .
	OPTIONAL { ?p sioc:topic ?tag } .
}
ORDER BY DESC(?d)";

$res = $ep->query($q_posts);
$titles = array();
if(!$ep->getErrors()){
	foreach($res['result'] as $uri => $post){
		$titles[$uri]['title'] = $post['http://purl.org/dc/terms/title'][0]['value'];
		$titles[$uri]['tags'] = array();
		if($post['http://rdfs.org/sioc/types#topic']){
			foreach($post['http://rdfs.org/sioc/types#topic'] as $tags){
				if($tags['type'] == "literal"){
					array_push($titles[$uri]['tags'], $tags['value']);
				}elseif($tags['type'] == "uri"){
					$titles[$uri]['list'] = $tags['value'];
				}
			}
		}
	}
}

function get_existing_tags(){
	$q_tags = "PREFIX sioc: <http://rdfs.org/sioc/types#>
	SELECT DISTINCT ?tag
	WHERE {
	       ?p sioc:topic ?tag .
	}
	ORDER BY ASC(?tag)";

	$tags_res = $ep->query($q_tags);
	$tags = array();
	if(!$ep->getErrors()){
		foreach($tags_res['result']['rows'] as $tag){
			array_push($tags, $tag['tag']);
		}
		return $tags;
	}else{
		return $ep->getErrors();
	}
}

?>
<html>
<head><title>Quick tags/lists editor</title></head>
<body>
	<form method="post">
		<p>
			<label for="list">List (Category):</label>
			<select name="list" id="list">
				<option value="0">(choose)</option>
				<option value="http://vocab.amy.so/blog#Todo">To do</option>
				<option value="http://vocab.amy.so/blog#Doing">Doing</option>
				<option value="http://vocab.amy.so/blog#Done">Done</option>
			</select>
		</p>
		<p>
			<label for="tag">Tag (topic):</label>
			<input type="text" name="tag" id="tag"></input>
		</p>
		<p><input type="checkbox" name="del" id="del" value="1" /> <label for="del">Remove?</label></p>
		<hr/>
	<?foreach($titles as $uri=>$title):?>
		<p><input type="checkbox" name="t[]" id="<?=substr($uri, -26)?>" value="<?=$uri?>" /><label for="<?=substr($uri, -26)?>">
			<?=$title['list'] == "http://vocab.amy.so/blog#Todo" ? "[ ] " : ""?>
			<?=$title['list'] == "http://vocab.amy.so/blog#Doing" ? "[~] " : ""?>
			<?=$title['list'] == "http://vocab.amy.so/blog#Done" ? "[x] " : ""?>
			<?=$title['title']?> (<?foreach($title['tags'] as $tag):?><?=$tag?>, <?endforeach?>)
		</label></p>
	<?endforeach?>
		<p><input type="submit" value="Makeatriplesgo" name="sub" /></p>
	</form>
</body>
</html>