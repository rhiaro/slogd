<?
session_start();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Publications</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="pingback" href="https://webmention.io/rhiaro.co.uk/xmlrpc" />
        <link rel="webmention" href="https://webmention.io/rhiaro.co.uk/webmention" />
        <link rel="authorization_endpoint" href="https://indieauth.com/auth" />
        <link rel="token_endpoint" href="https://tokens.indieauth.com/token" />
        <link rel="micropub" href="http://rhiaro.co.uk/micropub.php" />
        <link rel="stylesheet" href="/css/normalize.min.css"/>
        <link rel="stylesheet" href="/css/main.css" title="rhiaro"/>
        <link rel="stylesheet" href="/css/font-awesome.min.css"/>
        <link rel="stylesheet alternate" media="all" title="LNCS" href="http://linked-research.270a.info/media/css/lncs.css"/>
        <link rel="stylesheet alternate" media="all" title="ACM" href="http://linked-research.270a.info/media/css/acm.css"/>
        <link rel="stylesheet alternate" href="http://www.w3.org/StyleSheets/TR/W3C-REC.css" media="all" title="W3C-REC"/>
        <link rel="stylesheet" media="all" href="http://linked-research.270a.info/media/css/lr.css"/>
        <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="http://linked-research.270a.info/scripts/html.sortable.min.js"></script>
        <script src="http://linked-research.270a.info/scripts/lr.js"></script>

        <!--[if lt IE 9]>
            <script src="js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
        <![endif]-->
    </head>
    <body class="h-feed" about="[this:]" typeof="foaf:Document sioc:Post biblio:Paper prov:Entity" prefix="rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns# rdfs: http://www.w3.org/2000/01/rdf-schema# owl: http://www.w3.org/2002/07/owl# xsd: http://www.w3.org/2001/XMLSchema# dcterms: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ v: http://www.w3.org/2006/vcard/ns# pimspace: http://www.w3.org/ns/pim/space# cc: http://creativecommons.org/ns# skos: http://www.w3.org/2004/02/skos/core# prov: http://www.w3.org/ns/prov# schema: http://schema.org/ rsa: http://www.w3.org/ns/auth/rsa# cert: http://www.w3.org/ns/auth/cert# cal: http://www.w3.org/2002/12/cal/ical# wgs: http://www.w3.org/2003/01/geo/wgs84_pos# org: http://www.w3.org/ns/org# biblio: http://purl.org/net/biblio# bibo: http://purl.org/ontology/bibo/ book: http://purl.org/NET/book/vocab# ov: http://open.vocab.org/terms/ doap: http://usefulinc.com/ns/doap# dbr: http://dbpedia.org/resource/ dbp: http://dbpedia.org/property/ sio: http://semanticscience.org/resource/ opmw: http://www.opmw.org/ontology/ deo: http://purl.org/spar/deo/ doco: http://purl.org/spar/doco/ cito: http://purl.org/spar/cito/ fabio: http://purl.org/spar/fabio/ sro: http://salt.semanticauthoring.org/ontologies/sro#">
      <data class="p-name" value="Publications" />
      <data class="p-url p-uid" value="http://rhiaro.co.uk/<?=$_SERVER['REQUEST_URI']?>" />
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <main class="w1of1 clearfix">
          <div class="w1of1 color3-bg clearfix">
            <header id="header" class="w1of5">
              <? include 'templates/h-card.php'; ?>
            </header>
            <div class="w4of5 lighter-bg"><div class="inner"><div class="w3of4">
              <?if(!isset($_GET['p'])):?>
                <h1>Academic publications (6)</h1>
                <ul>
                  <li><a href="?p=websci15">Self Curation, Social Partitioning, Escaping from Prejudice and Harassment: the Many Dimensions of Lying Online</a> (Max van Kleek, Dave Murray-Rust, Amy Guy, Daniel Smith and Nigel Shadbolt), ACM WebSci 2015</li>
                  <li>Poster: Self Curation, Social Partitioning, Escaping from Prejudice and Harassment: the Many Dimensions of Lying Online (Max van Kleek, Dave Murray-Rust, Amy Guy, Daniel Smith and Nigel Shadbolt), WWW 2015</li>
                  <li>Poster: <a href="?p=microposts15">Connections between Twitter Spammer Categories</a> (Gordon Edwards and Amy Guy), Making Sense of Microposts, WWW 2015</li>
                  <li><a href="?p=socm15">Social Personal Data Stores: the Nuclei of Decentralised Social Machines</a> (Max van Kleek, Daniel Smith, Dave Murray-Rust, Amy Guy, Kieron O'Hara, Laura Dragan, Nigel Shadbolt), SOCM, WWW 2015</li>
                  <li><a href="?p=cim14">Roles and relationships as context-aware properties</a> (Amy Guy), Context, Interpretation and Meaning, ISWC 2014</li>
                  <li><a href="?p=socm14">Constructed Identity and Social Machines: A Case Study in Creative Media Production</a> (Amy Guy and Ewan Klein), SOCM, WWW 2014</li>
                </ul>
              <?else:?>
                <? include("pub/".$_GET['p'].".php"); ?>
              <?endif?>
            </div></div></div>
          </div>
  <?
  include("templates/end.php");
  ?>
  