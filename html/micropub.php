<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
include_once 'lib/Slogd_Tripler.php';
include_once 'lib/Slogd_makeUri.php';

function default_metadata($type, $tags="", $target=null, $date=null){
    if($date == null){ $date = date("jS F Y, G:i e"); }
    $data = "<!---
";
    
    if($type == "eat"){
        $tags .= "eat%2Cfood%2Cquantified self%2Clife";
        $data .= "start: ".$date."
end: ".$date."
type: LlogPost
tags: ".urldecode($tags);
    }elseif($type == "like" || $type == "favourite"){
        $data .= "published: ".$date."
like-of: ".$target."
tags: ".urldecode($tags);
    }elseif($type == "share" || $type == "repost"){
        $data .= "published: ".$date."
share-of: ".$target."
tags: ".urldecode($tags);
    }elseif($type == "bookmark" || $type == "save"){
        $data .= "published: ".$date."
bookmark-of: ".$target."
tags: ".urldecode($tags);
    }
    $data .= "
-->

";
    return $data;
}

// Tell client where I can syndicate to
if(isset($_GET['q']) && $_GET['q'] == "syndicate-to"){
    header('Content-Type: application/x-www-form-urlencoded');
    echo "syndicate-to[]=twitter.com%2Frhiaro";
    exit;
}

// Tell a client preferred format of source, for editing
if(isset($_GET['q']) && $_GET['q'] == "source"){
    header('Content-Type: application/x-www-form-urlencoded');
    echo "source=text/markdown";
}

// Check for post
if(!empty($_POST)){

    $headers = apache_request_headers();
    // Check token is valid
    $token = $headers['Authorization'];
    $ch = curl_init("https://tokens.indieauth.com/token");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, Array(
         "Content-Type: application/x-www-form-urlencoded"
        ,"Authorization: $token"
    ));
    $response = Array();
    parse_str(curl_exec($ch), $response);
    curl_close($ch);
    // Check for post scope
    // Check for me=http://rhiaro.co.uk
    $me = $response['me'];
    $iss = $response['issued_by'];
    $client = $response['client_id'];
    $scope = $response['scope'];

    // Sort out content
    $notpost = false;
    
    // writing log files
    $log = "log_".date("ymd-His").".txt";
    $h = fopen($log, 'w');
    foreach($_POST as $k=>$v){
      $logtext .= "[$k] $v\n";
    }
    fwrite($h, $logtext);
    fclose($h);
    
    $syndicate = false;
    
    if(!empty($_POST['content'])){
        // Like/share/bookmark shortcutting
        if(in_array($_POST['content'], array("like", "favourite", "share", "repost", "bookmark", "save"))){
            if(isset($_POST['in-reply-to'])){ $target = $_POST['in-reply-to']; }
            elseif(isset($_POST['like-of'])){ $target = $_POST['like-of']; $syndicate = true; }
            elseif(isset($_POST['repost-of'])){ $target = $_POST['repost-of']; $syndicate = true; }
            elseif(isset($_POST['bookmark'])){ $target = $_POST['bookmark']; }
            elseif(isset($_POST['bookmark-of'])){ $target = $_POST['bookmark-of']; }
            $content = default_metadata($_POST['content'], $_POST['category'], $target);
            $notpost = true;
        }else{
            // Normal post
            $content = $_POST['content'];
            // Check if it's a retweet to bridgy-posse omg this is awful i hate everything *vomit*
            // TODO: This is syndicating everything wtf
            if(stripos($content, "share-of: https://twitter.com/") || stripos($content, "repost-of: https://twitter.com/")){
              $syndicate = true;
            }
        }
    // Teacup food post
    }elseif(!empty($_POST['p3k-food'])){
        $content = default_metadata("eat", "", null, $_POST['published']);
        $content .= $_POST['p3k-food'];
        $fn = "llog_eat";
    }elseif(!empty($_POST['bookmark'])){
        $content = default_metadata("bookmark", $_POST['category'], $_POST['bookmark'], $_POST['published']);
        $notpost = true;
        $fn = "save";
    }elseif(!empty($_POST['like-of'])){
        $content = default_metadata("like", $_POST['category'], $_POST['like-of'], $_POST['published']);
        $syndicate = true;
        $notpost = true;
        $fn = "like";
    }elseif(!empty($_POST['repost-of'])){
        $content = default_metadata("repost", $_POST['category'], $_POST['repost-of'], $_POST['published']);
        $syndicate = true;
        $notpost = true;
        $fn = "share";
    // No content dun-dun
    }else{
        $content = false;
        $fn = false;
    }
    if(empty($response)){
        header("HTTP/1.1 401 Unauthorized");
        exit;
    }elseif($me != "http://rhiaro.co.uk/" && $me != "http://rhiaro.co.uk" || $scope != "post"){
        header("HTTP/1.1 403 Forbidden");
        exit;
    }elseif(empty($content)){
        header("HTTP/1.1 400 Bad Request");
        echo "Missing content";
    }else{
        // All good
        // Check for slug to use
        if(isset($_POST['slug'])){
            $slug = $_POST['slug'];
            $uri = uri_from_slug($slug, time(), true);
        }else{
            // No slug, URI will be generated during triplification
            $uri = null;
        }
        // Check for additional geo and parse
        if(isset($_POST['location'])){
            preg_match("/(?<=\:)(.*?)(?=\;)/", $_POST['location'], $out);
            $geo = explode(",", $out[0]);
        }
        // Triplify content
        //   TODO: create relation between md file and post
        //   TODO: make Tripler accept string instead of file, move file writing to Tripler
        $tmp = "posts/".date("ymd-His")."_.txt";
        $h = fopen($tmp, 'w');
        fwrite($h, $content);
        fclose($h);
        $trip = new Slogd_Tripler($ep, $tmp, $uri);
        $title = $trip->get_title();
        if(isset($geo) && count($geo) == 2){
            $trip->add_triple($uri, "latitude", $geo[0]);
            $trip->add_triple($uri, "longitude", $geo[1]);
        }
        // Check for additional in-reply-to only for posts not already caught by default_metadata
        if(isset($_POST['in-reply-to']) && !$notpost){
            $trip->add_triple($uri, "reply-to", $_POST['in-reply-to']);
        }
        $trip->insert(0,true,$syndicate);
        // Turn content into markdown file
        if(!$fn){ $fn =  str_replace(" ", "", preg_replace("/[^\w\d \-]/ui", '',ucwords($title))); }
        $md = "posts/".date("ymd-Gi")."_".$fn.".md";
        $h = fopen($md, 'w');
        fwrite($h, $content);
        fclose($h);
        unlink($tmp);
        // Set headers, return location
        header("HTTP/1.1 201 Created");
        header("Location: ".$trip->get_url());
        
        var_dump($uri);
        var_dump($trip->get_triples());
    }

}else{
  $post = file_get_contents('php://input');
  
  if(isset($post)){
    
    $json = json_decode($post, true);
    // hack, need to check published date from context TODO
    if(isset($json["published"])){
      $pub = $json["published"];
    }elseif(isset($json["dct:created"])){
      $pub = $json["dct:created"];
    }elseif(isset($json["as:published"])){
      $pub = $json["as:published"];
    }elseif(isset($json["as2:published"])){
      $pub = $json["as2:published"];
    }else{
      $pub = date(DATE_ATOM);
    }
    $id = uniqid();
    
    if(isset($headers['Slug'])){
      $slug = urlencode($headers['Slug']);
      $uri = uri_from_slug($slug, strtotime($pub), true);
      $pto = uri_from_slug($slug, strtotime($pub), false);
    }else{
      $uri = Slogd_makeUri($ep, $id, strtotime($pub));
      $pto = Slogd_makeUri($ep, $id, strtotime($pub), "BlogPost", false);
    }
    $json["@id"] = $uri;
    $json["http://xmlns.com/foaf/0.1/isPrimaryTopicOf"] = array("@id" => $pto);
    $json["http://purl.org/dc/terms/created"] = array("@value" => $pub, "@type" => "xsd:dateTime");
    
    require_once("lib/rdfhelpers.php");
    
    $data = json_encode($json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    $turtle = json_to_turtle($data);
    
    $log = "apdumps/".date("ymd-His")."_".$id.".json";
    $h = fopen($log, 'w');
    fwrite($h, $data);
    fclose($h);
    $log2 = "apdumps/".date("ymd-His")."_".$id.".ttl";
    $h2 = fopen($log2, 'w');
    fwrite($h2, $turtle);
    fclose($h2);
    
    $q = "LOAD <$log2> INTO <http://blog.rhiaro.co.uk#>";
    $res = $ep->query($q);
    
    var_dump($res);
    
    header("HTTP/1.1 201 Created");
    header("Location: $pto");
    
  }else{
    // writing log files
    $log = "log_".date("ymd-His").".txt";
    $h = fopen($log, 'w');
    $logtext = "No POST\n";
    foreach($_POST as $k=>$v){
      $logtext .= "[$k] $v\n";
    }
    fwrite($h, $logtext);
    fclose($h);
    header("HTTP/1.1 400 Bad Request");
    echo "Nothing posted";
  }
}
?>
