<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
include_once 'lib/Slogd_makeUri.php';

/////// Update URIs

if(isset($_POST['sub'])){
    $new = $_POST['new'];
    $uri = $_POST['uri'];
    $pto = $_POST['pto'];
    $insq = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    INSERT INTO <http://blog.rhiaro.co.uk#> { <$new> ?p ?o . <$new> foaf:isPrimaryTopicOf <$pto> . }
    WHERE { <$uri> ?p ?o . }";
    $delq = "DELETE FROM <http://blog.rhiaro.co.uk#> { <$uri> ?p ?o . } 
    WHERE { <$uri> ?p ?o . }";
    $insres = $ep->query($insq);
    $delres = $ep->query($delq);
    echo "<pre>";
    var_dump(htmlentities($insq));
    echo "\n";
    var_dump($insres);
    echo "\n\n";
    var_dump(htmlentities($delq));
    echo "\n";
    var_dump($delres);
    echo "</pre>";
    
}
/////// Get list of posts.
$q = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX blog: <http://vocab.amy.so/blog#>
SELECT ?s ?title ?date WHERE { ?s dct:title ?title . ?s rdf:type ?type . ?s dct:created ?date . FILTER ( ?type != blog:LlogPost ) }";
$res = $ep->query($q);
foreach($res['result']['rows'] as $r){
    echo "<form method=\"post\"><input type=\"submit\" name=\"sub\" value=\"Update URI\"/><br/>";
    $uri = $r['s'];
    $title = $r['title'];
    $date = strtotime($r['date']);
    echo "$uri - $title ($date)<br/>";
    echo "<input type=\"hidden\" name=\"uri\" value=\"$uri\" />";
////// Change raw URIs to new format
    $new = Slogd_makeUri($ep, $title, $date);
    $pto = rendered_from_raw($new, $date);
    echo "<input type=\"text\" name=\"pto\" value=\"$pto\" size=\"100\" /><br/>";
    echo "<input type=\"text\" name=\"new\" value=\"$new\" size=\"100\" /></form>";
    echo "<hr/>";
    
}


?>