<?
require_once "lib/rdfhelpers.php";
$as2 = ns("as");
$rdf = ns("rdf");

if(isset($_GET['format'])){
  if($_GET['format'] == "ttl"){
    header("Content-Type: text/turtle");
    echo file_get_contents("locations.ttl");
  }elseif($_GET['format'] == "json"){
    header("Content-Type: application/ld+json");
    echo arc2array_to_as2_collection(parse_turtle("locations.ttl"));
  }
}else{

session_start();

$venues = parse_turtle("locations.ttl");
unset($venues["http://rhiaro.co.uk/locations"]);

$title = "go places";
$listheader = "Locations";
$c = count($venues);

include("templates/home_top.php");
?>
<div class="w1of1 color3-bg clearfix">
  <div class="w1of5">
    <? include 'templates/h-card.php'; ?>
  </div>
  <div class="w4of5 lighter-bg" about="http://rhiaro.co.uk<?=$_SERVER['REQUEST_URI']?>" typeof="as2:Collection"><div class="inner">
    <h1><?=$listheader?> (<?=$c?>)</h1>
    <ul class="plist" rel="as2:items">
    <?foreach($venues as $uri => $location):?>
      <?if(isset($location[$as2."name"])):?>
        <li class="h-card" resource="<?=$uri?>" typeof="<? if(isset($location[$rdf."type"])) foreach($location[$rdf."type"] as $type) { echo $type." "; } ?>">
          <a href="<?=$uri?>" class="u-url"><h2 class="p-name" property="as2:name"><?=$location[$as2."name"][0]?></h2></a>
          <p class="p-summary" property="as2:summary"><?=$location[$as2."summary"][0]?></p>
        </li>
      <?endif?>
    <?endforeach?>
    </ul>
  </div></div>
</div>
<?
include("templates/end.php");
} // end conneg
?>
