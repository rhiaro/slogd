<?
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');

if(isset($_GET['m']) && isset($_GET['y']) && isset($_GET['post'])){
    $m = $_GET['m'];
    $y = $_GET['y'];
    $id = $_GET['post'];
    // TODO fix this
    $page = "http://rhiaro.co.uk/".$y."/".$m."/".$id;
}

if($id == ""){ header("Location: /".$y."/".$m); die(); }

$postlist = new Slogd_Renderer($ep);
if($postlist->render_post_and_replies($page)){
    $items = $postlist->get_output();
    
    if(isset($items["http://blog.rhiaro.co.uk/".$id]['name'])) {
      $title = $items["http://blog.rhiaro.co.uk/".$id]['name'];
    }elseif(isset($items["http://blog.rhiaro.co.uk/".$id]['content'])){
      $title = substr(strip_tags($items["http://blog.rhiaro.co.uk/".$id]['content']), 0, 32);
      if(strlen(strip_tags($items["http://blog.rhiaro.co.uk/".$id]['content'])) > 32){
        $title .= "...";
      }
    }
}
$h = "h-entry";
include("templates/home_top.php");
?>

<div class="w1of1 color3-bg clearfix">
  <div class="w1of5">
    <? include 'templates/h-card.php'; ?>
  </div>
  <? $trackdate = 0; ?>
  <div class="w4of5 lighter-bg"><div class="inner">
    <?foreach($items as $uri => $post):?>
      <?if($uri != "template"):?>
      <article<?=$h != "h-entry" ? " class=\"h-entry\"" : ""?>>
        <? include 'templates/post.php'; ?>
        
        <?if(isset($post['replies'])):?>
          <ul class="w1of1 uplist clearfix">
            <h2 id="mentions" class="w3of4">Mentions (<data about="http://rhiaro.co.uk<?=$_SERVER['REQUEST_URI']?>" property="sioc:num_replies" value="<?=count($post['replies'])?>"><?=count($post['replies'])?></data>)</h2>
            <?foreach($post['replies'] as $uri => $post):?>
              <li class="u-comment u-mention h-cite" about="<?=$uri?>" typeof="as2:Object">
                <? include 'templates/post_mini.php'; ?>
              </li>
            <?endforeach?>
          </ul>
        <?endif?>
      
      </article>
      <?endif?>
    <?endforeach?>
  </div></div>
</div>
<?
include("templates/end.php");
?>