<!---
published: 11th October 2013, 16:24
last-modified: 12th October 2013, 21:40
type: BlogPost
tags: hacking,learning,semantic web,linked data,blog,markdown
list: doing
-->

# PHP Markdown by Michel Fortin

I'm using [Michel Fortin's PHP Markdown](http://michelf.ca/projects/php-markdown) to grab each md file in the posts directory and convert the contents to HTML.

I'll add some custom stuff to parse labels and categories that I plan to use to organise my content.

I also need to think about templating, and when the to-triples part is going to happen.

**Updates (16:48)**

1. PHP Markdown is already able to work with Smarty, but the Smarty docs are awful and the site hurts my brain.
2. The site and docs for Twig seem much nicer, and it claims to be more modern and faster than Smarty, so that's leading the templating engine race at the moment.
3. Do I need to use a templating engine at all? Isn't this kind of what I'm building..?