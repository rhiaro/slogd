<!---
published: 
type: BlogPost
tags: hacking,indieweb,activitystreams,activitystreams2.0,microformats2,mf2,post types,activities,feed,social web
list: doing
-->

# More on post types

## UI First

Stuff on the web is for humans, so thinking about your data model from the UI first makes sense. Whilst I'm perfectly capable of figuring out what makes an interface good or not, I'm not a designer and - like many other developers I know - don't necessarily trust myself to make the best UI decisions.
