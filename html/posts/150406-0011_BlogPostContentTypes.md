<!---
published: 6th April 2015, 00:11
type: BlogPost
tags: hacking,indieweb,micropub,php,slogd,htaccess,content negotiation,linked data,turtle,rdf,modrewrite,uris
list: doing
-->

# Blog post content types

Finally you can get my blog post content in a number of different formats. You'll notice alternative URLs (noted currently as 'Permalink') in the metadata section of a normal view of a post. Hit this up with no Accept header, and you'll get neat markdown (which is what I authored it in; the 'source', as it were).

Browsers automatically send `Accept: text/html`, in which case they are redirected to the rendered HTML version of the post you're probably reading now. But here are some alternatives you can try:

     curl http://blog.rhiaro.co.uk/micropub-test

-> plain markdown.

     curl -H "Accept: application/json" http://blog.rhiaro.co.uk/micropub-test

because there's obviously not enough JSON in the world. Note this isn't correct JSON-LD yet. I'll sort that out another time.

     curl -H "Accept: text/turtle" http://blog.rhiaro.co.uk/micropub-test

Ohemgee! RDF! It's what you've all been waiting for, I know.

For those of you who like angular brackets, you could try:

     curl -H "Accept: rdf/xml" http://blog.rhiaro.co.uk/micropub-test

## Relevance to micropub

Briefly discussed with [Tantek](http://tantek.com), [Aaron](http://aaronpk.com) and [Bret](http://bret.io) at [IWC Cambridge](http://indiewebcamp.com/2015/Cambridge) and [on IRC](http://indiewebcamp.com/irc/2015-04-03/line/1428103596233) about how a micropub client could fetch post content for editing. Many people aren't editing raw HTML as it's presented on the page, but maybe markdown or some other syntax, so a client needs to be able to discover this (the 'source') to present it to be edited. Now if someone makes their client check a post for a `rel="source"` or shoots off a request with `Accept: text/plain` (less likely, since static sites can't do conneg) then they'll get my markdown directly (uh, when I actually put rel="source" in my HTML, which I haven't yet).

As a related aside, I also return `source="markdown"` if a micropub client asks my endpoint `q=source`.