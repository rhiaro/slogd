<!---


-->

# On communication channels

Recently the topic of how best to communicate with each other has been raised in the [Social Web WG](https://www.w3.org/wiki/Socialwg). The group (as is normal for W3C WGs) uses three channels, in addition to weekly teleconference calls: a mailing list (actually three, but we'll not go into that), an IRC channel (actually two, again..) and a Wiki. Most members of the group manage to use and keep up with all of these as they see fit, but not everyone can or wants to, and so they concentrate on their preferred channel.

I think each is appropriate for different purposes, and the discussion about this started me thinking about the properties I like about various comms mechanisms. Here are some properties I consider desirable:

* `D`: **Decentralised** - messages posted by an individual are retained somewhere the individual controls, and are not vulnerable to data loss through a [site death](http://indiewebcamp.com/site-deaths).
* `A`: **Archivable** - discussions (collections of messages) on a particular topic can be easily permalinked to for archival purposes.
* `F`: **Filterable** - it's easy to find messages you're interested in; antonym: noisy, unfocused.
* `T`: **Threaded** - conversations are threaded; discussions on different topics can be kept separate.
* `L`: **Longform** - it's easy to have long, thought-out discussions.
* `I`: **Instant** - easy to get instant, or at least fast, responses to messages.
* `P`: **Pushed** - new messages are pushed to you, you don't have to seek them out.

Considering a discussion including many people, how does each channel perform according to these properties?

## Assumptions

* Mailing lists and IRC are publicly logged, as W3C ones are.
* Sent and recieved emails are stored locally/self-hosted, not only servers of a webmail provider.
* This is based on my experiences, I know not everyone's will be the same.

## Results

**Key**

* PS is Personal Site, as in hosted oneself indieweb style. 
* SNS is Social Networking Site, like facebook or twitter etc. I've lumped them all together because whatever.
* IT is issue tracker, commonly used for software projects but also useful for other things, I've found.
* 1 means I think it has this property.
* 0 means I think it doesn't have this property.
* ~ means I think it could have this property depending on your setup *and it would be trivial to implement* (not relying on IFTT or cron jobs or bots..), and I'll expand on most ~s afterwards.

|       | D | A | F | T | L | I | P | 1 | ~ | 0 |
| ===== | = | = | = | = | = | = | = | = | = | = |
| Email | 1 | 1 | ~ | 1 | 1 | ~ | 1 | 5 | 2 | 0 |
| ----- | - | - | - | - | - | - | - | - | - | - |
| IRC   | 0 | 1 | ~ | 0 | 0 | 1 | ~ | 2 | 2 | 3 |
| ----- | - | - | - | - | - | - | - | - | - | - |
| Wiki  | 0 | 1 | 1 | 1 | 1 | 0 | 0 | 4 | 0 | 3 |
| ----- | - | - | - | - | - | - | - | - | - | - |
| IT    | ~ | 1 | 1 | 1 | 1 | ~ | 0 | 4 | 3 | 0 |
| ----- | - | - | - | - | - | - | - | - | - | - |
| PS    | 1 | 0 | ~ | 1 | 1 | 0 | 0 | 3 | 1 | 3 |
| ----- | - | - | - | - | - | - | - | - | - | - |
| SNS   | 0 | 0 | ~ | 1 | ~ | ~ | ~ | 1 | 4 | 2 |
| ----- | - | - | - | - | - | - | - | - | - | - |

## Discussion

I actually wasn't expecting email to come out on top when I went into this, and I wasn't expecting IRC to score so low. We can see that none of them are good for everything - which I did expect, so my hypothesis that each is appropriate in different circumstances stands.

I'm going to go through a few things in more detail.

### Noise

A common complaint about email is that there's too much, there's spam, it's overwhelming. I've managed this with a setup I've evolved gradually over the years, which is complex if you take it all at once, but I introduced elements a bit at a time. It relies on intricate Gmail filters, labelling and archiving, very specific use of starring, and Multiple Inboxes (a gmail Labs feature), *and* (crucially) only subscribing to things I want to hear from. The reliance on Gmail is a *huge* downside. If Google decide to pull all the Labs features I'm using, or I move away from Gmail (which I desperately want to do), I might discover what a mess my email life really is. On the other hand, most email clients offer some level of filtering and labelling, and I've heard great things about MailPile, so maybe it's not so bad. For those who aren't de-Googling, I notice Google pushing things like Priority Inbox, Inbox and Important flags... which are supposed to automate this process, but what I concocted before these came along has worked so well I haven't tried any of these offerings. But perhaps for other people they're useful.

I'll argue that IRC could have a similar problem. I lurk in 65 channels at the time of writing (6 are Slack channels I access with my IRC client), some very active and some basically dead, but actively keep up to date with the logs for about four of the active ones. If they were *more* active, or I was unable to check them for a few days or more, the noise would build up, and since conversations within channels aren't threaded, it would be even harder to filter out what I'm interested in.

For finding things you're interested in on someone's personal site, you're at the mercy of the UI they've implemented. I have tags but not search or date sorting (yet!), and other people do things differently.

### Threading and collating

Something I think is critical, which email has and IRC doesn't, is threaded conversations. Many times I've been chatting in an IRC channel with one or two (or three or four) other conversations going on around me. I can follow at the time, because I know who I'm talking to, but anyone jumping in later or reading logs afterwards will really struggle to figure out which messages belong to which thread.

On a related note, public mailing list logging (as the W3C does) is great becaues one can reference a collection of messages with a URL. The W3C mailing list archive UI is not great (you have to read one message at a time; being able to pull a whole thread up to skim or ctrl+f would be ace) but that's fixable.  This is not easily possible with messages posted on personal sites, as they're hosted all over the place. Conversations can be threaded (via webmention responses etc) but we're lacking anything to aggregate and display these threads at a single URL. Well, maybe I'll add that to [my itches](http://indiewebcamp.com/User:rhiaro.co.uk#Itches). 

### Immediacy and pushing

A lot of this is just dependent on setup. Lots of people have email notifications on their phones, but lots don't, and many people (quite reasonably) object to being slaves to emails 24/7, so may not respond right away. Similarly, I know a core group of ~~nerds~~ people who have IRC clients on their phones (myself included) and recieve notifications when they're mentioned. Though the apps aren't polished and connectivity drops often, so it's not reliable. Most clients allow you to set notifications for any words mentioned, so you could be alerted at once if someone talks about a particular topic. But again, not a core need for most people. On the other hand, since IRC messages are shorter, when you *do* see one, it usually makes sense to reply at once. So if you know someone is available, an IRC discussion is an instant and easy conversation. With email, people tend to be conditioned to leave it longer.

I also find it important that while I can join mailing lists to increase the chance (in theory) of seeing interesting messages, don't have to actively follow people to recieve messages from them. I not infrequently get interesting messages out of the blue from people I either don't know or am not keeping up to date with. Maybe I'm just lucky. Gmail's spam filter certainly helps me to avoid seeing actual spam (but I get maybe zero to ten messages in my spam folder per day, so no big deal). Sure, someone could write a blog post and send me a webmention to tag me, and maybe even have access control set up so only I can see it, but... as it turns out, I communicate with people from all walks of life, not just web developers. Particulary note parents of kids who attend [Prewired](http://www.prewired.org), who often send their kids to Prewired because they understand that all of the kid's staring at a screen is somehow important for their education, but that's as far as it goes. I organise lots of events, and sometimes caterers don't even have their own sites, and various admin staff sure as hell don't want them. And my Mum.

I went off on a bit of a tangent there. But actually, since I mention my Mum, she would generally prefer that I email her regularly with updates about my life, but I don't, so she has to go somewhat out of her way to check twitter to see what I'm up to lately. If I haven't tweeted for a while, she emails me to check I'm okay. I haven't broached the blog with her yet. So that's the other angle; I don't push messages directly to an interested party, and she's inconvenienced as a result.

Something missing - but under development - in the indieweb community is readers/aggregators and nofications. I'm sure it'll come, but we're not there yet, and there are lots of problems to tackle on the way.

### Brevity

This is the key area where I think different channels shine.

IRC is great for a rapid fire conversation. Great for getting quick answers to short questions. 

But many things need more contemplation. Sometimes (often?) the first thing you think of isn't the best thing, and explaining yourself through prose can help to clarify an idea. Many discussions need context, which can be lengthy. Many issues are simply too complex to condense into a couple of sentences. There's the notion of the 'elevator pitch' (or 'twitter pitch') which can sometimes be helpful in communicating an idea, but can also result in dumbing down, and a loss of nuance. Often to explain something properly, particularly if to recipients with varied perspectives, it's necessary to walk through a concept without being interrupted. And rather than having people fire back immediately, possibly before they've understood everything, the recipients can take it all in at once and think it over before responding.

I've frequently typed responses to emails in the heat of the moment, and left them in my drafts, returning the next day to find I'd misunderstood something, or there's a better way of phrasing my reply. IRC doesn't lend itself to that. 

Posting an article on a personal site *does* lend itself to that, but for discussions is only useful if other people have a way to respond in context. If everyone's sites were compatible with each other (like the IndieWeb folks) then fine (barring issues with collating and pushing, but they're not and it's not unreasonable to make allowances 

asdf