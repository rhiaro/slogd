<!---
published: 13th October 2013, 21:20
last-modified: 13th October 2013, 21:53
list: doing
type: BlogPost
tags: markdown,metadata,semantic web,arc2,slogd,blog,learning,hacking
foo: bar
-->

# Metadata parsing

Right-ho, it parses whatever arbitrary metadata I put in the comments of the markdown files providing they start with "label:" and end with a newline ("\n").

And lumps it into displayable html along with the post it belongs to, mostly for easier testing.

The next step is to engage some ontologies and turn the metadata and posts into a graph.  It's ARC2 time!

Update: Actually the metadata it accepts is not all that arbitrary.  Will remedy.

Update 2: Fixed that.