<!---
published: 21st April 2015, 02:21
type: BlogPost
tags: events,prewired,maker week,kids,code club,coder dojo,edmakerfaire,Edinburgh Mini Maker Faire,rsvp
reply-to: makerfaireedinburgh.com
start: 19th April 2015, 09:00
end: 19th April 2015, 17:30
list: done
-->

# Edinburgh Mini Maker Faire

Had an amazing time supporting the Prewired table at the Edinburgh Mini Maker Faire on Sunday. I printed a load of flyers and posters, and helped haul laptops and lego, but then the kids did all the work. I was *so* impressed with how they pitched and demo'd their projects, told parents and kids all about Prewired, and generally enthused. I felt pretty safe sitting back and knowing they had things covered.

* Andrew (age 10) showed his webcam/gesture-controlled Scratch bubble-bursting game.
* Albie (age 9) demoed iPad app [Wormy Drawing](https://itunes.apple.com/TR/app/id882326367) (which is in the app store with over 1k downloads!)
* Harry and Kiran (age 10) showed their fish-eating, physics-heavy Scratch game.
* Marley (age 13) brought his Raspberry Pi and Arduino based robotic arm, that moves around and picks things up in response to commandline short codes, or a Wii remote.
* Sean and Toby (age 12) showed their Arduino model bus, which uses an ultrasound sensor to prevent accidents involving low bridges.
* Adam (age 8) showed his Scratch pong game. 

(ages approximate, I'm pretty bad at figuring them out..)

TODO: photos! (but also see [@Prewired](http://twitter.com/prewired)).

Also massive thanks to Eder, [Cameron](http://camerongray.me/) and [Harry](http://harryreeder.co.uk) who stuck it out *all day* and helped with carrying, setting up and demo-ing hardware, and Martha and [Rikki](http://rikkiguy.me.uk) who came for the last few hours (after most of the kids had left) and helped with take-down. These people are awesome.
