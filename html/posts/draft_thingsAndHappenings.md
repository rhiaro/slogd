<!---
tags: socialwg,activitystreams2,as2,social web,phd
-->

# Things and happenings: modelling social for the web

The online social world is simultaneously and varyingly a mirror, augmentation, and reduction of our offline social lives. Human behaviours are complex and nuanced and context-dependant, inconsistent, spread across many different media, subject to multiple interpretations.. and all kinds of other words that mean it's difficult to wrangle this space into a coherant or complete data model. But, we (computer scientists, software developers) do it anyway.

For some reason, nobody has come up with a solution that pleases everybody yet.

It seems probable, given the diversity of systems, requirements, desires and general worldviews, nobody ever will. In this post, I want to explore two different (but overlapping) models. I also want to emphasise that there can't possibly be a single right way of looking at this space. These models aren't in conflict (as some may think) but simply two different ways of looking at the same thing. Where proponants of one may see shortcomings in the other, the opposite is equally true. People's different experiences and needs have led them a way of looking at things that they find perfectly intuitive. Often, this means alternative perspectives are seen as unintuitive and perhaps even in competition.

So, let's take *stuff*. We start with the idea that there is *stuff* on the social web that we want to describe. One subset of *stuff* is people. I'm going to assume most agree that there are people (or agents) involved in the social web, with relationships to other people. To what extent or rigidity to attempt to model people and relationships (dots and lines in the social network analysis world) is.... the subject of another post. And a thesis chapter I should be writing. Ahem.

Other subsets of *stuff*, which I want to focus on right now, are *things* and *happenings*. I'm deliberately trying to pick silly terminology to keep this abstract. *Things* are the *stuff* that aren't people, and *happenings* are when people, uh, operate on *stuff*. (I'm backed myself into a corner of using general terms that I now can't use in their normal context). You might also think of *things* as content or objects, and *happenings* as events, interactions or activities.

The two social data models I've spent the most time thinking about recently are ActivityStreams 2.0 and microformats2 (as2 and mf2 henceforth). It's the structures of and relationship between these which have given rise to my *stuff*, *things* and *happenings* model; to various degrees, I've [written]() [about]() them before.