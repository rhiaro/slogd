# Standards and Star Trek

This is part of an ongoing series of notes about Star Trek episodes that remind me in some way of interactions with W3C Working Groups.

## On issue closing and edit wars

In TNG S1E26 ("The Neutral Zone"), a man from the late 21st century is found cryogenically frozen. He is awoken aboard the Enterprise, and soon becomes frustrated that he can't speak to the Captain (who is busy diffusing a dangerous situation with the Romulans). He uses a communications panel to call the Captain, who isn't best pleased. When reprimanded for unauthorised use of the panel, he says "If they are so important, why don't they need an 'executive key'?" to which Picard replies "Aboard a starship, that is not necessary. We are all capable of exercising self-control."

## On maintaining a sense of humour in dire situations

In TNG S1E7 ("Contagion") a computer virus attempts to rewrite the software of the Enterprise. Geordi: "We have two completely incompatible computer systems trying to interact!" Sound familiar? A potential consequence is complete destruction of the Enterprise, as happened to the USS Yamato shortly before. Later, during multiple systems failures and faced with imminent Romulan attack, the shields are going up and down and weapons systems are going on and off, Troi smirks: "In another time and another place this could be funny." Riker adds "In case it should become necessary to fight, do you suppose you could find me some rocks to throw at them?"

## On neverending circular arguements

"It is a matter of honor!" - Worf, most episodes.

## On personality conflicts

Wes: "What do I do about personality conflicts?"
Riker: "That's completely irrelevant, these people are professioanls."