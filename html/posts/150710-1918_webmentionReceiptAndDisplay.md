<!---
tags: indieweb,webmention,indiewebcamp,hacking,slogd
-->

# Webmention receipt and display

This weekend at IndieWebCamp in Brighton, my main goal is to display recieved webmentions on my posts. Currently I collect them at [webmention.io](http://webmention.io) and will occasionally manually add them to my store when there's a reply I'd particularly like displayed. This is just displayed as a link at the bottom of the post.

Initially I thought about just running a cron job to pull from webmention.io into my store. Since webmention.io does all the validating etc, this takes away a lot of work. Maybe I'll do that first, and sort out the display of full comments (not just URLs), and then look at dealing with the whole process myself. Here's the expected flow:

```
// If target not exists
//   Fail
// else If source not exists (404 or 410)
//   If have had webmention from this source before OR have mentioned this post myself
//     Delete
//     -> Remove $source from local store
//   else
//     Fail
// else If source not contains link to target
//   Fail
//
// else If have had webmention from this source before
//   This is an update: what has changed?
//     Content - edit "$source-author edited their {inferred post type} of/to $target"
//       -> Store updated $source for display
//     {property} - edit or addition of property "$source-author edited/added {property} of their {inferred post type} of/to $target"
//       -> If this is a property I store/display, store update
//     Replies/comments - new reply (receiving salmention) "$reply-author {inferred post type}d $source-author's post that mentions $target"
//       -> Store for future threading display purposes..
//
// else Infer mention type
//   Content - mention "$source-author mentioned $target in a post."
//   like-of - like "$source-author liked $target"
//   repost-of - repost "$source-author reposted $target"
//   bookmark-of - bookmark "$source-author bookmarked $target"
//   category - tag "$source-author tagged $target in a post" / "$source-author tagged a post with $target"
//   in-reply-to - reply "$source-author replied to $target"
//   {other property} - {other} "$source-author {other}-ed $target"
//   -> Store necessary $source contents for display
//   -> Resend webmentions to all links in $target (send salmention)
```

Once automatic display is working, one way or another, I'll also:

* add a form to let people send webmentions to my posts from my site.
* display all mentions aggregated at [/mentions](http://rhiaro.co.uk/mentions) (including homepage mentions aka person mentions).
* some sort of push notifications hack, possibly an email, SMS or a poke from an IRC bot.

Other things I might get around to this weekend:

* Displaying reposts properly.
* Fixing my micropub endpoint; it's currently rejecting Teacup posts for unknown reasons.
* Automatically sending bridgy webmentions for likes and reposts of tweets.