<!---
published: 30th August 2014, 16:42
last-modifed: 27th September 2014, 16:42
type: BlogPost
tags: semantic web,iswc2014,iswc,context,event,conference,presentation
list: todo
-->

# ISWC Context, Interpretation and Meaning Workshop

My paper *Roles and Relationships as Context-Aware Properties on the Semantic Web* was accepted to the ISWC Context, Interpretation and Meaning workshop. I'll be presenting it on the 17th of October, in Riva del Garda, Italy.