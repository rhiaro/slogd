<!---
tags: https,ssl,hacking,nginx,docker
-->

# HTTPS: whaaaaat

Following on from [a terrible experience](/2015/12/https-what), I discovered that indeed Gold had taken over port 443. As soon as I kicked it off, I could launch the nginx proxy container with `-p 80:80` *and* `-p 443:443`. So now the proxy knows where to find the key and cert, and is trying to load the subdomain over https, but is getting connection refused. The proxy docs say this might happen "if the container does not have a usable cert" so now I have to find out what is wrong with my cert?

Is it permissions? It's usually permissions. Nginx might reject things if they key is world readable? Tried `chmod 600` for the key and cert. No dice.