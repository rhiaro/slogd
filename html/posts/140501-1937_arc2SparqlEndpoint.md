<!---
published: 1st May 2014, 19:38
last-modifed: 1st May 2014, 20:38
type: BlogPost
tags: semantic web,triplestores,rdf,sparql,blog,slogd,hacking,learning,arc2
list: doing
-->

# ARC2 SPARQL Endpoint

So Slog'd got stuck for a little while because the fast, nice-looking, somewhat magical SPARQL endpoint provided by ARC2 stopped working for no discernable reason.

I thought I'd try leaving it alone for a few weeks to see if it started working again by itself, but alas, it has not.

Everything is fine until I try to query for a specific predicate. (Specific objects or subjects are fine). The query runs, it just returns no results. I know the data is in there, because I can get it out with less specific queries. Also because I can see it all in the MySQL database on which it is based. _When I left it, it was working fine._

I'm going to kill the database and set it up again.

I did this by - and oh, it was joyous - going into the database settings and appending '2' to the name of the database. I then reloaded the endpoint page, and it set everything up by itself :)

I inserted two triples, and successfully queried for a specific prefix. So, it works. I wonder what will happen if I dump all my old data back in there? (I validated the raw file with all the triples in RDF/XML, and they're fine).

I inserted the rest the rest: ```LOAD <path/to/rdf.rdf> INTO <>```

Ran a test query, aaaand... it's fine.

So what the hell was wrong with my other database? Perhaps I'll never know...