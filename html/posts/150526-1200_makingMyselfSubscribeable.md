<!---
tags: slogd,hacking,indieweb,readers,subscribing,update,delete,posts,feed,stream
-->

# Making myself subscribeable

For too long have pages on my site been an unsubscribeable mess. I rely on POSSEing to twitter as my distribution mechanism because it's basically impossible to subscribe to any of my 'feeds' with an indiereader due to how my pages are laid out at the moment. My homepage is three lists, todo, doing and done. This has its roots in website-as-CV (where my site was an overview of things I've done and was doing in order to persuade people to hire me). My blog was separate, but sometime in 2011 I attempted to follow the "show don't tell" mantra and merge the blog and CV concepts by tagging all my posts with 'todo', 'doing' or 'done' and displaying them grouped by overall CV-useful concepts like degrees I'd done, projects I'd worked on and job positions I'd held. (I was also going through a bit of a kanban phase, in case you hadn't noticed). That was alright for a while, but it was horrible to maintain and update (I was parsing my blogger RSS feed because blogger templates are *ass*, re-sorting and displaying), and the focus was on longer articles that I wasn't writing many of, and when I was it was because I needed to update my 'CV'. It was also way too green. I started [Slog'd](http://rhiaro.co.uk/tag/slogd) to move away from blogger, play with linked data for 'normal' web applications, and generally tidy things up. I was still planning on sticking to the todo/doing/done idea.

Then indieweb came along and suddenly I wanted all of my tweets, and everything else I was publishing, on my own site, and without some seriously good UI (skills I don't have) they would have flooded anything CV-useful. To manage this so far, I kept the three lists on my homepage, and published a lot of short posts without assigning them a list, so that they only appeared on /tag pages. But they're invisible to anyone who only looks at my homepage and hasn't seen a POSSE copy. Which has been fine, because I mostly post short notes for myself, and anyone else who cares is following me on twitter where they'll see them anyway.

But times are a-changin', and I want people to be able to subscribe to me without having to use twitter. Or at the very least, make more of my posts easily discoverable from my homepage off the bat. Also I publish a *lot* of crap at the moment (food, vague checkins) which some people are interested in (my mum, people wondering whether I'm there yet) but which most people *definitely* don't want to see; I also post about a variety of topics - different people are going to be interested in my tech posts vs academia vs travel vs writing... so I need to allow granualar subscriptions in a few different ways.

## indie readers

Reader technologies are fairly new in indieweb, a lot of existing use cases are fairly straight forward, and a lot of stuff is still being figured out. Plus, I'm far from getting my head around how it all works and haven't implemented any kind of subscription mechanism yet, so bear with me.

Currently, as I understand it, if you subscribe to a site with a reader like [Woodwind](http://reader.kylewm.com), the reader goes off to the sites you have told it to subscribe to, parses the microformats at that URL and displays the posts to you in its own interface. Every time you load the reader, it refetches your subscriptions, and updates what you see, based entirely on what is displayed on the homepage of the site (or a specific URL) you have subscribed to. So if there's a new post on the page, the new post shows in your reader.

[Monocle](http://monocle.p3k.io) supports subscribing to sites via Pubsubhubbub, which means the site notifies a hub when something has changed and the hub notifies the reader.

Because my homepage is a mess of lists and future-dated posts (for events, travel plans), and not all posts even end up there, my site doesn't play well with microformats readers. I think it'd work okay if I implemented PuSH, but I haven't got round to that yet.

### Updates and deletes

A question that's come up recently is how to notify readers if a post is updated or deleted. Most people page their feeds, so the only way for a microformats reader to know a post has been edited is if it was pushed to the top of the site's homepage (which most people probably don't want to do for minor edits or tweaking of really old posts). If a post is deleted, the reader would have to need to explicitly try to fetch the post and discover a 410 to know. If a reader has cached a post, there isn't a way for a site to tell the reader it's gone. (is there in PuSH?)

This is where small, inconspicuous, activity-style `update-of` or `delete-of` posts could come in handy. A new thing for a reader to find on the homepage so it knows there's a change and can re-fetch posts it might have cached, without having to push the actual post affected back to the top of the homepage.

### Mentions

Readers, as feeds you see on silos, may want to show interactions with posts alongside the post itself. Eg. "5 likes, 2 replies, 3 reposts".

If someone I don't follow comments/likes/reposts a post of someone I do follow, this is likely to be displayed where the post originated, so the first time the reader needs to fetch the post it can fetch this information too. However, if someone I don't know subsequently interacts with the post, I'm not subscribed to anything that would allow my reader to fetch that change. The reader would need to re-fetch and re-parse all posts every time I checked my feed, and couldn't cache anything (??? maybe they already work like this and it's fine?).

But if, included in the small, inconspicuous, activity-style feed on my subscription's homepage mentions were also included ([lots](http://aaronpk.com/mentions) [of](http://ben.thatmustbe.me/activity) indiewebbers have a such mentions pages separately anyway), readers would be able to see these at once and either quietly update their cached posts, or push these new interactions to the feed the user sees (maybe a setting for this).

## Homepage

With these points in mind, here are things I'm changing on my site:

* One main feed of posts that defaults to notes and articles (which includes replies, RSVPs, events, checkins, travel plans, likes, reposts and bookmarks) on the homepage.
* Options to switch off likes, bookmarks, travel plans and checkins (separately).
* Options to switch on food posts and follows, and when I have them, exercise, sleep, code commits and music listens.
* These options will just live in URL params, so you can subscribe to a version of my homepage according to URL params, without needing to subscribe to separate pages (it works like this, right?). So you click things to customise what you want to see of my homepage, then copy the resulting URL into your reader.
* A smaller feed of updates, deletes and mentions of posts as filtered by above options.

### And some smaller, bug-fixy things

* For goodness sake, add pagination. Actually thinking about defaulting to only showing posts from the current day, might have a play with this.
* Make sure all markdown gets parsed out to html in all views, sometimes Woodwind just displays collapsed markdown for my notes.
* Fully expand all posts; currently articles are only headings.
* Sort out published dates. Even though I do store correct published date for everything, for posts with start and end dates, in certain views, the start or end date gets rendered as the published date. It's just a bit screwed up and needs fixing.

### Curated feeds

I'm still going to have some special feeds that people can subscribe to if they're interesting in specific subsets of things I post (that aren't necessarily just groupable by tag).

* Travel: all posts tagged 'travel', all events and RSVPs, all travel plans, all specific checkins.
* Where: most recent checkin (specific or vague).
* Calendar: all RSVPs and events, all travel plans. Sorted by published date, or event date..??
* People: follow posts and posts where I mention a homepage URL in any context.
* Vegan: all food posts, all posts tagged 'food', all recipes, all restaurant reviews, all checkins to restaurants.

Tag pages and /likes, /reposts and /bookmarks will also be individually subscribeable.


questions:

* aaronpk, your homepage feed doesn't include likes/bookmarks? so I'd have to subscribe explicitly to your likes page to see these? any particular reason? does it include reposts?
* does PuSH let you notify hub of changes to existing post? or is it just changes to whole feed?
* how does pagination in readers work?
* do existing indiereaders actually cache, or fetch feed every time?