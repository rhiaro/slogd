<!---
published: 4th January 2015, 05:00
last-modified: 4th January 2015, 05:00
type: BlogPost
tags: sleep,polyphasic,sleep cycle,quantified self,life
list: doing
-->

# Polyphasic sleep

I've been thinking about adopting a polyphasic sleep cycle for some time. I really just want more hours in the day. In theory I like mornings, but I usually struggle to get up. When I do, it's very rewarding. Usually I'll drag myself out of bed an hour before the first thing I have to be at (meeting, seminar), which could be anywhere between 0830 and early afternoon. If I don't have anything specificially scheduled, I won't set an alarm, figuring I trust my body to wake up when it's ready. When I do this, I usually sleep a solid 10 hours and feel pretty shitty when I wake up.

Regardless of when I get up, I start to crash around 1500. By crash, I mean suddely find myself staring, zombielike, at Buzzfeed and Facebook and stuck in a constant cycle of clicking mindless linkbate and scrolling through social feeds. Ugh. If I catch myself, I'll probably call it a day and go to do something physical like make food or clean. Sometimes (usually only possible if I have some decent food in my office) instead I push through the zombie and hit a second wave of productivity in the early evening. I start to wake up, feel more alert, and can get sucked into coding or researching something until the early hours of the morning.

That feels great, but sometimes I have to make myself stop work before midnight so I can go home and get enough sleep to be awake for my first scheduled thing the next day. Sometimes I fail at stopping, only get a few hours sleep and feel like shit the next day.

Occasionally I'll find time to take a nap if I'm feeling completely out of it, but naps often overrun by many hours, and then I hate myself for 'wasting' even more time asleep.

So I want to figure something out that lets me fit in my nighttime productivity, and see the sunlight, and not oversleep or undersleep. Setting a rigid timetable for sleep and naps will help with this, I think. If I know I'm trying to be consistent, and naps are purposeful and scheduled, my willpower should kick in get me up when I need to be. In theory, anyway. I know these things look much different in the cold hard light of day[*][#cold-hard-light] than they do from a warm, comfy bed... just 5 more minutes...

I'm aware that transitioning to polyphasic is going to be a nightmare, but if I keep putting it off until I don't have anything important to do, I'll never do it. Right now, I know I'm not travelling until at least March, so I've got time to adjust on my own schedule (and figure out how to handle conferences when I come to it). I've pulled all-nighters and had remarkably productive periods of sleep deprivation before, so I figure I'll cope. Oh, to be young. I read a lot of people have problems with actually being able to fall asleep for naps, but I have this superpower whereby I can go to sleep easily on any surface, in any situation, with any background noise, so I'm not worried about that.

## The schedule

Turns out the reason some people can get away with fewer than eight hours is because there are different types of sleep, and not all of them are necessary to being alert when you're awake. So the 'science' is to cut out the useless sleep and train your body to fall straight into useful sleep. This is possible by paying attention to your natural circadian and ultradian rhythms, and reading websites by people who have already figured this stuff out.

Sleep cycles in 1.5 hour blocks for most people, and the useful types are SWS ([Slow Wave Sleep](http://en.wikipedia.org/wiki/Slow-wave_sleep)) and REM ([Rapid Eye Movement](http://en.wikipedia.org/wiki/Rapid_eye_movement_sleep)). Knowing when and how long for these are likely to occur, over the course of a 24 hour period and in relation to each other, can help you to time your sleep sessions to maximise them.

[The Everyman 3](http://www.polyphasicsociety.com/polyphasic-sleep/overviews/everyman/) schedule consists of 4.5 hours sleep broken into a 'core' 3.5 hours, plus three 20 minute naps at approximately 4-6 hour intervals. I read that SWS happens best around dusk, so the core sleep should be around 2100 to 0030, and naps (when you get your REM) at 0410, 0810 and 1440. This is flexible of course, and once you've forced yourself into a pattern you can adjust by listening to your body (eg. move your nap an hour earlier or later if you're consistently tired/awake at certain times). It also needs to be adapted into the schedule of daily life. 

There are versions pre-planned for people with 9 to 5 jobs, but my schedule can be pretty irregular. There are certain things I have to do on certain days at different times. I think the day I'm going to have the most problem fitting around is Friday, when I have a 0930 seminar. Anyway, term hasn't started yet so most things aren't kicking off for another one to two weeks, so I have time to figure it out.

So for now, I've put the Everyman 3 schedule in my calendar, and will ease into it. I might make my core sleep longer to start with, depending on how I feel when I wake up after 3.5 hours, and maybe have 40 minute naps instead of 20. I'm probably also going to shift the schedule mentioned above later by 2 hours, as I already know I won't be able to go to bed at 2100 any night.

## Logging

For now I'm going to dump my sleep sessions into [this google calendar](https://www.google.com/calendar/embed?src=s2nceddftg5vclefkgdoc9is6o%40group.calendar.google.com&ctz=Etc/GMT ) for quick reference alongside the rest of my schedule, but I'll also start tracking through this site at /llog when things are up and running properly.

## Other stuff

Diet and exercise are obviously important, and have an impact on how well one can sleep. Part of altering your sleep cycle is retraining your body to do things like digestion at different times. Sensible advice I've read so far seems to be to have a main meal at least two hours before the core sleep, and use breakfast to train your body to expect to be awake. Also not to exercise too close to core sleep, but exercising or eating right before naps is okay.


<span id="cold-hard-light">*</span> Metaphorical cold hard light of day. Most of these decisions I've made around 4am during a Scottish winter, so I have to use my imagination.