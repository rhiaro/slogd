<!---
published: 22nd March 2015, 15:00
type: BlogPost
tags: microformats2,rdf,linked data,json-ld,mf2,owl,indieweb,hacking,socialwg,activitystreams,foaf,vcard,ontologies
list: doing
-->

# Microformats2 RDF Ontology

I'm serializing the [microformats2](http://microformats.org/wiki/microformats2) vocabulary into RDF; [repo on github](https://github.com/rhiaro/mf2rdf). This post is notes as I go along.

Useful notes by [Tom Morris](https://tommorris.org/) about [mf2 to RDF mapping on the microformats wiki](http://microformats.org/wiki/microformats2-parsing-rdf). I updated the namespace to use the one suggested there (`http://microformats.org/profile/`).

Everything starting with `h-` looks like it's a class. Typically RDF classes are capitalised so this makes me cringe, but oh well.

Well shit, there are a lot of properties. Seventy unique properties. I didn't realise. On the wiki most don't have a description, many are duplicated in the list, and none of them seem to have their own description page (and thus no URIs to refer to them with). Pasting them into a spreadsheet was the quickest way to de-dup and alphabetise them. Now I'm working through one at a time, adding descriptions and mapping them to existing properties in [FOAF](http://xmlns.com/foaf/0.1/) (because many are obvious), [VCard](http://www.w3.org/2006/vcard/ns#) (because many (all?) are derived from this), [ActivityStreams 2.0](http://rawgit.com/jasnell/w3c-socialwg-activitystreams/extended-vocabulary/activitystreams2-vocabulary.html) (because SocialWebWG) and hesitantly [Dublin Core](http://purl.org/dc/elements/1.1/) and [SIOC](http://rdfs.org/sioc/types#. Where they map to something well-described in RDF, I don't bother with `rdfs:comment` and `domain` and `range` of their own, but I'll add for those with no good `sameAs` mappings.

* Is `dt-reviewed` same as `dt-published`? If not, why do reviews get special treatment with regards to differentiation between when they're written and published, and nothing else does?
* Is there an official definition of `e-content`? Couldn't find one on the wiki, so made one up that is compatible with AS2.0 content, with additional caveat about including markup, which is explicit on the mf2 wiki: "The contents of something like a post, including markup and embedded elements."
* Are `e-description` and `e-instructions` sub-properties of `e-content`?
* `e-description` is only listed as a property for `h-product` and `h-review`. I feel sure other things should be able to have descriptions containing embedded elements.

I keep getting caught out by AS2.0 terms that are in the JSON-LD document but are actually deprecated (the JSON-LD contains no information other than property and class names and sometimes types; I have to remember to check the written docs). Eg. `as2:author` -> `as2:attributedTo`.

* I couldn't find an AS2.0 equivalent property for `category`. Am I missing something? Oh wait, `tag` will do it.
* `p-count` is only officially defined for counting `h-review`s in a `h-review-aggregate`. Seems like we ought to be able to generalise this for collections of anything.
* Uhhh `p-description` is only listed as a property for `h-event`, which is inconsistent with `e-description`. Nobody said they should be consistent, but this doesn't seem right. Also how does it differ from `p-summary`?
* Wiki says `p-education` should contain a nested `h-card` (of school and location). Doesn't that mean it should be an `e-` or have I misunderstood that completely?
* It also says `p-education` is "an education `h-calendar` event" but `h-calendar` doesn't exist, it's `h-event`. Probably just a typo? Ditto `p-experience`.
* ActivityStreams2.0 doesn't have a `name` property for `Actors` (only `displayName`). Interesting.
* Wiki says `label` 'new in vCard4' but vCard4 says it's deprecated, and in any case was for address labels, so not sure what use this is in mf2.
* What's `p-note` for specifically?
* Why is `p-reviewer` not just `p-author`?
* Maybe I just don't know how to read the vCard4 spec, but the mf2 wiki says `sex` and `gender-identity` are part of it, but all I can see is `hasGender`.

AS2.0 has `rating` but it's explicitly defined as "a non-negative decimal number between 0.0 and 5.0 (inclusive) with one decimal place of precision". MF2 says it's a number between 1 and 5. I think even less specification is better, as ratings come in come in many forms. Anyway, because I'm not venturing into SKOS, I'm `owl:sameAs`ing them for the time being.

Hmm, I just put the domain of `p-skill` as `h-resume` but the person has the skill not the resume... but the resume lists the skill... whatever, moving on.

