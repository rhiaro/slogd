<!---
tags: walk,life,runkeeper,north berwick,travel,edinburgh
-->

# The Long Walk

[Rikki](http://rikkiguy.me.uk) and I walked from Edinburgh to North Berwick, almost 30 miles, in 11 hours. This is the longest walk I've ever done in one go, and I pushed my legs and feet to the absolute limit. Here's the [GPS trace on Runkeeper](http://runkeeper.com/user/rhiaro/activity/634961126).

![Trace from Runkeeper](http://rhiaro.co.uk/photos/1508walk/map.png)

We departed Edinburgh at 2am, and walked straight to Portobello.

![Entering Portobello](http://rhiaro.co.uk/photos/1508walk/01_portobello.jpg)

From there, we just followed the coast. Next stop: Musselburgh.

![Musselburgh](http://rhiaro.co.uk/photos/1508walk/02_musselburgh.jpg)

The sun started to make an appearance around 5am, albeit on the other side of clouds. Looking back at where we'd come from, the distinctive shape of Arthur's Seat and the Crags:

![Looking back at Holyrood](http://rhiaro.co.uk/photos/1508walk/03_holyrood.jpg)

And forward to the decommissioned power station at Port Seton:

![Looking forward to Port Seton](http://rhiaro.co.uk/photos/1508walk/04_powerstation.jpg)

From here, Morrison's Haven:

![We were there](http://rhiaro.co.uk/photos/1508walk/05_morrisonshaven.jpg)

Murals welcomed us to Prestonpans.

![Entering Prestonpans](http://rhiaro.co.uk/photos/1508walk/06_prestonpans.jpg)

![Scottish Diaspora](http://rhiaro.co.uk/photos/1508walk/07_diaspora.jpg)

![John Muir](http://rhiaro.co.uk/photos/1508walk/08_johnmuir.jpg)

Above is John Muir, whose [Way](http://johnmuirway.org/) we followed for most of the walk.

Undergoing deconstruction, the old power station:

![Power station](http://rhiaro.co.uk/photos/1508walk/08_powerstation.jpg)

And some very personalised graffiti in Port Seton:

![Amy G](http://rhiaro.co.uk/photos/1508walk/09_amyg.jpg)

Finally the rain let up (did I mention it had been raining for the entire first few hours?).

![Sun](http://rhiaro.co.uk/photos/1508walk/10_sun.jpg)

By now, the power station and Holyrood are recognisable silhouettes in the distance. Our feet were starting to ache slightly.

![Power station and Holyrood](http://rhiaro.co.uk/photos/1508walk/11_powerstationholyrood.jpg)

But beautiful beaches and encouraging signs awaited us.

![Beach](http://rhiaro.co.uk/photos/1508walk/12_beach.jpg)

![North Berwick this way](http://rhiaro.co.uk/photos/1508walk/13_sign.jpg)

Soon we were deep into golf course territory.

![Golf Course](http://rhiaro.co.uk/photos/1508walk/14_golf.jpg)

My knees were saying things like "ohai I see you've been walking for several hours now, might want to get that looked at."

![](http://rhiaro.co.uk/photos/1508walk/15_aberlady.jpg)

So naturally we decided to take a 3 mile diversion via the Aberlady nature reserve, rather than head straight down the road to North Berwick.

![](http://rhiaro.co.uk/photos/1508walk/16_bridge.jpg)

This paid off with gorgeous empty beaches.

![](http://rhiaro.co.uk/photos/1508walk/17_reserve.jpg)

![](http://rhiaro.co.uk/photos/1508walk/18_reserve.jpg)

![](http://rhiaro.co.uk/photos/1508walk/19_beach.jpg)

![](http://rhiaro.co.uk/photos/1508walk/20_beach.jpg)

![](http://rhiaro.co.uk/photos/1508walk/21_stack.jpg)

And interesting things from the sea.

![](http://rhiaro.co.uk/photos/1508walk/2223_seathings.jpg)

![](http://rhiaro.co.uk/photos/1508walk/24_beach.jpg)

Then we lost the path, scrambled through undergrowth, aggressive plantlife shredded my legs (why do I always head out in shorts?) and we emerged on a golf course. Bemused golfers advised us to get a bus, and directed us to the road. We had other plans, headed through private golf course residents land, with fancy houses.

> "Look at those children, running around like they still have feet." - Rikki

We figured that if we were caught somewhere we weren't supposed to be, the worst that could happen is they'd take is to an office somewhere and... sit us down... in chairs!! Didn't seem so bad.

But we limped on, to North Berwick at last.

![](http://rhiaro.co.uk/photos/1508walk/252627_northberwick.jpg)

Except the distance between the 'welcome to North Berwick' signs and the town itself was eternal. We were both feeling a bit sick at this point, on top of all lower-body pain. But eventually we made it, and celebrated by SITTING DOWN.

![](http://rhiaro.co.uk/photos/1508walk/28_sitting.jpg)

Also chips.

![](http://rhiaro.co.uk/photos/1508walk/29_chips.jpg)

We slept on the grass overlooking the beach for a couple of hours. Then limped to the train station. Some old ladies with walkers got stuck behind us in the street.

30 minutes and £4.10 later we were back in Edinburgh, via the sensible route.

![](http://rhiaro.co.uk/photos/1508walk/30_train.jpg)

The last book I finished was [The Long Walk](http://dbpedia.org/resource/The_Long_Walk) by Stephen King. That's pretty much all I could think about on this journey.