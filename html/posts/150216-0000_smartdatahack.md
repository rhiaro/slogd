<!---
published: 16th February 2015, 00:00
type: BlogPost
tags: events,hackathon,hack,smart data hack,ilwhack,innovative learning week,open data,data,edinburgh,rsvp
list: todo
-->

# Smart Data Hack

I'm again helping to organise the University of Edinburgh [Smart Data Hack](http://smartdatahack.org), which is happening next week, 16th - 20th February. It's an event for undergraduates during the university's Innovative Learning Week, during which they get to learn loads of stuff they wouldn't normally see in class, hack around with datasets from various sponsors and supporters, eat tons of free food and hopefully win prizes.

If you're interested in getting involved, check out the [Smart Data Hack website](http://smartdatahack.org)...

* students can sign up and see the challenges and schedule
* mentors can find out how to help out (and get free lunch) 
* sponsors can find out how to help us feed the students and mentors