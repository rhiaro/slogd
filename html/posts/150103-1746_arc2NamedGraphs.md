<!---
published: 3rd January 2014, 17:46
last-modified: 3rd January 2014, 17:46
type: BlogPost
tags: hacking,slogd,blog,post,named graph,contexts,linked data,semantic web,pedantics
list: doing
-->

# ARC2 Named Graphs

Dimly aware that ARC2's triplestore uses named graphs, I decided to check where all my triples are at the moment. Turns out any inserted from php scripts are inside a graph whose URI is the file path to the script, eg. `<http://localhost/testclasses.php>`.

Maybe I should think about more proactively sorting my graphs.