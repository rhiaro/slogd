<!---
published: 12th April 2015, 19:47
modified: 12th April 2015, 22:19
type: BlogPost
tags: hacking,indieweb,activitystreams,activitystreams2.0,microformats2,mf2,post types,activities,feed,social web
list: doing
-->

# Post and Activity types

Recently I've been trying to wrap my head around the implicit vs. explicit post types debate (in the [Social Web WG](https://www.w3.org/wiki/Socialwg/), [indieweb community](http://indiewebcamp.com), and elsewhere). Here is some analysis of the different perspectives, and report on my own experiences so far.

## tl;dr

I think each side of the debate about explicit vs implicit types are misaligned, due to conflation of Objects and Activities. I think Objects (like blogposts) needn't have explicit types, but Activities (like the act of creating a post) should do.

Thanks to [Tantek](http://tantek.com), [Aaron](https://aaronpk.com), [elf Pavlik](https://wwelves.org/perpetual-tripper/), [Chris](http://dustycloud.org) and everyone whose conversations I've listened in on in #social and #indiewebcamp IRC channels for discussing this stuff with/around me.

## Indieweb: focus on UI

Indieweb development starts with thinking about UI. The reason for thinking about different kinds of posts comes from a desire to display some posts differently to others. But rather than declaring them outright, one can [infer how to display them based on the properties of a post](http://indiewebcamp.com/posts#Inferring_post_kinds_from_properties) (see also [IndieWeb Posts by aaronpk](http://aaronparecki.com/articles/2012/10/22/1/creating-content-on-the-indie-web#indieweb-posts).

> "explicit post/object types are architecture-centric design, rather than user-centric design" - tantek, [#indiewebcamp irc](http://indiewebcamp.com/irc/2015-04-08/line/1428522815837)

### Posting UI

Another rationale against explicit post types is that it encourages posting clients to constrain users regarding what kind of posts they can create. 

> "...content types dictate the kinds of content you actually allow people to create with your software" - benwerd, [The tyranny of content types](http://werd.io/entry/51c72566bed7dee673ee2052/the-tyranny-of-content-types-indiewebcamp)

Most mainstream social networks now have a generic posting interface, that allows one to add things like photos, videos, people tags, etc, *without* forcing you to choose what type of post you want to make *first*.

### Software architecture

Some people started with explicit post types and ran into problems when structuring their software around this idea. TODO: Citation from aaronpk in irc in March/April that I can't find right now.

### URL design

If you include a slug for a post type in your URLs and decide to change the post type, this is *definitely* a pain. I figured this pretty early and [dropped it from Slog'd](https://bitbucket.org/rhiaro/slogd/commits/61898eb26568e0945af7855c4831291138775049). See also: [Update URL Scheme issue for p3k](https://github.com/aaronpk/p3k/issues/86).

On the other hand:

> "but including type [in URL] because I like clustering by type in a day" - tantek, [#indiewebcamp irc](http://indiewebcamp.com/irc/2015-02-20/line/1424470947652)

... which I think relates to [querying, discussed later](#querying).

### Clarification: content-based vs intent-based post types

Often [photo-type-post vs article-type-post is thrown up](http://indiewebcamp.com/irc/2015-04-08/line/1428531822689): ie. if you start with an article, then edit it to add a photo, does the post type change? **I completely agree that typing based on content doesn't make sense here**. When I talk about post types, I'm referring to types for *like*, *bookmark*, *repost*, *log/metrics* (eg. food, sleep), where they may look like a note in terms of content, but *aren't*. I display different icons for all of these at the moment, and you'll note that twitter for example displays retweets differently in the timeline.

### Post all the things

Conclusion: in the indieweb community, so far, everything created is considered a `post` (a `h-entry` in microformats), and display differences can be inferred from properties of the post.

* an *article* is a post with a `title`.
* a *like* is a post with a `like-of`.
* a *reply* is a post with `in-reply-to`.
* etc.

## ActivityStreams2.0

ActivityStreams2.0 has explicit [object types](http://www.w3.org/TR/activitystreams-vocabulary/#object-types) and explicit [activity types](http://www.w3.org/TR/activitystreams-vocabulary/#activity-types). [It's easy to conflate them](https://github.com/jasnell/w3c-socialwg-activitystreams/issues/94); I think this is what is happening when discussing types in AS2 with indieweb people, and I certainly had this problem for a while.

**Taking an action generates an `Activity`, which might `result` in an `Object` (like a post).**

### Beyond posts

Contrary to indieweb stuff so far, AS2 allows for the idea that *not every action on the social web results in a post*. Having been immersed in indieweblogworld for ages, this was briefly a hard sell, but then I started to think of social actions (*activites*) I might want to take - that it's reasonable to want to publish in a feed - that I probably *don't* need to create a post for. Eg:

* edit a post (or a wiki page)
* git commits
* follow someone
* reading/viewing/watching something
* blocking someone / reporting content
* joining or leaving a group

I don't publish any of these activities on my site yet. There are some activities I *do* publish with posts (or plan to imminently), that don't *necessarily* (I might need convincing on some fronts) need to be posts:

* [likes](http://rhiaro.co.uk/likes)
* [reposts](http://rhiaro.co.uk/shares)
* [bookmarks](http://rhiaro.co.uk/bookmarks)
* eating, drinking, sleeping (See [Llog](http://llog.rhiaro.co.uk)).
* exercise, specifically running and yoga
* listening to music
* acquiring physical objects
* arriving at a destination (aka checkin)

### Activities vs objects

An `Activity` can be generated for any action. 

* Publishing a blogpost could generate in a `Create` activity, for which the `object` is the post created. 
* Bookmarking a page could generate a `Save` activity, for which the `object` is the page and the `result` is [a post about my having bookmarked the page](http://rhiaro.co.uk/2015/04/1428850017). 
* Editing a post could generate a `Update` activity for which the `object` is the post being edited, but there is **no** new post as a `result`.
* Liking a post could generate a `Like` activity for which the `object` is the post being liked, but there is **no** new post as a `result` (if you're into that sort of thing, so far [I'm not](http://rhiaro.co.uk/2015/04/1428671289)).

**I strongly feel that all `Activity` should have URIs** so that if it doesn't `result` in a post, it can still be interacted with (eg. you can *like* my *edit*). (However, this presents another point for confusion: when do I like a post, and when do I like *the fact you published it* (the create activity). Anyway, this is for another time..).

Since activities are *generated* as a result of an action, *not explicitly created* by a user (unlike objects), arguments for not explicitly typing them fall down:

* you're unlikely to need to edit an activity, causing a need to change its type.
* post creation UIs have nothing to do with this. Instead, UIs for interacting with posts such as [webactions](http://indiewebcamp.com/webaction) - but [typing in these seems to be acceptable](http://indiewebcamp.com/irc/2015-04-08/line/1428523029300).

If you want to generate an activity stream from your actions, we do get an additional argument *for* explicit types of *objects*: it makes it much easier to generate a suitable `Activity`.

### Querying

Explicit types for objects *or* activities make it easy to query by type. Inferring types - particularly from a combination of properties - complicates this a lot. I recently temporarily dropped quicklinks to /articles and /notes in Slogd after removing `sioc:BlogPost` and `sioc:MicroblogPost` as explicit post types made it suddenly more complicated to get them. Given a stream of many people's activities from all over the place [how do you filter for specific things?](http://socialwg.indiewebcamp.com/irc/social/2015-04-11/line/1428769941847)

### Human-friendly display

Displaying an activity (the fact that someone did something) is different to displaying an object. You might want to form a sentence ("alice likes bob's tweet") without caring what the properties of the `object` or possible `result` are. It's easier to figure out how to display it if it just has a type, without needing to follow links to the `object` or possible `result` to infer what's going on.

### Generating activity streams

I guess... If you want to take a bunch of objects and generate an activity stream from them, and they don't have explicit types, it's totally up to you (an implementation detail, aka plumbing) how you decide what types the corresponding `Activity`s have.

I'm not sure if it would be nice to have something in the AS2.0 spec to help with consistently deciding how to infer *activity types* from *object properties*. Maybe it doesn't matter. 

...Wait, is this [ISSUE-4](http://www.w3.org/Social/track/issues/4)? Lack of explicit mention of activities or objects makes it hard to tell.

## Conclusions

* Every creation of a post should (can) generate an `Activity`.
* Not every `Activity`-generating action needs to result in a post, but it's cool if it does.
* `Activity`s need types. `Object`s (like posts) do not (necessarily).
* If you don't want to generate a stream of all of your possible actions, but rather a feed of objects (as appears to be the case for indiewebbers so far) then you don't need activities, and this conversation is basically irrelevant... but if you think someone else might generate an activity stream from your objects, you're at their mercy regarding `Activity` types.

## Next?

1. I should probably generate an AS2.0 feed from my posts.
2. Start integrating non-post-resulting activities into it.

## Other notes

I went through the lists of `Activity` types and `Object` types in [current draft of AS2.0 vocab](http://www.w3.org/TR/activitystreams-vocabulary/) and did a first pass at things I am using, might use and probably won't use. I'll publish this in another post.

I drew lots of diagrams of how I'd turn my different types of posts into `Activity`s based on their properties (how I'd infer *activity types* from *object properties*). I'll also type these up for another post.

`Activity` is a subclass of `Object` in AS2. Practical and conceptual differences could be discussed in more depth, but include things like [ephemerality of activities vs objects](https://lists.w3.org/Archives/Public/public-socialweb/2015Apr/0046.html).

Further discussion (possibly): [https://lists.w3.org/Archives/Public/public-socialweb/2015Apr/0048.html](https://lists.w3.org/Archives/Public/public-socialweb/2015Apr/0048.html)