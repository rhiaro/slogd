<!---
tags: twitter,like,indieweb,bridgy,slogd,hacking,webmention,https://kylewm.com/
-->

Let the records show that I will never click or tap the twitter heart icon. Apparently it explodes or something? Dumb. All twitter likes are henceforth (finally) [*fully automated*](https://bitbucket.org/rhiaro/slogd/commits/67c1fe366988a9fe13cd08fd9c07899508d14fc6) via [bridgy](https://brid.gy).

(And a million thanks to [kylewm](https://kylewm.com/) for spontaneous inclusion of error logs in bridgy profile pages!)