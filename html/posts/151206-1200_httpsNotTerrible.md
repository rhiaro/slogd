<!---
tags: ssl,hacking,cpanel,letsencrypt,cert,tls
-->

# HTTPS: Not a terrible experience

## Shared hosting with cPanel

Installed letsencrypt on my local machine (a Chromebook running Ubuntu in crouton, didn't melt):

`git clone https://github.com/letsencrypt/letsencrypt`

`cd letsencrypt`

Ran in manual mode:

`./letsencrypt-auto certonly --manual`

Followed instructions for domain validation (dealt with rogue .htaccess file that stopped .well-known being accessible).

`fullchain.pem`, `chain.pem`, `cert.pem` and `privkey.pem` were generated into `/etc/letsencrypt/live/mydomain.tld`.

cPanel -> SSL/TLS Manager.

Uploaded `privkey.pem` to Private Keys. Uploaded `cert.pem` to Certificates.

Manage SSL Hosts -> Browse Certficates -> picked the cert (it prefilled domain automatically). First two boxes prefilled with private key and cert. Pasted CA bundle into the third box from [the lets encrypt site](https://letsencrypt.org/certs/lets-encrypt-x1-cross-signed.pem.txt).