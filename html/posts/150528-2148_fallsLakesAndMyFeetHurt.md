<!---
published: 28th May 2015, 21:14
tags: travel,croatia,korenica,hitchiking,plitvice,lakes,national park,waterfall,hiking,hostel
-->

# Falls, lakes, and my feet hurt

Last night I arrived in Korenica (the bus showed up 35 minutes late; 84 kuna, somehow arrived on time 2 hours later, really spectacular mountain scenery), and successfully navigated to Falling Lakes hostel. I thought Tchaikovsky in Split was the best, but Falling Lakes is even nicer. Croatia could really teach the world a thing or two about how to do hostels. I was greeted by Irena, who helped me figure out buses and gave me a ton of advice about how to explore Plitvice. The hostel has a lovely kitchen, plus fresh herbs in the garden, so I immediately went to Konzum to get things I could cook. The hostel is so nice and I was dreading the rush of picking up my rucksack and catchign the bus to Zagreb after walking all day. Then suddenly I realised I could just book another night in Korenica and go to Zagreb in the morning. My host in Zagreb was happy with the change of plan, and suddenly that was a weight off.

I didn't get up quite as early as I'd planned, but still early enough that I didn't want to wait for the official (35 kuna) hostel ride to Plitvice. It's about a 15 minute drive, and the highway is not safe to walk on, with no footpaths, sometimes steep banks, many trucks and everyone going really fast. But I hadn't even made it out of Korenica before I was picked up by a middle aged couple from Zagreb who were very interested in all of my travel. They dropped me at Gate 1 at 0900, which threw off my well thought out agenda that began at Gate 2.

Entrance to the park is 80 kuna with a student card, and the ticket includes one boat ride and one mini-bus-thing ride. Maps are 20 kuna, but fortunately Irena gave me one at the hostel. I did some dynamic replanning, figured there was no way I wasn't going to have to cover a stretch twice if I wanted to leave from Gate 2, and headed for the boat. This was supposed to be the last thing, and I considered walking the length of the lake and getting the boat for the repeat of that later, but then it arrived, so I hopped on. I'm *really* glad I did, because when I went past again later there were three boats worth of people waiting. That took me to near Gate 2, and the start of the main bit of world-famous multi-tiered waterfalls. I followed the path.

I could have done with learning a few Japanese phrases, like "if you must walk this slowly at least stop zigzagging", "this walkway is too narrow for you to stop and look at every little thing" and "put that selfie stick away you're going to have someone's eye out". There were a lot of elderly Japanese tourists who walked very slowly and had no regard for the high risk of sending people flying into the waterfalls when they stopped suddenly and started brandishing cameras. Sigh.

The waterfalls were impressive and everything, but I had a bit of human overload. Not just the tourists (I know they were all there for the same reason I was), but the work that had gone into making the surroundings traverseable. The walkways were all wooden, and very tasteful; often stretching across whole lakes, low to the water, or steps directly above a waterfall so you were always really close.

![be gone, walkway](http://rhiaro.co.uk/photos/1505croatia/waterfallsteps.jpg)

I couldn't help wondering what this place would be like without the intervention. How many of the waterfalls and streams were actually there as a result of some footpath sculpting? I enjoyed more when the walkways were flooded and the water was trying to take back its space.

![be gone, walkway](http://rhiaro.co.uk/photos/1505croatia/flood.jpg)

Following Irena's advice, I took a less-trodden trail, from point 1 to point 3 (purple on the map) which led deeper into the woods of the park, away from the main area. I'd have loved to go via point 2, which goes way into the park through small settlements that are still occupied, but I didn't think I had time for the full 24km. I was soon deep in peaceful woods, and saw only two other hikers the whole time. That's more like it. I started to recharge. The trail was well-marked with little read dots on trees, so I could drift. After the intial excitement of interesting mushrooms and rocks, and dappled sunlight, the woods were pretty samey. I love the woods, and it gave me lots of thinking time. I was hankering to work on indieweb and socialwg stuff.

The route took me back to the park, and in a lot less time than I expected. I was back at the Big Waterfall, not far from where I started, at 1430. This time I walked around the eastmost side of the lake, close to the water, to Gate 2. That didn't take long either, and I didn't feel I could just go back so early. I sat for a bit, and then wandered south. I decided to see how far I could get in the direction of Korenica without going back to the highway. Technically the park goes all the way, but I had no terrain information. I ploughed on over a definitely-off-track hill. The ground was leaves and squish and the trees were close and I had to fight through them. The ground though! That's what the world is made of. Decaying things, packed together. It was nice. I made it to the top of one hill and there was another, and another, and another. I'd gone too far to turn back, and my GPS was telling me I was slowly progressing in the right direction. The hills were steep; like I said, off-track, and I basically just had to charge directly upwards, clinging to bushes.

It doesn't seem to matter where I go, I will always ultimately end up scrabbling up hills through undergrowth.

It was *such* a relief when the track/settlement I could see on google maps appeared over the hill. I made for the highway, cos there was no way trying to go through the park all the way back was a good idea. The verge was such that I could stay off the road, so I followed the highway for about 20 minutes before I was picked up by a local. He didn't speak English but I think we managed to communicate a bit. I was passed by loads of cars that didn't stop this time; I presume they were tourists. Bah.

He dropped me in the centre of Korenica, and I stumbled back to the hostel and collapsed for a bit, by 4pm.

I walked around 18 miles today. My legs, aside from being extremely reluctant to support me, have developed all kinds of interesting enormous red lumps with varying degrees of itchiness. I assumed they were insect bites (they have been developing for several days now) but some of them are going a bit weird. I should probably at least document it...

Also I should sleep... 0625 bus to Zagreb in the morning!