<!---
published:
type: BlogPost
tags: technology,hardware,devices,tales of things,reuse
list: done
-->

# Device Tales

Compared to a lot of people I know, I don't get through hardware particularly fast. I tend to make things last as long as possible and/or pick them up second hand and/or find them a suitable new home when I'm done. My consumption is accelerating though, and I want to start keeping a small record of where I get bits of technology from and why, and what happens to them, as well as which bread product they are named after. 

If you really like this general concept, you should check out the [Tales of Things](#) project.

* PATTISSERIE, enormous clunky Acer laptop
  * 2008: from my parents for my 17th and 18th birthdays combined as pre-uni gift.
  * 2011: became paperweight; fully intended to use it for something smart-house-y, didn't.
  * 2014: donated to [SHRUB](http://shrubcoop.org).
* SANDWICH, 250gb hdd, 2009 - present
* TOAST, cheap-ass colour printer/scanner
  * 2008: negligible money from amazon.
  * 2015: still going strong!
* WAFFLES, secondhand Mac Mini
  * 2009: bargain Ebay purchase, because I wanted to learn how to use a Mac.
  * 2015: Still going strong.
* CROISSANT, Acer Aspire One netbook
  * 2009: desperately wanted something more portable than Pattisserie. My first foray into Linux was installing Mint to replace Linpus.
  * 2011: donated to sister, who possibly has never used it.
* BAGUETTE, 1tb hdd, ? - present
* DONUTS, second hand crappy road bike
  * 2010: a few quid from a car boot sale.
  * 2014: death by fire and exposure (vandalism, then I neglected it, oops).
* CRUMPETS, refurbished original Kindle
  * 2011: bought for keyboard and unlimited global data, so I could blog in the desert. Became Kindle touchtyping pro. Used for navigating using PDF maps before I had a smartphone. Made me only care about websites that would load in experimental Kindle browser.
  * 2012: cracked the screen, amazon replaced under warantee no questions asked!
  * 2015: stuck in a restart loop for reasons unknown, warantee expired :( I miss it!
* PURI, secondhand HTC Magic
  * 2011: acquired from housemate.
  * 2012: developed multiple software and hardware bugs, but is still knocking around; lend it to visitors on occasion.
* PANCAKES, Thinkpad T430
  * 2011: splashed out, hoping it'll last many years. Linux Mint became my primary OS.
  * 2015: currently my main machine. Dropped numerous times. H key has fallen off.
* PASTY, secondhand HTC Desire
  * 2012: acquired from [KitB](http://ninjalith.com).
  * 2014: started developing unbearable software bugs; donated to brother.
* PANINI, Nexus 7 tablet
  * 2012: wanted something more portable than Pancakes.
  * 2014: saw less use when I got Pretzel; currently on indefinite loan to extended family member.
* PRETZEL, Fairphone 1
  * 2014: my first ever brand new mobile phone!
  * 2015: one battery incident later, still <3 it.
* CREAMPUFF, secondhand Chromebook
  * 2014: still wanted something more portable than Pancakes, but more useful than Panini. [Kim](http://thelensaffair.com) was selling hers, so I took it up.
  * 2015: good for everything but coding and IRC. Might duel boot it with Ubuntu.

BAGEL - ??