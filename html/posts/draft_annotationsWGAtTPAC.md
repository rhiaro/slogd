<!---
tags: w3c,tpac,tpac2015,conference,event,phd,annotations,socialwg
-->

# Annotations WG at TPAC

I spent most of my first two days at TPAC as an observer of the Annotations WG. Friendly bunch, I enjoyed it. Here's a brief report, focussed around things that are relevant to the SocialWG.