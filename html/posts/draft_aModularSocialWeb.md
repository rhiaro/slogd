<!---
tags: socialwg,phd,social web,api,building blocks,indieweb,activitypump
-->

# A Modular Social Web

A million people and their dogs have written about the 'building blocks' of the social web. My turn! I've only been thinking about this stuff for a couple of years, and my experience is negligible compared to many others who write and build on this topic. Last year I sort of wandered into the [W3C Social Web WG](http://w3.org/wiki/Socialwg), and now it seems to be my main focus.

From the perspective of being webby, decentralised and easy to implement one by one, and trying to be neither too academic nor too technical, these are the pieces that I see right now.

## 1. Profiles

Identity is at the heart of most lists like this, and carries a lot of baggage. I'm not talking about auth (something bearer tokens something something), but of course identity and relationships come coupled with access control, so this topic can get rabbit-holey quickly. Nevertheless, way outside of my area of expertise.

Right now, I'm thinking about profiles as documents with a subject which could reasonably be the originator of some social content. This can be a fully fledged 'real' person, a group or organisation, a persona or character, a chatbot, [this cat](https://indiewebcat.com) or [this building floor](https://twitter.com/atlevel5). A profile is a document with an identifier (globally unique unless you've a good reason not to) that you can point to when you want to address, notify, attribute or otherwise refer to the subject. Obviously, I'm averse to the idea that folks should unify their identities or use a single one online, or be required to link them in any way. However, sharing attributes and activities across different contexts can be useful, so selectively linking parts of profiles should be *possible* - not required or even expected.



## 2. Reading

## 3. Discovering

## 4. Subscribing

## 5. Creating (and updating and deleting)

## 6. Notifying (mentions)