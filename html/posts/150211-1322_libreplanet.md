<!---
published: 11th February 2015, 13:24
type: BlogPost
tags: events,libreplanet,free software,conference,travel,usa,boston,cambridge,rsvp
list: todo
-->

# LibrePlanet

Straight after the Social Web Working Group face-to-face *and* [indiewebcamp Cambridge](http://indiewebcamp.com/2015/Cambridge) is [http://libreplanet.org](libreplanet) (21st and 22nd February). It's going to be a busy week.