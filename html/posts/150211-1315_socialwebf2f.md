<!---
published: 11th February 2015, 13:15
type: BlogPost
tags: events,social web,socialwg,social web working group,working group,w3c,decentralised web,travel,usa,boston,cambridge,rsvp
start: 17th March 2015, 09:00
end: 18th March 2015, 18:00
reply-to: https://aaronparecki.com/events/2015/03/17/1/socialwg-2015
list: done
-->

# Social Web F2F

I'm looking forward to attending the next face-to-face for the [W3C Social Web Working Group](http://w3.org/wiki/Socialwg) in Boston, MA, on the 17th and 18th of March.