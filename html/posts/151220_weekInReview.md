<!---
tags: week in review,phd
published: 20 December 2015, 23:59 GMT
-->

* Opened and closed enough issues on [Social Web Protocols](https://w3c-social.github.io/social-web-protocols/social-web-protocols) for it to be accepted as FPWD.
* Wrote at least some of [Chapter 3](https://rhiaro.github.io/thesis/chapter3)
* Started outlining [Chapter 2](https://rhiaro.github.io/thesis/chapter2)
* Finished and deployed my [webmention endpoint](/q)
* Updated my webmention sending to also send the `property` parameter, and to be able to discover Solid reply endpoints (`rel="pingback:to") for a resource and write to them if they're public.