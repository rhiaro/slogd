<!---
published: 2nd April 2015, 16:53
type: BlogPost
tags: events,social web,socialwg,social web working group,working group,w3c,decentralised web,travel,usa,boston,cambridge,libreplanet,people,running,november project,indieweb,indiewebcamp,slogd,progress
reply-to: 
list: done
-->

# Weeks in review: everything++

In one week I attended a face-to-face meeting of the [W3C Social Web Working Group](#), [an Indiewebcamp](https://indiewebcamp.com/2015/Cambridge), and [LibrePlanet](#).

I also ate a _lot_ of _amazing_ food.