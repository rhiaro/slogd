<!---
published: 29th July 2015, 21:30 BST
tags: indieweb,ownyourdata,memories,social media,archive,journal,ephemerality,life,existential crisis
-->

# Digital Memories

For about fourteen years I kept detailed daily journals. I couldn't sleep unless I'd written. And I couldn't just write "Went to school, Polly and Laura fell out again." I had to write every detail. Food, friends, lessons, homework, learning to make websites, gerbils, cats (those are all the things associated with being a normal teenager, right?), and all the thoughts and feelings associated with everything I'd encountered that day. It was habit, and compulsion. It was also a burden. When I went away, I had to plan to carry a notebook with me, and keep it somewhere other people wouldn't get hold of it. I had to make time before bed to write, usually for a full hour. If I had to skip a night, I had to double time the next night, and felt the pressure of holding onto the memories I hadn't manage to record for an extra day.

This continued during my undergrad. But was harder. Entries got shorter, even though more was happening. I'd pull spontaneous movie, coursework or just hanging-out all-nighters with friends who lived across town. Even if I happened to have the journal on me for some reason, ducking into another room for an hour to write would have been.. pretty weird. Sometimes I'd miss a few days, then spend a full afternoon, day or even weekend catching up. And while I was doing that... I was missing out on other things.

I have a heavy box containing dozens of notebooks. This is also a pain when I move house (as noted by people who have moved it for me). After living things, it's what I'd save in the event of a fire. It's *really* heavy though.

After moving to Edinburgh, I found less and less time to write. I'd do catch up spurts, but they became less frequent. It became exhausting. Eventually they seemed fruitless because I couldn't remember nearly as much detail as I wanted to. At first this was terrifying. I've read back journals from years ago, and don't remember thinking or feeling a lot of what I wrote about. If I hadn't written it down at the time... it'd be gone. So by not writing in the present, I'm depriving my future self of a lot. But eventually not-writing normalised, and the burden started to lift. When it was no longer a compulsion, it was no longer painful when I missed it.

My social media use was happening in fits and starts around then too. Posting to facebook probably helped to ease into worrying about journaling less, as I was recording my day-to-day elsewhere. But when I realised banal status updates had become a compulsion too, coupled with some people taking facebook activity far too seriously, I deleted everything from there (post by agonising post). I didn't think to export it beforehand. I was using twitter, but not for personal stuff.

We pour a lot of ourselves into digital archives, one way or another. But how do we get it out again? Why do we need to? Most of us post to silos like facebook and twitter, providing fuel for the corporate advertising machine and seeing only fleeting value for ourselves. My skepticism of this restricted how I used social media. Then I got into this decentralised social web malarkey, and my journaling addiction started to re-stir. Now I'm posting a lot again. Some of it makes it to twitter, but I post more solely on here, rhiaro.co.uk. It's not the journal, daily records material of old, but shorter, realtime, in the moment posts that in aggregate provide a record of the day (particularly as I pull in more quantified-self activity tracking type stuff).

And the burden is back.

I noticed it when I started posting less again this past month. I was tracking every single thing I ate for long enough that it became habit; tracking when I left or arrived at home, office, meetings, events, social occasions, and more. I stopped tracking both food and locations because some bugs have materialised in my code and I haven't got around to fixing them yet. It was agonising to start with. But as I still haven't made time to find out what the problems are, I realise I don't miss the stress of trying to log food or check-in on a poor mobile connection or worse, scribbling notes on paper to back-fill later when I can't do it in the moment. Once I started logging something, it didn't seem worth doing unless it was *complete*, unless I logged *everything*. Things missing made me anxious. Wtf?

Not everyone has this problem. Some people (so I've heard) post to social media because it satisfies an immediate need, and what happens to it after that *doesn't matter*. Many people aren't interested in the Own Your Data mantra, because this 'data' is ephemeral, not archival. Some people are totally happy to drop their thoughts and feelings into the black hole of the social media machine, never expecting to get them back. How freeing that must be.

I've never been blackout drunk and I have never understood the appeal; I'm kind of terrified of the idea that in a few years time and I want to look back over my years in Edinburgh there are going to be enormous gaps. Does that mean the memories I have retained are the only ones that were worth keeping? Or am I poorer because the things I neglected to record are gone forever?

Collectively, the Web-privileged world is recording an insane amount of unstructured personal data; so many fleeting thoughts and feelings and desires and needs. Where did this come from? Didn't we used to manage fine without? If it's a sign of progress, maybe we should be using it to progress. Whether it's stored under the originator's control or surrendered to a corporation, all together we have a detailed picture of what it is to be (certain types of) human. But nobody is using this for anything other than personalisation, recommendation, profiling... selling more crap to people. Except for the academics doing cool disaster-relief stuff with realtime twitter data: props for that.

Imagine if we could tap into the historical archive and use it to understand different perspectives, to boost empathy and tolerance. To create a concrete, collective ancestral memory that helps us build a better future for everyone.

If we're not going to do that, we should probably focus on living in the moment a bit more. I feel like that is healthier, but it goes against my impulse to (at least try to) record and permanently store everything.

Given all this, you'd think I'd have a better strategy for automatically backing up my database.