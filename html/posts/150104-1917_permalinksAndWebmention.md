<!---
published: 4th January 2015, 19:17
last-modified: 4th January 2015, 19:17
type: BlogPost
tags: hacking,slogd,webmention,indiweb,permalinks,linked data,uri,learning,microformats2,microformats,mf2
list: doing
-->

# Permalinks and webmention

As mentioned in [Blog Post URIs](http://blog.rhiaro.co.uk/blog-post-uris), I'm a bit concerned about people replying to the 'wrong' URI for a post. That is, using the URL of a HTML page with the post rendered within it, rather than the post's permalink.

I couldn't find a [Microformats2](http://microformats.org/wiki/microformats-2) term for permalink, which I thought would be the obvious solution.

The webmention 'protocol' says that when I recieve a webmention:

- Check the `target` is a valid URI on my site *after following redirects*.

In my case, upon recieving a `target` of a `http://rhiaro.co.uk/yyyy/mm/id`, to validate it, rather than 'following a redirect' I'll actually be getting it's `foaf:topic` from the triplestore, which is fine. *But* in accepting this as the `target` I'm confirming to the sender that this is the correct URI to reply to. Hypothetically this non-permalink could break, breaking the reply. The permalink URI is one I'm promising won't break, so it's in everybody's interests to send the reply there. But logically, if somebody is linking to me in a post of their own, they're going to want to link to the rendered version, rather than whatever my raw version will look like (probably crappy, but that's on me, and maybe part of the problem).

## Replying to shortlinks

Because I'm a linked data nut, my problem derives from a deliberate differentiation between a blog post and a page about a blog post. The connection between them (or from one to the other) can be discovered through RDF, but there is no HTTP redirection. I suspect this is a very much minority use-case for #indieweb folks. 

However, use of shortlinks is widespread (and you can see redirects are accounted for in the webmention protocol), and so as soon as someone's URL shortner breaks (or moves) permanently, unless they store the post contents and metadata themselves, they might have to have to think about this a bit.

## Solution?

I'd like to be able to send back the correct URI (303 redirect style) so the sender can either:

- try again with the correct URI, 

*or*

- update their system with the correct URI, knowing I've accepted the webmention against the correct URI on my end (more like a (2NN)[http://tools.ietf.org/html/draft-prudhommeaux-http-status-2nn-00] than a 303).

I haven't looked at any implementations yet or thought about how to do it, so maybe I can just do this. I'm not sure how to anticpate people handling it.

**Straw poll**: What do you/your system do if you send a webmention and it's rejected, though you're *sure* the `target` is valid?

And if you're going to send your reply as a webmention from your own system, please target this post's permalink - the URI without a year and a month in :)