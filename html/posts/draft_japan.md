<!---
published: 25 October 2015, 22:00 JST
tags: travel,japan
-->

# Days one and two: Tokyo

arrived evening, walked to hostel, back to station for jr passes, failed to find food, convenience store sushi, hostel food.

Akhibara, hunting for food, learning about navigating, rapid T's, train to Sapporo.

<!---
published: 29 October 2015, 23:59 JST
tags: travel,japan
-->

# Days two to five: Sapporo

arrived late, cold and snowing, but could walk most of the way underground natch.
Random cheap self contained hotel room in 'love hotels' area.
variously walked, trained, bussed and taxid around, all straightforward.
mostly tpac and tpac food.
learned to love Lawson for sushi and mochi.
monday, dinner at chinese place in nearby mall; accidentally ordered fries (I didn't know how potatoes were going to be prepared) and salad.
Took Tuesday morning to see hokkaido museum and historical village, learn about ainu. Buckwheat noodles and fried potato things for lunch at village restaurant. Return buses are hourly..
AC dinner was smokey but veggie good
Fancy pants breakfast at Art Hotel
Odori park and JR Tower
THursday night vegan shenanigans, ending in Ike Laboratory

<!---
published:
tags: travel,japan,train,jrpass,sapporo,hakodate
start: 30 October 2015, 08:?? JST
end: 30 October 2015, 11:?? JST
startLocation: http://dbpedia.org/resource/Sapporo,_Hokkaido
endLocation: http://dbpedia.org/resource/Hakodate,_Hokkaido
-->

<!---
published: 31 October 2015, 20:00 JST
tags: travel,japan
-->

# Days six and seven: Hakodate

Cheap but convenient businessy hotel.
Almost climbed mountain, but rain. Wandered markets, many treats, more bustling than expected. Bought many rice cakes. Discovered turkish place.
Next day easy climb; park, museum, turkish place
train to tokyo..

<!---
published:
tags: travel,japan,train,jrpass,sapporo,hakodate
start: 31 October 2015, 08:?? JST
end: 31 October 2015, 11:?? JST
startLocation: http://dbpedia.org/resource/Hakodate,_Hokkaido
endLocation: http://dbpedia.org/resource/Tokyo
-->

<!---
published: 2 November 2015, 18:00
tags: travel,japan
-->

# Days seven, eight and nine: Tokyo again

train arrives evening, dinner at T's
friends in Ebisu
walked to Shinjuku, checking out coffee, bread, cakes etc on the way. Lunch at vegan basement place, curry & desserts
Government towers, some korean thing going on.
Failed attempts at entering parks.
Dinner at Ebisu various mushrooms place
RAIN. Ebisu art museum. Umbrella locks. Cat cafe. Falafel lunch.
Train to Nagano

<!---
published: 3 November 2015, 23:59 JST
tags: travel,japan
-->

# Days nine and ten: Nagano, Obuse, no monkeys

Nagano for mountains, hot springs and monkeys. One out of three.
Two nights in a nice AirBnB with a well stocked kitchen.
Wandered, bought supplies for cooking. Found a couple of potential veg*n places but went with a bustling, smokey local one. Edamame, tofu, pickles, various tempura. Scraped the bacon off. Coconut bubble tea. Night temple.
Zenkuji(?) temple.
Train to Yunadaka. No monkeys. Bus and walk to monkey park anyway, and back. Volunteers at station suggest Obuse, so go there.
Quiet. Kittisensei(?) Chestnut things, soba, chestnut mochi.
Wander. Fail to find onsen.
Back to Nagano. Cooked.

<!---
published: 5 November 2015, 15:59 JST
tags: travel,japan,kyoto
-->

# Days eleven and twelve: Kyoto and environs

Up early for train to Kyoto. Huge station and roof garden. Ryukan/hostel.
Lunch (?) Walked, Philosopher's Walk, various temples and shrines.
Terrible curry. KstiltsShrine no access, cemetary.
Early train to Arashiyama. BEAUTIFUL. Walked, river, shrines, bamboo. Back by checkout.
Locked our stuff at the station and sought the airport bus.
Lunch places at the station super busy, stumbled on Cosme, got takeout for Inari shrine whistlestop tour.
Somehow made the airport bus.
Largely slept until Boston.