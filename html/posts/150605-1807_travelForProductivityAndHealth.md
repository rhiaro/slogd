<!---
tags: travel,health,productivity,phd,work
-->

# Travel for productivity and health

I'm flying back to the UK having been away for five weeks. I attended a meeting, a hackathon and two conferences, and in between climbed some mountains, hiked through some forests, swam in lakes and sea, spent an awful lot of time on trains and buses, visited some new countries and cities and met a ton of awesome people.

Most people are incredulous when I say I'm actually working and not on a break from my PhD, and tell me how they'd never get away with this at their university or organisation. But it turns out, I've been way more productive over the last five weeks than I would have been if I had been in my office.

I'm excused from obligations like meetings and seminars, I don't have to run errands or do chores, and as much as I love coffee breaks with my officemates six of them a day at half an hour each can be a bit of a time sink.

There have been other distractions instead of course. Mountains that won't climb themselves. Chasing buses that never come. Hunting for a last minute bed to sleep in. These kinds of distractions keep me awake though, paying attention, instead of turning my brain to mush or making me feel resentful of everything around me. I'm primed for ideas and inspiration at all times. Turns out I work much better on the beach and in trains than I do in the same room every day.

I've felt more awake and more able. My confidence is boosted every time I resolve some travel-related inconvenience or handle a difficult situation. I've been able to be spontaneous and felt empowered because of it. I'm stronger for living day by day out of a rucksack and not always knowing where I'm going. I've been able to focus on the moment and follow my instincts.

It helps that I've been healthier, physically and mentally. I've been able to totally relax. The exczema on my hands cleared up completely and my nose has been more often functional than ever before. I've spent a lot of time outside, and been active. I've walked hundreds of miles, worn myself out completely and slept solidly. The flip side of this is I've pushed my feet and legs further than they're used to and they're starting to complain quite a lot. I've gained all kinds of new and weird injuries and rashes from insects, plants, rocks, sea creatures, my shoes, and miscellaneous things I've walked into.

People. I've had days of intense socialising, and days of total solitude. I need the solitude. Between WWW in Florence and ESWC in Portoroz, I spent a week mostly alone. I realised how much good this had done me when I arrived in Portoroz, immediately sought people to have dinner with and was chirpy and social all evening. Throughout ESWC I've been keen to talk to people and didn't start feeling the need to hide until the very last afternoon. I've managed to see lots of friends I see rarely or only talk to online, which makes everything worth it by itself. I've had wonderful experiences with great people - travellers, hosts, friendly locals or conference-goers - who I'll probably never see again or keep in touch with. And I've made solid new friends I'm planning to keep hold of for the long term.

I've had new experiences, and unexpected incredible opportunities have come my way. It's clearer what's important. I've been more myself. I hope I can keep this up when I'm back. But if not, I'm going to coin PhD-by-interrail.

Incidentally, I've also spent less money than I would have at home (if I'd stopped paying my Edinburgh rent for this month; 60% couchsurfing, 40% hostel dorms is quite a bit cheaper).