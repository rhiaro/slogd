<!---
published: 22 November 2015, 23:59 EST
tags: phd,week in review
-->

# Week in review

## 16 - 22 November 2015

* Submitted a tutorial proposal to WWW2016: Building decentralized applications for the social web.
* Digested CHI reviews for our pro-social deception paper; mostly neutral, we need a very strong rebuttal.
* [Rewrote](https://github.com/rhiaro/activitypump/blob/restructure/index.md) the [ActivityPump](http://w3c-social.github.io/activitypump/) spec in the structure of my [SocialAPI](http://w3c-social.github.io/SocialAPI/socialapi) document with the goal of decoupling the constitent parts and making it easier to implement in pieces rather than requiring all or nothing. Awaiting feedback from AP editors/implementors/interested parties.
* Wrote a script for publishing photo albums as AS2 JSON-LD and HTML+RDFa at [img.amy.gy](http://img.amy.gy). Each album is served as HTML+RDFa by default and can be retrieved as AS2 JSON-LD with the Accept header set to `application/ld+json`, `application/activity+json` or `application/json`.