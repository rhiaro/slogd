<!---
published: 11th February 2015, 13:19
type: BlogPost
tags: events,social web,indiewebcamp,indieweb,decentralised web,hackathon,travel,usa,boston,cambridge,rsvp
start: 19th March 2015, 09:00
end: 20th February 2015, 17:00
reply-to: http://indiewebcamp.com/2015/Cambridge
list: doing
-->

# Indiewebcamp

Straight after the Social Web Working Group face-to-face is an [indiewebcamp](http://indiewebcamp.com/2015/Cambridge) at MIT! 19th and 20th of March. Can't wait. Despite my best efforts in the past, this will be my first one.

This happened!