<!---
published: 3rd April 2015, 22:45
modified: 3rd April 2015, 22:55
type: BlogPost
tags: hacking,indieweb,test,micropub
list: doing
-->

# Micropub test

If you can see this, my micropub endpoint works :D

And if you can see _this_, I can also edit posts.