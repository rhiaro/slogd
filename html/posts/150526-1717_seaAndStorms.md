<!---
published: 26th May 2015, 22:00 Europe/Berlin
tags: travel,croatia,split,marjan,rain,sea,beach
-->

# Sea and storms

It was crazy hot this morning, though my legs made me promise not to do anything too strenuous. I deliberately set out ill-equipped for any adventures involving mountains, rocks or undergrowth. So you can imagine how that went ;)

I picked up a hemp burger and interesting couscous cake thing from Makrovega, dropped off clothes at a laundrette, and made for the beaches on the north side of Marjan. I was spoilt for choice regarding beautiful and deserted spots of clear water. I noticed this starting to roll in...

![storm approaches](http://rhiaro.co.uk/photos/1505croatia/storm.jpg)

Becoming rapidly closer, and emitting thunder, but still far enough away..

![dundundundun](http://rhiaro.co.uk/photos/1505croatia/storm2.jpg)

I had enough time to dry off in the sun, eat my burger and get dressed and duck into the woods before the first rain started to fall. Seeing and hearing it approach across the water was cool, and I sheletered under a pine tree until it passed. I carried on around the edge of Marjan, each little rocky cove more blue and clear than the last. The next wave of rain was heavier and lasted longer. I sheltered under the trees for the worst, and managed to make it to a restaurant before it got really bad. The restaurant had wifi, coffee and a sea/rain view, so I bedded in. The rain didn't look like it was going away any time soon. I did, however, have a pressing need to pick up a chocolate amaranth pudding from Makrovega before the socialwg call at 7 (not to mention my laundry), so I couldn't stay there all day. I heard an Irish couple asking the staff to help call a taxi, so I got in on that, and they wouldn't even take any money. They happened to be staying in a perfect proximity to the laundrette, Makrovega, and my hostel, so I successfully completed all tasks shortly after being dropped off and made it back in time. And the rain had stopped.

Later I wandered out to pick up food from Vege on the seafront, but despite Google's assurance they were open til 2300, they were not. I found some pretty great box noodles on the way home, with seitan, many veggies and peanuts. Split's answer to Edinburgh's Red Box, but cheaper and all round better.

Up super early for the bus to Zadar tomorrow...