<!---
tags: travel,ferry,italy,ancona,port,protip,trains
-->

# Ferry at Ancona

I decided last minute to get a ferry from Ancona (Italy) to Split (Croatia). I thought I'd write a note about navigating Ancona and ferries, because I didn't find much online beforehand and if you don't read Italian and/or have an intricate understanding of ports it's not a particularly welcoming experience.

Initially - which is unrelated, but sets the tone - I planned to take trains from Firenze to Faenza, then Faenza to Ancona. The train from Firenza was delayed by 25 minutes, but fortunately I had an hour to make the connection at Faenza. I have already noted that most regional trains I've been on in Italy so far don't have any clues on the outside about where they're going, and lots don't have screens or announcements on the inside either. On top of that, smaller stations don't have live departures signs on platforms (ie. which train is here next) but paper posters for looking up platform based on time and destination. Which are good, but there is a certain amount of hoping for the best when you jump on the mystery train that rolls up to where you think you should be waiting. Anyway, at Faenza the Ancona train was also delayed by 35 minutes. Despite knowing this, I jumped on a mystery train that rolled up at the correct platform at about the correct time, then realised what I'd done. This turned out to be a delayed train from earlier. This was one of the great ones with zero hints on the inside about route audibly or visibly. The Interrail timetable app is *great* however, and after a couple of stations I managed to pin down which train I was on and could work out that it should terminate at a station where I could pick up the train I *should* have got - assuming that one was still running late.

It was - even later, in fact - and I eventually switched to the Ancona train at Rimini. The last stretch of the journey was along the coast, which is always good.

The internet advised me to check in to the ferry 2 hours before departure, and I'd left an hour of buffer time, so I still had 20 minutes or so spare even with the delays when I got to Ancona. It was absolutely chucking it down, but I'm getting good at imagining how beautiful towns are in the sun. Pretty sure Ancona would've been stunning. Turn left out of the station and follow the road for about 20 minutes to reach the Ferry Port Authority. Got soaked through, but it was easy to find.

And, all closed up. I could see the ferries, but not where to go to check in. There were signs, but it wasn't clear where I was supposed to go first. I followed various signs for passport control until a disgruntled police officer managed to put me on the number 20 bus. She was grumbling and pointing in Italian so I did as she said, without really knowing why. The bus driver asked where I wanted to go in English, and I said 'check in' and he said 'aha, terminal' and took me there, which was about half way between the train station and the Port Authority, but I'm not really sure if it's reachable on foot or not. This is apparently a free shuttle bus between the different bits of the port you need to visit to successfully board a ferry.

* Step one: don't walk for half an hour to the Port Authority in the rain. Take the number 20 bus to the Terminal.

There, the signage was more helpful, and I swapped showing my passport and PDF ticket for a paper ferry boarding pass. The next instruction was to go to Port 9, which I recalled was back by the Port Authority where I'd already been. It looked like the number 20 bus went round in a circle, so I picked that up again outside the checkin terminal and sure enough, it took me back to the Port Authority.

* Step two: The number 20 bus is good. Take it back to the ferry ports.

This time was different, because the bus was full of other ferry-bound tourists, so I was able to follow them. Also the same passport control building appeared to be open now, so I followed them into precisely where I'd failed to get in before.

* Step three: Go through passport control. There is no security check or anything. Possibly this is not available until two hours before your departure time.

Inside the post-passport-control waiting room was zero indication of where to go next. I followed other people.

* Step four: Go straight out the door on the right as you enter the post-passport-control room. You'll see ferries. Walk along past ferries until you see your ferry.

We had to wait in the rain a bit, but then got straight on the ferry to be greeted by extremely cheerful staff, presumably to make up for all of the grumpy port staff.

* Step five: Board ferry. Yay!

I booked a 'deck passage' which translates to 'sit wherever you can find'. I briefly considered an extra 4 euros to guarantee a seat but then remembered I could get like three gelato for that. That turned out to be great, as the ferry isn't busy (or everyone is in super expensive cabins) and any empty 'paid' seats are fair game. I found one beside a window and a power socket!!! They're good seats, too; big and recliny, and I have a whole row to myself right now so I'm going to lie down and sleep properly in a bit.

(By the way, the ferry ticket was 38 euros with an Interrail discount).

I've eaten all of my stolen conference lunch food and I'm still starving, so Imma go on a ferry-adventure-food-quest.

Update: There are people sleeping everywhere, floors and bar sofas; weird they're not using the empty seats, but more space for me. I procured some overpriced paprika crisps and a jar of souvenir antipasti for sustainance. The rest of the ferry is pretty boring, and largely full of loud middle-aged Italians.