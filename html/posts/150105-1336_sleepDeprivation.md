<!---
published: 5th January 2015, 13:36
last-modified: 5th January 2015, 13:36
type: MicroblogPost
tags: sleep,quantified self,life,polyphasic
list: done
-->

Sleep deprivation definitely kicking in a bit. Finding it hard to concentrate or remember what I'm doing. Feels like Friday because I've slept so many times since Saturday. Feel pleasant, but out of it.