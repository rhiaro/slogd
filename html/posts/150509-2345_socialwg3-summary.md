<!---
published: 9th May 2015, 23:45
tags: socialwg,social web,w3c,meeting
list: doing
-->

# Social Web WG 3rd face-to-face summary

*This post is my own opinion, and does not necessarily represent the opinion of the Social Web WG!*

**tl;dr:** There is increased understanding and expectation of convergence between Micropub, ActivityPump and SoLiD. We're all excited about working towards this convergence. There is increased agreement and understanding about similarities and differences between protocols, and where strong points of one protocol could plug weak points in another. More people are excited about implementing experiments to really figure out what works and what doesn't.

See also [day 1 minutes](https://www.w3.org/wiki/Socialwg/2015-05-04-minutes) and [day 2 minutes](https://www.w3.org/wiki/Socialwg/2015-05-05-minutes) and [self-critique whiteboard](http://aaronparecki.com/uploads/whiteboard-20150505-161540.jpg).

What follows is more detail on my perspective of the main conversations we had over the two days. Clarifications and corrections welcome.

## Issues in the tracker

Closed a bunch of ActivityStreams2.0 issues with the general conclusion that issues raised in future should be more specific and actionable, rather than vague problems. An alternative for reporting this kind of issue wasn't mentioned specifically, but vague problems can probably go in brainstorming sections on the wiki somewhere (indieweb style) or into github issues.

A note that we can always re-open issues if they turn out to be a problem in the future, but we shouldn't *keep* them open just in case they might be.

## Awesome user-story protocol walkthroughs

[Aaron](http://aaronpk.com), Jessica and Andrei and Henry walked us through exactly how some of the user stories would be carried out according to the protocols [Micropub](https://github.com/aaronpk/Micropub/blob/master/user-stories/), [ActivityPump](https://github.com/w3c-social/activitypump/tree/master/userstories) and [SoLiD](https://github.com/linkeddata/SoLiD/blob/master/UserStories/) respectively. This was *super* useful, and if you weren't there you *need* to read the accompanying documents (linked just prior) and minutes (linked at the top of this post).

Results of this were:

* Proponants of each protocol started to really understand how the other protocols worked, in a way that just reading all the specs couldn't do.
* Everyone really started to see where some protocols were strong and where their own weak points or gaps were.
* Everybody started to express that their protocols aren't complete and entertain the idea that they could make changes.
* We started to get a feeling of how the three protocols could complement each other well, and the feeling of competition between them was gone.
* We started to get *really* excited about making this happen.

I particularly noted:

* Sandro said he could see how Micropub and SoLiD were similar/complementary (SoLiD advocates seem keen on adding webmention for notifications).
* Aaron and Jessica agreed that ActivityPump and Micropub were actually way more inline than they realised before.
* I can see the ways in which ActivityPump and SoLiD are similar, and Evan also noted the complementary nature of the two.

That's a full triangle of connections! The protocols are all friends.

The resolution, then, was that we won't rush to pick a base to work from, and instead will spend some more time (with regular updates to the group) tweaking each protocol to get closer to convergence, so everyone will be happier when a base does eventually need to get chosen.

(*Exciting*, right?! This was like the best meeting of minds that has happened so far. If you weren't in the room you might have missed this, so... trust those of us who were).

## Extensibility

There should be some kind of clear warning in the spec that extensibility won't work well if you're using plain JSON, not JSON-LD (implying the best way to do vocabulary extensibility is through URI namespacing, which hasn't neccessarily been agreed, but to me seems like a sensible way). It also needs to be clear that upon receiving terms that a server does not understand it should *not* drop the data, even if it ignores it - in case that data gets passed forward to a server that might understand it.

We resolved "ISSUE-36, Remove all non AS2 namespaces from the normative context, keep the normative context limited to only AS2 vocabulary terms". I'm not totally sure what this means: that if there are terms we decide are crucial we will include AS versions of them even if they already existing another vocabulary (instead of reusing)? In the name of 'completeness' might we overload ActivityStreams, and because we consider AS 'complete' risk sabotaging easy extension? I still don't get everything that is throw around during extension mechanism conversations, so I could be missing something. Mostly I'd love to see someone implement AS and extend it to a specific domain, to see how this would work in practice. On a related note, I don't think I've seen documentation of the use-cases / origin of all of the vocabulary terms in AS2, does this exist?

## Activities vs objects binary is false

The activity-oriented model of AS2 and the object-oriented model of microformats are actually basically the same but looked at from different perspectives. I think it's really important people try to see this, and not see them as conflicting. We're all trying to do the same thing, and there's no reason AS2-based and microformats2-based implementations can't interoperate. I'm going to work more on this, and others are too. We closed issues related to this as too vague, but it'll need revisiting when we have some more concrete proposals about what to do. By the end of the two days, people were starting to generally agree that this is the case.

## People and profiles

We clarified that `Person` in AS2 does not need to refer to a real-world person, and agreed we should make this clearer in the spec (eg. it could be a fictional person, a persona or aspect of someone, etc). We resolved to add `Profile` to AS2 to enable talking about documents about people separately to talking about people, and to facilitate connecting one `Person` to multiple `Profile`s, which I think is useful but not everyone agrees. Thus `Profile` is at-risk until there are implementations.

It's worth emphasising the two different parts of this discussion: `Person` vs. `Persona` is different from `Person` vs. `Profile` (ie. `Person` != `Persona` != `Profile`). There was some conflation of this during discussion. Whilst everyone tends to agree that there are three different things there, we disagree about which level of modelling is practically necessary. Figuring this out based on observing how existing social sites model this, implementing my own aspect-based profiles, and shunting my own data around between different profiles, is something I'm concentrating on over the next couple of months.

## Clients and servers

Describing things as client *or* server can be misleading. Things *act* as a client or server depending on the perspective, and it doesn't necessarily matter if they *run* in the client or on the server. We either need to clarify or alter our terminology, perhaps rephrasing to describe which componants we see having which *responsibility*.

## Namespacing terms

Not at all, with strings, with URIs? Uh... decent arguements for and against all, strong opinions all around. I dunno.

## Audience targeting vs group membership

Subtle distinction between access control via the author of a post deciding who to send it to (or who can see it), and someone actively joining a group in order to see additional content.

In ActivityPump (and pump.io) you specify `to` to decide who can see a note, eg. your followers. If someone new follows you, and someone else unfollows you, then you update your note, which list do the changes get propagated to? The original receivers of the note, or the current list of followers? I *think* this isn't specified and needs to be clarified. In ActivityPump, following someone is effectively adding yourself to a group (Collection) of their followers.

## Side effects and propagating changes to the social graph

LDP doesn't have any side effects by itself, it's simply posting and getting of data to a generic store that doesn't understand the data itself. But for more domain specific purposes like social, LDP could be one layer with another layer that handles side effects like notifications and distribution of changes.

Certain activities in ActivityPump cause specific side effects. This is a fixed list, and could do with being better described in the spec. These are things like: if A creates a follow activity of B, side effects are A is added to B's list of follwers, B is added to A's list of followings, B is notified that A followed them, and anyone with permission to see A's activities also sees that A followed B. From what I can tell, side effects tend to be updating/deleting objects or adding to/removing from collections.

Micropub's side effects could include syndicating a post to other sites, or triggering webmentions (notifications). There is increasing discussion about having a following list that a reader can subscribe to (so you don't have to enter all of your subscriptions if you change reader) and keeping track of when you followed/unfollowed someone; actively doing one could trigger the other.

My vague conclusion is that specifying an extensible way to describe side effects of activities would be useful, with perhaps some core musts and shoulds, but open ended for implementations to do more focused things (eg. if Floop-o-matic decide that posting a Floop automatically likes the second most recent post of everyone named Fred, then they could describe that in a spec-conformant way. Okay that was silly, but you know what I mean).

## RESTful vs endpoints

LDP is RESTful; Micropub (and Webmention) use discoverable endpoints; ActivityPump mostly uses endpoints (specific paths rather than discoverable I think) with some things able to be done RESTfully. Endpoints mean services can be delegated to third-parties, and can therefore be used on static sites. Not sure if there's a specific benefit to RESTful (or RESTfulish) but with a bit of sideways thinking they could actually co-exist, allowing implementers to use the method they prefer.

## Cooperation++

General observation: We're all working towards the same thing. Nobody in the group is trying to sabotage this effort. I don't think it would hurt to listen more, and maybe imagine that you *want* someone else's [solution|use case|constraint|idea] to be better than yours, and try to fit it to your world, instead of just insisting one is the best.

Every community has different constraints, and different use cases they're trying to solve. We can't elevate one set of requirements above another, but need to try to accept - or at least entertain the idea - that they're valid, without dismissing them outright just because you haven't experienced it directly. Periodically mentioning the business use cases from IBM and Boeing (for example) is helpful in reminding everyone that there are legitimate long-standing needs that probably aren't considered by most of us, but will be implemented if we support them, and will be implemented some non-standard way if we don't.

## Addendum

TimBL made sure to remind everyone that JSON is 'in fashion' just as XML once was, and really everyone will regret not just using RDF from the start in the future :)






