<!---
published: 6 December 2015, 23:59 GMT
tags: phd,week in review
-->

# Week in review

## 30 November - 6 December

* Added some RDFa to my site templates; needs more work though.
* SocialWG face-to-face ([notes]()).
* [Indiewebcamp SF](https://indiewebcamp.com/2015/SF)
  * Fixed a paging bug (December was an edge case, apparently).
  * Added sessions so indieauth logins ot my site persist.
  * Made progress on my [webmention endpoint](/q) which in addition to the base webmention spec accepts the `property` parameter and will be able to verify mentions marked up with RDFa.
* Wrote about different social web worldviews and how that affects data models: [Things and happenings](/2015/12/things-happenings).
* Attempted to figure out https with letsencrypt on two different server setups. One was [not terrible](/2015/12/https-terrible), the other was [pretty terrible](/2015/12/https-what).
