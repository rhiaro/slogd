<!---
published: 29th April 2014, 00:40
last-modified: 29th April 20104, 00:59
type: BlogPost
tags: hacking,learning,unhosted,remotestorage,idea,javascript
list: doing
-->

# On the importance of remoteStorage module reuse

Well, the point of Unhosted modules and being explicit about your data schema in modules is so that other developers can reuse them, to make it possible for users to switch between frontend apps that consume the same kinds of data.

But there seem to be loads of people making the same kinds of apps over remoteStorage (mostly todo apps or notetaking kind of things) and making their own modules every time!

If one module doesn't offer everything a dev needs for their new app, they should hook in anyway and extend it with their own module on top. That should be pretty straightforward.

So I love [LiteWrite](http://litewrite.net), but for notetaking maybe I need a few more organisational facilities. [McNotes](https://mcnotes.5apps.com/) looked awesome and I was poised to start using it. Then I saw [Laverna](https://laverna.cc/) offers remoteStorage support and that is pretty much a straight up port of Evernote, with tags and notebooks and everything! 

When I switched from Evernote to something to something to Springpad (the ones in between didn't do the job, and I've since forgotten what they were) I had all kinds of mess importing my Evernote data, and a lot of it was done manually due to limitations of the various importers. If I'd actually started storing anything useful in LiteWrite, and wanted to move to McNotes and then to Laverna I would have gone through the exact same process... which is a horrible thought, because that's one of the problems remoteStorage is supposed to _solve_.

So, please, if you're making an app over remoteStorage, look at the other [modules that exist first](https://github.com/remotestorage/modules/tree/master/src), and use them. And if you're making a new module, please publish it! Otherwise we're missing a butt-ton of opportunities for giving users choices about their UIs, and for mashing things up in amazing ways.

(On that note, I'm keeping a list of Unhosted mashup ideas).