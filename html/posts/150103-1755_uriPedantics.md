<!---
published: 3rd January 2015, 18:23
last-modified: 3rd January 2015, 18:23
type: BlogPost
tags: hacking,slogd,blog,post,socialweb,linked data,semantic web,pedantry,uri,issue15,httpRange-14,dublin core,foaf,provenance
list: doing
-->

# URI Pedantry

In my [conversion of Blogger RSS feed to triples](https://bitbucket.org/rhiaro/blogger2ld), which I created last year sometime, I included attribution of comments like this:

```
<http://blog.rhiaro.co.uk/2011-07-27-hope-you-have-fu> dc:creator <http://www.blogger.com/profile/10705745642913528294> .
```

If you're as ridiculously obsessed with URI pedantry as I have become over the last month, alarm bells should be ringing.

`<http://www.blogger.com/profile/10705745642913528294>` is the URI of a profile document, about a person. Not the URI of a person. The comment was not created by the profile document, but the person it's about.

But wait! I [read somewhere recently](#citation-needed) that since always, [Dublin Core](http://purl.org/dc/terms/) explicitly assumes it is only ever to be used with informational resources (things that can be retrieved over HTTP). So semantically using a profile URI as an object with a DC property is correct, and it should somehow imply that the-thing-described-by-this-document is the actual creator. I'm not sure how reliably anyone would assume that. It's just sort of chance (aka obsessive reading of W3C mailing list archives) that I came across this information.

It's certainly not documented like this. `dc:creator` has a range of `dc:Agent`, a fluffy sort of class that sounds like its instances should be non-information resources (eg. people, organisations, not retrievable over HTTP) but doesn't specify either way in the docs. Not to mention, `dc:creator` has an EquivalentProperty listed: `foaf:maker`. [FOAF](http://xmlns.com/foaf/0.1/) is definitely more picky about differentiating things and documents-about-things, so this equivalence suggests the object of `dc:creator` semantically should not be a profile document after all, but a person's URI.

However the `foaf:maker` documentation suggests the two properties are different after all, recommending that `dc:creator` be used only with plain-text strings, and `foaf:maker` be used with the URI of a thing.

So what I should really be saying is:

```
<http://blog.rhiaro.co.uk/2011-07-27-hope-you-have-fu> 
    foaf:maker [
            foaf:page <http://www.blogger.com/profile/10705745642913528294> 
    ] .
```

Yay, blank nodes!

But not to worry; I'm going to consolidate it all by writing a quick script that scrapes names from the Blogger profiles and generates URIs for them to drop into my triplestore. For a couple of commentors who I know have URIs of their own, I'll manually add them for now. I'm not going to get too hung up on automating it, since I have a finite number of comments from Blogger that isn't going to grow once I deploy Slog'd.