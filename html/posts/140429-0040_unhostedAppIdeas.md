<!---
published: 29th April 2014, 00:40
last-modified: 29th April 20104, 01:05
type: BlogPost
tags: hacking,learning,unhosted,remotestorage,idea,javascript
list: todo
-->

# Unhosted App Ideas

If people release their models and data schema for remoteStorage apps, we can mash them up with much ease! I'm maintaining a list here of ideas that I may or may not get around to implementing:

* Notetaking app + bookmarking app = WebClipper functionality _and/or_ annotation of webpages
* Todo app + calendar app = duh.

Since most of the early Unhosted apps are life-organise-y ones, can we just have a dashboard that combines everything? Can we call it SORT YOUR LIFE OUT?