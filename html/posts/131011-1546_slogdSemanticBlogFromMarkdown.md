<!---
published: 11th October 2013, 15:46
last-modified: 12th October 2013, 21:36
type: BlogPost
tags: hacking,learning,semantic web,linked data,blog,markdown
list: doing
-->

# Slog'd: *S*emantic B*log* from Mark*d*own

Gotta start somewhere.  So here's a first post.

With Slog'd you will be able to:

* Write posts in markdown
* Add tags and categories
* Upload them to the server

Then when someone loads your blog:

* Any new MD is parsed and turned into triples
* They can browse through your frontend, with posts sorted however you like, within the layout you created.

I don't know how I'm going to do this yet, but I'll save my progress as I go...