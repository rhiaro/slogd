<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/newbase60.php');

$q = "CONSTRUCT { ?s ?p ?o . } WHERE { ?s ?p ?o . ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdfs.org/sioc/types#BlogPost> . }";
$res = $ep->query($q);

foreach($res['result'] as $s => $ps ){
    echo "$s<br/>";
    foreach($ps as $p => $os){
        if($p == "http://purl.org/dc/terms/created"){
            foreach($os as $o){
                $t = strtotime($o['value']);
                $s = num_to_sxg($t);
                echo "$t -> $s -> ".sxg_to_num($s)."<br/>";
            }
        }
    }
}

/*
foreach($res['result'] as $s => $ps ){
    foreach($ps as $p => $os){
        foreach($os as $o){
            echo "$s $p {$o['value']}<br/>";
        }
    }
}*/
?>
