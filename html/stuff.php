<?
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');
require_once('lib/rdfhelpers.php');

$tag = "travel";
$postlist = new Slogd_Renderer($ep);
if($postlist->render_expanded_list(array("view"=>"acquisitions"))){
	$items = $postlist->get_output();
	$c = $postlist->get_count();
}

if(isset($_GET['format'])){
  if($_GET['format'] == "ttl"){
    header("Content-Type: text/turtle");
    $ser = ARC2::getTurtleSerializer();
    echo $ser->getSerializedIndex($postlist->get_raw());
  }elseif($_GET['format'] == "json"){
    header("Content-Type: application/ld+json");
    unset($items['template']);
    $items["http://rhiaro.co.uk/stuff"] = array(
          "http://www.w3.org/ns/activitystreams#items" => array(),
          "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" => array("http://www.w3.org/ns/activitystreams#Collection")
        );
    foreach($items as $k => $v){
      // mad haxx need new renderer
      unset($items[$k]['icon']);
      unset($items[$k]['author']);
      unset($items[$k]['date']);
      $items['http://rhiaro.co.uk/stuff']['name'] = "Stuff acquired";
      if($k != "http://rhiaro.co.uk/stuff"){
        $items["http://rhiaro.co.uk/stuff"]["http://www.w3.org/ns/activitystreams#items"][] = $k;
        if(!isset($v["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"])){
          $items[$k]["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"] = array("http://www.w3.org/ns/activitystreams#Activity", "http://vocab.amy.so/blog#Acquisition");
        }
        $items[$k] = convert_pred('cost', 'http://vocab.amy.so/blog#cost', $items[$k]);
        $items[$k] = convert_pred('tags', 'tag', $items[$k]);
        
        $items[$k]["actor"] = array(array("@id" => "http://rhiaro.co.uk/about#me"));
      }
      if(isset($items[$k]["published"]) && is_object($items[$k]["published"])){
        $items[$k]["published"] = $items[$k]["published"]->format(DATE_ATOM);
      }
    }
    echo arc2array_to_as2_collection($items);
  }
}else{

  $title = "$c stuff posts";
  include("templates/home_top.php");
  $listheader = "Stuff acquired";
  $template = "post_thumb";
  
  
  ?>
  <div class="w1of1 color3-bg clearfix">
    <div class="w1of5">
      <? include 'templates/h-card.php'; ?>
    </div>
    <div class="w4of5 lighter-bg"><div class="inner">
    <? include("templates/list.php"); ?>
    </div></div>
  </div>
  <?
  include("templates/end.php");
}
?>