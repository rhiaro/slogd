<?
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');

$title = ": lifelog";
$listheader = "Llogposts";
$listintro = "Prounounced <em>yog</em>, llog is to lifelog as blog is to weblog.";

$postlist = new Slogd_Renderer($ep);
if($postlist->render_list(array("extype"=>"LlogPost"))){
    $items = $postlist->get_output();
    $c = $postlist->get_count();
}

include("templates/home_top.php");
?>
<div class="w1of1 color3-bg clearfix">
  <div class="w1of5">
    <? include 'templates/h-card.php'; ?>
  </div>
  <div class="w4of5 lighter-bg"><div class="inner">
    <? include("templates/list.php"); ?>
  </div></div>
</div>
<?
include("templates/end.php");
?>