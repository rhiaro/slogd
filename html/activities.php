<?
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');

$postlist = new Slogd_Renderer($ep);
if($postlist->render_activities()){
	$items = $postlist->get_output();
	$c = $postlist->get_count();
}

$title = ": $c activities";
include("templates/top.php");
$listheader = "Changelog";

include("templates/list_activities.php");
include("templates/list_1of2_end.php");
include("templates/end.php");

?>