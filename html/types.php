<?
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');

$tagslist = new Slogd_Renderer($ep);
if($tagslist->render_tags()){
	$tags = $tagslist->get_output();
	$count = $tagslist->get_count();
}

if(isset($_GET["type"])){
	$a_tag = true;
	$tag = ucfirst(urldecode($_GET["type"]));
	$postlist = new Slogd_Renderer($ep);
	if($postlist->render_expanded_list(array("type"=>$tag))){
		$items = $postlist->get_output();
		$c = $postlist->get_count();
	}
}else{
	$a_tag = false;
}

if($tag == "Save") { $tag = "Bookmark"; }
if($tag == "MicroblogPost") { $tag = "Short post"; }
if($tag == "BlogPost") { $tag = "Long post"; }

$title = ": ".$tag."s";
include("templates/home_top.php");
$listheader = $tag."s";
?>
<div class="w1of1 color3-bg clearfix">
  <div class="w1of5">
    <? include 'templates/h-card.php'; ?>
  </div>
  <? $trackdate = 0; ?>
  <div class="w4of5 lighter-bg"><div class="inner">
    <h1><?=$listheader?> (<?=$c?>)</h1>
    <? include 'templates/list.php'; ?>
  </div></div>
</div>
<?
include("templates/end.php");
?>