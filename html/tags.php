<?
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');

if(isset($_GET["tag"])){
	$a_tag = true;
	$tag = urldecode($_GET["tag"]);
	$postlist = new Slogd_Renderer($ep);
	if($postlist->render_expanded_list(array("tag"=>$tag))){
		$items = $postlist->get_output();
		$c = $postlist->get_count();
	}
}else{
	$a_tag = false;
}

$title = $tag;
$listheader = "Posts tagged with <span class=\"p-name\">$tag</span>";

include("templates/home_top.php");
?>
<div class="w1of1 color3-bg clearfix">
  <div class="w1of5">
    <? include 'templates/h-card.php'; ?>
  </div>
  <? $trackdate = 0; ?>
  <div class="w4of5 lighter-bg"><div class="inner">
    <h1><?=$listheader?> (<?=$c?>)</h1>
    <?if(isset($tag) && $c > 0):?>
      <? include 'templates/list.php'; ?>
    <?endif?>
    <? include("templates/list_1of2_tags.php"); ?>
  </div></div>
</div>
<?
include("templates/end.php");
?>
