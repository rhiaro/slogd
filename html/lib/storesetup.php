<?
include_once("ARC2/ARC2.php");
include_once("dbsettings.php");

/* MySQL and endpoint configuration */ 
$config = array(
  /* db */
  'db_host' => $DB_HOST, 
  'db_name' => $DB_NAME,
  'db_user' => $DB_USER,
  'db_pwd' => $DB_PW,

  /* store name */
  'store_name' => 'blog_store',

  /* endpoint */
  'endpoint_features' => array(
    'select', 'construct', 'ask', 'describe', 
    'load', 'insert', 'delete', 
    'dump' /* dump is a special command for streaming SPOG export */
  ),
  'endpoint_timeout' => 60, /* not implemented in ARC2 preview */
  'endpoint_read_key' => '', /* optional */
  'endpoint_write_key' => $EP_KEY, /* optional, but without one, everyone can write! */
);

/* instantiation */
$ep = ARC2::getStoreEndpoint($config);

if (!$ep->isSetUp()) {
  $ep->setUp(); /* create MySQL tables */
}

?>