<?

# Get ARC2
include_once("ARC2/ARC2.php");

function store_dydra_test()
{
	$triples = _get_triples();
	$storename = "about-me";
	$user = "rhiaro";
	return store_dydra($triples, $user, $storename);
}

function store($triples, $method)
{
	if($method == "mysql"){
		$result = store_mysql($triples);
	}
	elseif($method == "dydra"){
		$result = store_dydra($triples);
	}
	elseif($method == "remoteStorage"){
		$result = store_remoteStorage($triples);
	}
	elseif($method == "flat"){
		$result = store_flat($triples);
	}

	else{
		$result = array("No such storage mechanism");
	}

	return $result;
}

# Use ARC2 to store in a MySQL database
function store_mysql($triples)
{}

# Use Dydra's beta triplestore
function store_dydra($triples, $user, $storename)
{
	$url = "http://dydra.com/".$user."/".$storename."/sparql";
	$query = "prefix foaf: <http://xmlns.com/foaf/0.1/>
			insert data {";
	foreach($triples as $triple){
		$query .= $triple['s']." ".$triple['p']." ".$triple['o']." .\n";
	}
	$query .= "}";

	return query_dydra($query, $user, $storename);
}

# Temp reusing of these (will refactor...)
function query_remote_store($store, $query)
{
	$url = $store."?query=".$query; // Apparently don't need to urlencode this.
	$curl = curl_init();
	$header = array("Accept: application/rdf+xml,application/json");
	curl_setopt_array($curl, array(
									 CURLOPT_URL => $url
									,CURLOPT_HTTPHEADER => $header
									,CURLOPT_RETURNTRANSFER => 1
									)
					);
	$res = curl_exec($curl);
	if(!$res) $res = array("Error ".curl_errno($curl) => curl_error($curl));
	
	curl_close($curl);

	return $res;
}

function query_dydra($query, $user, $storename)
{
	$url = "http://dydra.com/".$user."/".$storename."/sparql";

	$res = query_remote_store($url, $query);
	
	if(is_array($res)) return $res; //Errors

	$parser = ARC2::getRDFXMLParser();
	$parser->parse("", $res); // First param is base url, not needed here.
	$triples = $parser->getTriples();
	return $triples;
}

# Connect to a remoteStorage provider
function store_remoteStorage($triples)
{}

# Store in flat RDF files
function store_flat($triples)
{}

function _get_triples()
{
	$turtle = "@prefix foaf: <http://xmlns.com/foaf/0.1/>.
				<http://rhiaro.co.uk/about#me> foaf:account [
			        a foaf:OnlineAccount ;
			        foaf:accountServiceHomepage <http://youtube.com/> ;
			        foaf:accountName \"06085668\"
			    ];";
	$parser = ARC2::getRDFParser();
	$parser->parse("", $turtle); // First param is base url, not needed here.
	$triples = $parser->getTriples();
	return $triples;
}

?>