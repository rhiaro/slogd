<?

class Slogd_Storer {

	public function __construct($endpoint, $triples){
		$this->ep = $endpoint;
		$this->triples = $this->_set_triples($triples);
		$this->errors = array();
		$this->response = array();
	}

	public function get_triples(){
		return $this->triples;
	}

	public function get_errors(){
		return $this->errors;
	}

	public function store(){
		return $this->_insert_triples();
	}

	private function _set_triples($triples){
		// TODO: validate triples
		$this->triples = $triples;
	}

	private function _insert_triples(){
		$q = "INSERT INTO <> {";
		$q += $this->triples;
		$q += "}";

    	$this->response = $this->ep->query($q);
    	// Set errors if detected
    	return $this->response;
	}

	private function _move_post($fn){
		rename("queue/$fn", "posts/$fn");
	}

}

?>
