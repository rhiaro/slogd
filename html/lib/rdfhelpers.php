<?
require_once('storesetup.php');

function ns($convert, $prefix=false){
  // URI from prefix -> false
  // Prefix from URI -> true
  $ns = array(
      "as" => "http://www.w3.org/ns/activitystreams#",
      "as2" => "http://www.w3.org/ns/activitystreams#",
      "rdf" => "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
      );
  if($prefix){
    return array_search($convert, $ns);
  }else{
    return $ns[$convert];
  }
}

function parse_turtle($file){
  $parser = ARC2::getTurtleParser();
  $parser->parse($file);
  return $parser->getSimpleIndex();
}

function prefix($to_prefix, $context=null){
  
  if($to_prefix == "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"){
    return "@type";
  }
  
  if(!$context){ // defaults
    $context = array(
      "as" => ns("as"),
      "rdf" => ns("rdf")
    );
  }
  
  if(!is_array($context)){ // one context, no prefixes needed
    return str_replace($context, "", $to_prefix);
  }
  
  // recurse if an array of URIs passed, and return array of prefixed ones
  if(is_array($to_prefix)){
    foreach($to_prefix as $uri){
      $uris[] = prefix($uri, $context);
    }
    return $uris;
  }else{
    $uri = $to_prefix;
  }
  
  // check the context for the base and prefix the uri
  foreach($context as $prefix => $base){
    if(stripos($uri, $base) !== false){
      return str_replace($base, $prefix.":", $uri);
    }
  }
  
  // didn't find prefix, return what was passed
  return $to_prefix;
}

function convert_pred($from, $to, $data){
  if(isset($data[$from])){
    $data[$to] = $data[$from];
    unset($data[$from]);
  }
  return $data;
}

function arc2array_to_as2_collection($parsed){
  $as2 = ns("as");
  $rdf = ns("rdf");
  $out = array("@context"=>"http://www.w3.org/ns/activitystreams#");
  foreach($parsed as $uri => $data){

    if(isset($data[$rdf."type"]) && in_array($as2."Collection", $data[$rdf."type"])){
      $out["@id"] = $uri;
      foreach($data[$as2."items"] as $item){
        $out["items"][$item] = array("@id" => $item);
        foreach($parsed[$item] as $property => $value){
          if(is_array($value) && count($value) == 1){
            $out["items"][$item][prefix($property, $out["@context"])] = prefix($value[0], $out["@context"]);
          }else{
            $out["items"][$item][prefix($property, $out["@context"])] = prefix($value, $out["@context"]);
          }
        }
      }
      unset($data[$as2."items"]);
      
      foreach($data as $property => $value){
        if(is_array($value) && count($value) == 1){
          $out[prefix($property, $out["@context"])] = prefix($value[0], $out["@context"]);
        }else{
          $out[prefix($property, $out["@context"])] = prefix($value, $out["@context"]);
        }
      }
      
    }
  }
  $out["items"] = array_values($out["items"]);
  return json_encode($out, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
}

function content_type_json($headers){
  $accepted = array("application/json", "application/ld+json", "application/activity+json");
  $ct = $headers['Content-type'];
  if(in_array($ct, $accepted)){ return true; }
  else { return false; }
}

function json_to_turtle($json){
  
  // TODO: Get a library to do this
  $ch = curl_init("http://rdf-translator.appspot.com/convert/json-ld/turtle/content");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "content=".urlencode($json));
  $response = curl_exec($ch);
  curl_close($ch);
  
  return $response;
}

function insert($ep, $path){
  $q = "LOAD <$path> INTO <http://blog.rhiaro.co.uk#>";
  $res = $ep->query($q);
  return $res;
}

/********
 Mapping different predicates shiz, this should probably go somewhere else
*********/

function find_published($array){
  if(isset($array["created"])) return array("key" => "created", "value" => $array["created"]);
  elseif(isset($array["published"])) return array("key" => "published", "value" => $array["published"]);
  elseif(isset($array["dct:created"])) return array("key" => "dct:created", "value" => $array["dct:created"]);
  elseif(isset($array["as:published"])) return array("key" => "as:published", "value" => $array["as:published"]);
  elseif(isset($array["as2:published"])) return array("key" => "as2:published", "value" => $array["as2:published"]);
  else return false;
}

function is_listy($key){
  $listy = array("category", "tag", "sioc:topic", "topic", "as:tag", "as2:tag");
  if(in_array($key, $listy)) return true;
  return false;
}

function find_title($array){
  $titlekeys = array("name", "as:name", "as2:name", "dct:title");
  foreach($array as $k => $v){
    if(in_array($k, $titlekeys)) return $v;
  }
  return false;
}

function find_content($array){
  $contentkeys = array("content", "as:content", "as2:content", "sioc:content");
  foreach($array as $k => $v){
    if(in_array($k, $contentkeys)) return array("key" => $k, "value" => $v);
  }
  return false;
}

/**********
 General helper stuff
**********/

function get_inner_string($start, $end, $string){
  $string = " ".$string;
  $init = strpos($string,$start);
  if ($init == 0) return "";
  $init += strlen($start);
  $len = strpos($string,$end,$init) - $init;
  if($len <= 0){ $len = strlen($string); }
  return substr($string,$init,$len);
}

?>