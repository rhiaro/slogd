<?php
require_once "MentionClient.php";

function get_links_from_markdown($md){
  $links = array();
  preg_match_all("/\[([^\[]+)\]\(([^\)]+)\)/", $md, $matches);
  foreach($matches[2] as $link){
    $links[] = $link;
  }
  return $links;
}

function links_to_html($links){
  $html = "";
  foreach($links as $link){
    $html .= "<p><a href=\"$link\">$link</a></p>";
  }
  return $html;
}

function send_webmentions_from_md($url, $md){
  $links = get_links_from_markdown($md);
  $html = links_to_html($links);
  $client = new IndieWeb\MentionClient($url, $html);
  $client->debug(true);
  $sent = $client->sendSupportedMentions();
  
  return $sent;
}

function send_webmentions($url, $links){
  $html = links_to_html($links);
  // writing log files
  /*  $log = "linkshtml_".date("ymd-Gis").".txt";
    $h = fopen($log, 'w');
    fwrite($h, $html);
    fclose($h);*/
  $client = new IndieWeb\MentionClient($url, $html);
  $client->debug(false);
  $sent = $client->sendSupportedMentions();
  
  return $sent;
}

function send_wm_to_bridgy($source){
  /* $source=http://rhiaro.co.uk/2015/06/1433853639 */
  $client = new IndieWeb\MentionClient($source);
  $client->debug(false);
  $bridgymention = $client->sendWebmention("https://www.brid.gy/publish/webmention", $source, "https://brid.gy/publish/twitter");
  /*$endpoint = "https://www.brid.gy/publish/webmention";
  $target = "http://brid.gy/publish/twitter?bridgy_omit_link=";
  $payload = http_build_query(array(
      'source' => $source,
      'target' => $target
    ));
  $headers = array(
      'Content-type: application/x-www-form-urlencoded',
      'Accept: application/json'
    );
  
  $ch = curl_init($endpoint);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  $response = curl_exec($ch);
  */
  /*$log = "logs/log_wm_".date("ymd-Gis").".txt";
  $h = fopen($log, 'w');
  fwrite($h, $source."\n");
  fwrite($h, print_r($bridgymention), true);
  fclose($h);*/
  
  return $bridgymention;
}

function test(){
  $url = "http://rhiaro.co.uk/testlink3.html";
  $md = "<!---
  tags: test,hacking,indieweb
  -->
  
  Test [rhiaro](http://rhiaro.co.uk). [post](http://rhiaro.co.uk/2015/05/socialwg3-summary)
  ";
  
  var_dump(send_webmentions($url, $md));
}


?>