<?
/**
 * Slogd URI maker
 *
 * raw uri format: http://blog.rhiaro.co.uk/unique-slug
 * rendered uri format: http://rhiaro.co.uk/yyyy/mm/unique-slug
 *
 * slugs are the post title, lowercase, non alphanumeric- chars replaced
 * with -, stopwords removed, cut off after the longest full-word-combination
 * before 16 chars long.
 */

function Slogd_makeUri($ep, $title, $datestamp=0, $type="BlogPost", $raw=true, $unique=true)
{
    $fulltitleslug = substr(strtolower(str_replace(" ", "-", preg_replace("/[^\w\d \-]/ui", '',strip_tags($title)))), 0);
    $longtitleslug = remove_stopwords($fulltitleslug);

    $done = false;
    $max = 16;
    while(!$done){
        $titleslug = decide_when_to_stop($longtitleslug, $max);
        $uri = compile_uri($titleslug, $datestamp, $type, $raw);
        if(!$unique){ // Sometimes you want the first URI generated from post metadata.
            $done = true;
            return $uri;
        }else{
            //echo "Trying: $uri<br/>";
            if(is_unique($ep, $uri)){
                //echo "Success! Unique.<br/>----<br/>";
                $done = true;
                return $uri;
            }else{
                //echo "Not unique, add another word.<br/>";
                $max = $max + 4;
                //echo "Increase max to $max<br/>";
                if($max >= strlen($longtitleslug) && strlen($titleslug) == strlen($longtitleslug)){
                    $done = true;
                    //echo "No words left in: $longtitleslug, add a number.<br/>";
                    return compile_num_uri($ep, $titleslug, 2, $datestamp, $type, $raw);
                }
            }
        }
    }
    
}

function compile_uri($titleslug, $datestamp=0, $type="BlogPost", $raw=true){
    
    if($type=="LlogPost"){ $t = "llog"; }
    else { $t = "blog"; }
    if($raw){
        $base = "http://{$t}.rhiaro.co.uk/";
        $uri = $base . $titleslug;
        
    }else{
        $base = "http://rhiaro.co.uk/";
        $dateslug = date("Y", $datestamp)."/".date("m", $datestamp)."/";
        $uri = $base . $dateslug . $titleslug;
    }

    return $uri;
}

function compile_num_uri($ep, $slug, $i=2, $datestamp, $type, $raw){
    $done = false;
    while(!$done){
        $uri = compile_uri("$slug-$i", $datestamp, $type, $raw);
        if(is_unique($ep, $uri)){
            $done = true;
            return $uri;
        }else{
            $i = $i+1;
        }
    }
}

function remove_stopwords($string){
    $stopwords = array("a", "all", "am", "an", "and", "are", "as", "at", "be","but", "by", "etc", "for", "go", "had", "has", "hasnt", "have", "he", "her", "hers", "him", "his", "how", "ie", "if", "in", "into", "is", "it", "its", "me", "my",  "nor", "not", "now", "of", "on", "or", "she", "so", "such", "than", "that", "the", "their", "them", "then", "these", "they", "this", "those", "to", "was", "which", "while", "will", "the", "your", "putting", "you", "might", "i");

    $words = explode("-", $string);
    $filtered = array();
    foreach($words as $word){
        if(!in_array(strtolower($word), $stopwords)){
            array_push($filtered, $word);
        }
    }
    return implode("-", $filtered);
}

function decide_when_to_stop($full_slug, $max=16){
    $words = explode("-", $full_slug);
    $slug = array();
    foreach($words as $word){
        
        if(empty($slug) && strlen($word) >= $max){
            // Add words from the title to the slug
            //echo "Adding $word<br/>";
            array_push($slug, $word);
        }else{
            // Until max is reached
            $current = implode("-", $slug);
            if(strlen($current) + strlen($word) <= $max){
                //echo "Adding $word<br/>";
                array_push($slug, $word);
            }else{
                //echo "No room for $word, break<br/>";
                break;
            }
        }
        //echo "deciding; max: $max, slug:".implode("-",$slug).", sluglen: ".strlen(implode("-",$slug))."<br/>";
    }
    return implode("-", $slug);
}

function is_unique($ep, $uri){
    $q = "SELECT ?o WHERE { <$uri> ?p ?o } LIMIT 1";
    $res = $ep->query($q);
    if(is_null($res['result']['rows'][0]['o'])){
        return true;
    }else{
        return false;
    }
}

function uri_from_slug($slug, $datestamp, $raw){
    if($raw){
        return "http://blog.rhiaro.co.uk/".$slug;
    }else{
        $base = "http://rhiaro.co.uk/";
        $dateslug = date("Y", $datestamp)."/".date("m", $datestamp)."/";
        return $base.$dateslug.$slug;
    }
}

function uri_from_shortlink($ep, $slug){
    $stamp = sxg_to_num($slug);
    $date = date(DATE_ISO8601, $stamp);
    $date2 = date("Y-m-d\TH:i:s\.000\Z", $stamp);
    echo $date;
    echo $date2;
    $q = "SELECT ?s WHERE { {?s <http://purl.org/dc/terms/created> \"$date\"^^<http://www.w3.org/2001/XMLSchema#datetime> . } UNION { ?s <http://purl.org/dc/terms/created> \"$date2\"^^<http://www.w3.org/2001/XMLSchema#datetime> .  } }";
    $res = $ep->query($q);
    var_dump(htmlentities($q));
    if(is_null($res['result']['rows'][0]['s'])){
        return false;
    }else{
        return $res['result']['rows'][0]['s'];
    }
}

function rendered_from_raw($uri, $datestamp){
    $slug_ar = explode("http://blog.rhiaro.co.uk/", $uri);
    $slug = $slug_ar[1];
    $base = "http://rhiaro.co.uk/";
    $dateslug = date("Y", $datestamp)."/".date("m", $datestamp)."/";
    return $base.$dateslug.$slug;
}

function get_type_uri($type){
    $mapping = array(
                "BlogPost" => "http://rdfs.org/sioc/types#BlogPost",
                "MicroblogPost" => "http://rdfs.org/sioc/types#MicroblogPost",
                "InfoPost" => "http://vocab.amy.so/blog#InfoPost",
                "LlogPost" => "http://vocab.amy.so/blog#LlogPost",
                "Like" => "http://www.w3.org/ns/activitystreams#Like",
                "Share" => "http://www.w3.org/ns/activitystreams#Share",
                "Save" => "http://www.w3.org/ns/activitystreams#Save"
        );
    return $mapping[$type];
}

function get_property_uri($p){
    $mapping = array(
                 "like" => "http://vocab.amy.so/blog#like_of"
                ,"save" => "http://vocab.amy.so/blog#bookmark_of"
                ,"share" => "http://vocab.amy.so/blog#share_of"
                ,"title" => "http://purl.org/dc/terms/title"
                ,"follow" => "http://vocab.amy.so/blog#follow_of"
        );
    return $mapping[$p];
}

function get_list_uri($list){
    $mapping = array(
                "todo" => "http://vocab.amy.so/blog#Todo",
                "doing" => "http://vocab.amy.so/blog#Doing",
                "done" => "http://vocab.amy.so/blog#Done"
        );
    return $mapping[$list];
}

?>