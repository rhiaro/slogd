<?
include_once "Slogd_makeUri.php";
include_once "webmention.php";
include_once "Mf2/Parser.php";

class Slogd_Tripler {

	public function __construct($endpoint, $sourcePath, $uri=null){

		$this->ep = $endpoint;
		$this->uri_base = "http://blog.rhiaro.co.uk/";
		$this->ns = array(
				 'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'
				,'foaf' =>  'http://xmlns.com/foaf/0.1/'
				,'dc' => 'http://purl.org/dc/elements/1.1/'
				,'dct' => 'http://purl.org/dc/terms/'
				,'sioc' => 'http://rdfs.org/sioc/types#'
				,'blog' => 'http://vocab.amy.so/blog#'
				,'as2' => 'http://www.w3.org/ns/activitystreams#'
			);

		// Map my metadata labels (keys) onto actual predicates (values)
		// Includes whether the values of each predicate should be literals ("lit"=>true) or not (ie. the value is a URI)
		$this->predicates = array(
				 "published" => array(
				 					 "uri" => $this->ns['dct']."created"
				 					,"lit" => true
				 					,"type" => "date"
				 				)
				,"modified" => array(
									 "uri" => $this->ns['dct']."modified"
									,"lit" => true
									,"type" => "date"
								)
				,"last-modified" => array(
									 "uri" => $this->ns['dct']."modified"
									,"lit" => true
									,"type" => "date"
								)
				,"title" => array(
								 "uri" => $this->ns['dct']."title"
								,"lit" => true
								,"type" => "lit"
							)
				,"content" => array(
									 "uri" => $this->ns['sioc']."content"
									,"lit" => true
									,"type" => "lit"
								)
				,"type" => array(
								 "uri" => $this->ns['rdf']."type"
								,"lit" => false
								,"type" => "uri"
							)
				,"list" => array(
								 "uri" => $this->ns['sioc']."topic"
								,"lit" => false
								,"type" => "uri"
							)
				,"tags" => array(
								 "uri" => $this->ns['sioc']."topic"
								,"lit" => true
								,"type" => "lit"
							)
				,"reply-to" => array(
									 "uri" => $this->ns['sioc']."reply_of"
									,"lit" => false
									,"type" => "uri"
								)
				,"like-of" => array(
									 "uri" => $this->ns['blog']."like_of"
									,"lit" => false
									,"type" => "uri"
								)
				,"share-of" => array(
									 "uri" => $this->ns['blog']."share_of"
									,"lit" => false
									,"type" => "uri"
								)
				,"repost-of" => array(
									 "uri" => $this->ns['blog']."share_of"
									,"lit" => false
									,"type" => "uri"
								)
				,"bookmark-of" => array(
									 "uri" => $this->ns['blog']."bookmark_of"
									,"lit" => false
									,"type" => "uri"
								)
				,"follow-of" => array(
									 "uri" => $this->ns['blog']."follow_of"
									,"lit" => false
									,"type" => "uri"
								)
				,"author" => array(
									 "uri" => $this->ns['dc']."creator"
									,"lit" => false
									,"type" => "uri"
								)
				,"primary-topic-of" => array(
									 "uri" => $this->ns['foaf']."isPrimaryTopicOf"
									,"lit" => false
									,"type" => "uri"
								)
				,"start" => array(
									 "uri" => $this->ns['as2']."startTime"
									,"lit" => true
									,"type" => "date"
								)
				,"end" => array(
									 "uri" => $this->ns['as2']."endTime"
									,"lit" => true
									,"type" => "date"
								)
				,"location" => array(
									 "uri" => $this->ns['as2']."location"
									,"lit" => false
									,"type" => "uri"
								)
				,"latitude" => array(
									 "uri" => $this->ns['as2']."latitude"
									,"lit" => true
									,"type" => "lit"
								)
				,"longitude" => array(
									 "uri" => $this->ns['as2']."longitude"
									,"lit" => true
									,"type" => "lit"
								)
				,"to" => array(
									 "uri" => $this->ns['as2']."to"
									,"lit" => false
									,"type" => "uri"
								)
				,"cc" => array(
									 "uri" => $this->ns['as2']."cc"
									,"lit" => false
									,"type" => "uri"
								)
				,"state" => array(
				           "uri" => $this->ns['blog']."state"
				          ,"lit" => true
				          ,"type" => "lit"
				        )
				,"isReplacedBy" => array(
				           "uri" => $this->ns['dct']."isReplacedBy"
				          ,"lit" => false
				          ,"type" => "uri"
				        )
				// For anything that appears that's not in this array:
				,"default" => array(
									 "uri" => $this->ns['blog']
									,"lit" => true
									,"type" => "lit"
								)
					);

		// Defaults for things that shouldn't really be missing
		// Just add more here as needed. Use the labels used in the posts md as the keys.
		// TODO: not sure these work...
		$this->defaults = array(
							 "author" => "http://rhiaro.co.uk/about#me"
							,"published" => date("Y-m-d, H:i:s", time())
			);

		$this->triples = array();
		$this->title = false;
		$this->content = "";
		$this->paths = array();
		$this->errors = array();
		$this->last_query = "";
		$this->last_result = array();
		$this->uri = $uri;
		$this->url = null;
		$this->links = array();
		// Make triples
		$this->_set_triples($sourcePath);

	}

	#############################
	# Getting stuff from outside
	#############################

	public function get_triples(){
		return $this->triples;
	}

	public function get_title(){
		return $this->title;
	}
	
	public function get_content(){
	  return $this->content;
	}
	
	public function get_links(){
	  return $this->links;
	}

	public function get_url(){
		return $this->url;
	}

	public function get_paths(){
		return $this->paths;
	}

	public function get_query(){
		return $this->last_query;
	}

	public function get_results(){
		return $this->last_result;
	}

	public function get_errors(){
		return $this->errors;
	}

	public function blog_last_updated(){
		return get_last_updated();
	}

	#############################
	# Insert triples
	#############################

	public function insert($index, $sendwm=true, $syndicate=false){
		$triples = $this->get_triples();
		$triples = $triples[$index];
		if($this->uri){
			$dq = "DELETE FROM <http://blog.rhiaro.co.uk#> { <".$this->uri."> ?p ?o } WHERE { <".$this->uri."> ?p ?o }";
		}
		$q = "INSERT INTO <http://blog.rhiaro.co.uk#> { ".$triples." }";
		$this->_set_query($q);

		//var_dump(htmlentities($dq));
		//echo "<hr/>";
		//var_dump(htmlentities($q));

		if($dq){
			$dres = $this->ep->query($dq);
			if($this->ep->getErrors()){
				$this->_set_errors("Problem with delete $index", $this->ep->getErrors());
				return false;
			}
		}
		$res = $this->ep->query($q);
		if(!$this->ep->getErrors()){
			$this->_set_results($res);
			if($sendwm){
			  send_webmentions($this->url, $this->links);
			}
			if($syndicate){
			  // This runs twice
			  send_wm_to_bridgy($this->url);
			}
			return true;
		}else{
			$this->_set_errors("Problem with insert $index", $this->ep->getErrors());
			return false;
		}
	}
	
	public function insert_person($uri, $name, $depic, $url=null){
	  if(!$url) { $url = $uri; }
	  $q = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT INTO <http://blog.rhiaro.co.uk/authors#> { <{$uri}> a foaf:Person; foaf:name \"{$name}\"; foaf:depiction <{$depic}>; foaf:homepage <{$url}> . }";
    $this->_set_query($q);
    $res = $this->ep->query($q);
    if(!$this->ep->getErrors()){
      $this->_set_results($res);
      return true;
    }else{
      $this->_set_errors("Problem inserting $uri", $this->ep->getErrors());
      return false;
    }
	}

	public function insert_all(){

		foreach($this->get_triples() as $i => $t){
			if(!$this->insert($i)){
				// Stop if one goes wrong. TODO: Handle this better.
				break;
			}
		}
		if(!$this->ep->getErrors()){
			update_last_updated();
			return true;
		}else{
			return false;
		}
	}

	

	private function _set_query($query){
		$this->last_query = $query;
	}

	private function _set_results($results){
		$this->last_result = $results;
	}

	######################################
	# Getting source (md + metadata)
	# from source location
	######################################

	private function _get_source_from_path($path)
	{
		$post = array();
		if(file_exists($path)){
			$text = file_get_contents($path);
			$postmeta = $this->_get_metadata_array($text);
			$post['text'] = $text;
			$post['meta'] = $postmeta;
		}
		// Make a sensible error for file not exists.
		return $post;
	}

	private function _get_modified_from_path($path){
		return filemtime($path);
	}

	private function _get_source_from_email($email)
	{
		// Do email parsing stuff
	}

	############################
	# Parse content
	############################

	private function _get_metadata_array($post_md)
	{
		$metadata = $this->_extract_metadata($post_md);
		$metadataAr = explode("\n", $metadata);
		array_shift($metadataAr); # Lose empty first line
		array_pop($metadataAr); # Lose empty last line
		$result = array();
		foreach($metadataAr as $line){
			$label = substr($line,0,strpos($line,': '));
			$property = substr($line,strpos($line,':')+2);
			if($this->_is_listy_metadata($label)){
				$property = explode(",", $property);
				$property = array_map('trim', $property);
			}else{
				$property = trim($property);
			}
			$result[$label] = $property;
		}

		return $result;
	}

	private function _get_inner_string($start, $end, $string)
	{
		$string = " ".$string;
	    $init = strpos($string,$start);
	    if ($init == 0) return "";
	    $init += strlen($start);
	    $len = strpos($string,$end,$init) - $init;
	    if($len <= 0){ $len = strlen($string); }
	    return substr($string,$init,$len);
	}

	private function _extract_metadata($post_md)
	{
		# Post metadata is encapsulated in HTML comments
		return $this->_get_inner_string("<!---", "-->", $post_md);
	}

	private function _extract_content($post_md){
		return trim(substr($post_md, strpos($post_md, "-->") + 3));
	}

	private function _is_listy_metadata($label)
	{
		if($label == "tags" || $label == "labels"){
			return true;
		}
		return false;
	}
	
	private function _get_external($url){
		$parsed = Mf2\fetch($url);
		$post = array();
		foreach($parsed['items'] as $h){
			if(in_array("h-entry", $h['type'])){
				$author = $h['properties']['author'][0]['properties'];
				$post['author_name'] = $author['name'][0];
				$post['author_photo'] = $author['photo'][0];
				$post['author_url'] = $author['url'][0];
				$post['title'] = $h['properties']['name'][0];
				$post['content'] = $h['properties']['content'][0]['value'];
				$post['pubdate'] = $h['properties']['published'][0];
				break;
			}
		}
		
		return $post;
	}
	
	private function _get_external_creator($url){
	  $parsed = Mf2\fetch($url);
	  if(is_array($parsed['items'])){
  	  foreach($parsed['items'] as $h){
  	    if(in_array("h-entry", $h['type'])){
  	      $a = $h['properties']['author'][0]['properties'];
  	      break;
  	    }
  	  }
  	  if(isset($a)){
    	  $uri = $a['url'][0];
    	  
    	  $trip = new Slogd_Tripler($this->ep, null, null);
    	  $res = $trip->insert_person($uri, $a['name'][0], $a['photo'][0], $a['url'][0]);
    	  echo "---inserting happened--\n";
    	  var_dump($url);
    	  var_dump($uri);
    	  echo "<hr/>\n";
    	  $trip->add_triple($url, "author", $uri);
    	  $creatorres = $trip->insert(0);
    	  
    	  $this->known_authors[$uri]['name'] = $a['name'][0];
      	$this->known_authors[$uri]['photo'] = $a['photo'][0];
      	$this->known_authors[$uri]['url'] = $a['url'][0];
    	
    	  return $a['url'][0];
  	  }
	  }
	  return false;
	}


	##########################
	# Make triples
	##########################

	private function _set_triples($input)
	{
		$paths = array();
		// If it's not already an array of paths, make it so.
		if(!is_array($input)){
			// If it's a directory, get paths.
			if(is_dir($input)){
				$foundpaths = scandir($input, 1);
				for($i=0;$i<count($foundpaths);$i++){
					$tmp = $input . "/" . $foundpaths[$i];
					if($foundpaths[$i] != "." && $foundpaths[$i] != ".." && !is_dir($tmp)){
						$paths[] = $tmp;
					}
				}
			}else{
				// Array containing just the one path.
				$paths[] = $input;
			}
		}else{
			// Use input as is.
			$paths = $input;
		}
		$this->_set_paths($paths);
		// Make triples from paths if the file modified time is more recently than blog last updated - no
		//$last_updated = $this->_get_last_updated();
		foreach($paths as $path){
			$mod = $this->_get_modified_from_path($path);
			//if($mod > $last_updated){
			$post = $this->_get_source_from_path($path);
			if(!empty($post)){
				array_push($this->triples, $this->_make_triples_from_post($post));
			}
			//}
		}

	}

	private function _make_triples_from_post($post)
	{
		$triples = "";

		// Extract post title from content
		$post['meta']['tmpcontent'] = $this->_extract_content($post['text']);
		$post['meta']['title'] = trim($this->_get_inner_string("#", "\n", $post['meta']['tmpcontent']));
		if($post['meta']['title'] == ""){
			if($post['meta']['type'] == "LlogPost"){
				$post['meta']['published'] = $post['meta']['end'];
				unset($post['meta']['tmpcontent']);
			}
			unset($post['meta']['title']);
		}
		if($post['meta']['tmpcontent'] == ""){
		  unset($post['meta']['tmpcontent']);
		}

		// Make URI for post
		if($this->uri){
			$uri = $this->uri;
		}else{
			$post = $this->_set_post_defaults($post);
			$uri = $this->_make_uri($post);
			$this->uri = $uri;
		}

		// Make URIs for non-literals
		// Author:
		if($post['meta']['author']){
			if(substr($post['meta']['author'], 0, 7) != "http://" && substr($post['meta']['author'], 0, 8) != "https://"){
			// TODO: replace this with regex(?) ^
				$post['meta']['author'] = "http://".$post['meta']['author'];
			}
		}
		// Posts:
		if($post['meta']['reply-to']){
			if(substr($post['meta']['reply-to'], 0, 7) != "http://" && substr($post['meta']['reply-to'], 0, 8) != "https://"){
				$post['meta']['reply-to'] = "http://".$post['meta']['reply-to'];
			}
			array_push($this->links, $post['meta']['reply-to']);
		}
		// Type: Shouldn't be using any more, but here just in case I need to declare explicit type in metadata for some reason.
		if($post['meta']['type']){
			$post['meta']['type'] = get_type_uri($post['meta']['type']);
		}
		// List:
		if($post['meta']['list']){
			$post['meta']['list'] = get_list_uri($post['meta']['list']);
		}
		// Rendered at (foaf:isPrimaryTopicOf)
		if($post['meta']['type'] != $this->ns['blog']."LlogPost"){
			$post['meta']['primary-topic-of'] = rendered_from_raw($uri, strtotime($post['meta']['published']));
			$this->url = $post['meta']['primary-topic-of'];
		}else{
			$this->url = $uri;
		}

		// Turn content into triples
		$triples .= "<$uri> <{$this->predicates['content']['uri']}> \"\"\"".addslashes($post['text'])."\"\"\" .\n";
		$this->content = $post['text'];
		
		// Get links
		//$contentlinks = get_links_from_markdown($post['text']);
		//foreach($contentlinks as $l){
		//  $this->links[] = $l;
		//}
		if($post['meta']['like-of']){
		  array_push($this->links, $post['meta']['like-of']);
		}
		if($post['meta']['bookmark-of']){
		  array_push($this->links, $post['meta']['bookmark-of']);
		}
		if($post['meta']['share-of']){
		  array_push($this->links, $post['meta']['share-of']);
		}

		// Turn metadata into triples
		foreach($post['meta'] as $prop => $val){
			// Check property has had a predicate URI set
			if(!array_key_exists($prop, $this->predicates)){
				// Set to default if not
				$predicate = $this->predicates['default']['uri'].$prop;
				$type = $this->predicates['default']['type'];
			}else{
				$predicate = $this->predicates[$prop]['uri'];
				$type = $this->predicates[$prop]['type'];
			}
			// Non-listy metadata
			if(!is_array($val)){
				// Literals metadata
				if($type == "lit"){
					$triples .= "<$uri> <$predicate> \"\"\"".addslashes($val)."\"\"\" .\n";
				// Things with URIs metadata
				}elseif($type == "uri"){
					$triples .= "<$uri> <$predicate> <$val> .\n";
				}elseif($type == "date"){
					$triples .= "<$uri> <$predicate> ".$this->_make_date($val)." . \n";
				}
			// Listy metadata (assumes always literals for now)
			}else{
				foreach($val as $item){
					$triples .= "<$uri> <$predicate> \"\"\"".addslashes($item)."\"\"\" .\n";
				}
			}
		}

		return $triples;

	}

	function add_triple($s, $p, $o){
		$newtriple = "";
		if(empty($s)){ $s = $this->uri; }
		$uri = $s;
		if(!array_key_exists($p, $this->predicates)){
			$predicate = $this->predicates['default']['uri'].$p;
			$type = $this->predicates['default']['type'];
		}else{
			$predicate = $this->predicates[$p]['uri'];
			$type = $this->predicates[$p]['type'];
		}
		// Non-listy metadata
		if(!is_array($o)){
			// Literals metadata
			if($type == "lit"){
				$newtriple .= "<$uri> <$predicate> \"\"\"".addslashes($o)."\"\"\" .\n";
			// Things with URIs metadata
			}elseif($type == "uri"){
				$newtriple .= "<$uri> <$predicate> <$o> .\n";
			}elseif($type == "date"){
				$newtriple .= "<$uri> <$predicate> ".$this->_make_date($o)." . \n";
			}
		// Listy metadata (assumes always literals for now)
		}else{
			foreach($o as $item){
				$newtriple .= "<$uri> <$predicate> \"\"\"".addslashes($item)."\"\"\" .\n";
			}
		}
		$this->triples[0] .= $newtriple;
	}

	##########################
	# General useful things
	##########################

	private function _make_uri($post, $raw=true){
		if(isset($post['meta']['title']) && $post['meta']['title'] != ""){ $title = $post['meta']['title']; }
		elseif(isset($post['meta']['tmpcontent'])) { $title = strip_tags($post['meta']['tmpcontent']); }
		else{ $title = strtotime($post['meta']['published']); }
		$uri = Slogd_makeUri($this->ep, $title, strtotime($post['meta']['published']), $post['meta']['type'], $raw);
		return $uri;
	}

	private function _make_first_uri($post, $raw=true){
		// Make URI prior to it getting uniquified.
		$uri = Slogd_makeUri($this->ep, $post['meta']['title'], strtotime($post['meta']['published']), $post['meta']['type'], $raw, true);
		return $uri;
	}

	private function _post_exists($post){
		$q = "SELECT ?d WHERE { <$uri> ?p ?d } LIMIT 1";
	    $res = $ep->query($q);
	    if(is_null($res['result']['rows'][0]['d'])){
	        return true;
	    }else{
	        return false;
	    }
	}

	private function _make_date($string){
		$date = date(DATE_ISO8601, strtotime($string));
		return "\"$date\"^^<http://www.w3.org/2001/XMLSchema#datetime>";
	}

	private function _set_post_defaults($post)
	{
		foreach($this->defaults as $p => $o){
			if(!$post['meta'][$p] || $post['meta'][$p] == ""){
				$post['meta'][$p] = $o;
			}
		}

		return $post;
	}

	private function _set_paths($paths){
		$this->paths = $paths;
	}

	private function _set_errors($key, $error){
		$this->errors[$key] = $error;
	}

}

########################
# Update time          #
########################

function update_last_updated($ep){
	insert_last_updated($ep);
}

function insert_last_updated($ep, $when=0){
	if($when == 0){ $when = time(); }
	$delq = "DELETE FROM <sys> { <http://blog.rhiaro.co.uk> <http://purl.org/dc/terms/modified> ?t }
				WHERE { GRAPH <sys> { <http://blog.rhiaro.co.uk> <http://purl.org/dc/terms/modified> ?t } }";
	$insq = "INSERT INTO <sys> { <http://blog.rhiaro.co.uk> <http://purl.org/dc/terms/modified> \"".$when."\" }";
	$dres = $ep->query($delq);
	$ires = $ep->query($insq);
	if($ep->getErrors()){
		echo "Could not update blog modified time. ".$ep->getErrors();
		return false;
	}else{
		return true;
	}
}

function get_last_updated($ep){
	$q = "SELECT ?t WHERE { GRAPH <sys> { <http://blog.rhiaro.co.uk> <http://purl.org/dc/terms/modified> ?t . } }";
	$res = $ep->query($q);
	if($ep->getErrors()){
		echo "Could not get blog modified time.".$ep->getErrors();
		return false;
	}else{
		return $res['result']['rows'][0]['t'];
	}
}


?>