<?
# Get ARC2
include_once("ARC2/ARC2.php");

function init_remote_store($endpoint)
{
	$config = array(
		/* Remote endpoint */
		'remote_store_endpoint' => $endpoint
		);
	$store = ARC2::getRemoteStore($config);
	return $store;
}

function query_remote_store($store, $query)
{
	$url = $store."?query=".$query; // Apparently don't need to urlencode this.
	$curl = curl_init();
	$header = array("Accept: application/rdf+xml,application/json");
	curl_setopt_array($curl, array(
									 CURLOPT_URL => $url
									,CURLOPT_HTTPHEADER => $header
									,CURLOPT_RETURNTRANSFER => 1
									)
					);
	$res = curl_exec($curl);
	if(!$res) $res = array("Error ".curl_errno($curl) => curl_error($curl));
	
	curl_close($curl);

	return $res;
}

function query_dydra($query, $user, $storename)
{
	$url = "http://dydra.com/".$user."/".$storename."/sparql";

	$res = query_remote_store($url, $query);
	
	if(is_array($res)) return $res; //Errors

	$parser = ARC2::getRDFXMLParser();
	$parser->parse("", $res); // First param is base url, not needed here.
	$triples = $parser->getTriples();
	return $triples;
}

function get_all_triples_from_dydra($user, $storename)
{
	$query = "construct {?s ?p ?o} where {?s ?p ?o}";
	return query_dydra($query, $user, $storename);
}


function get_triples_html()
{
	$html = "";
	$triples_ar = get_all_triples_from_dydra("rhiaro", "about-me");
	foreach($triples_ar as $triple){
		$html .= "<p><strong>".$triple['s']."</strong> - ".$triple['p']." - <strong>".$triple['o']."</strong></p>";
	}

	return $html;
}
?>