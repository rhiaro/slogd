<?

# Install PSR-0-compatible class autoloader
spl_autoload_register(function($class){
	require preg_replace('{\\\\|_(?!.*\\\\)}', DIRECTORY_SEPARATOR, ltrim($class, '\\')).'.php';
});

include_once "Slogd_makeUri.php";
include_once "Mf2/Parser.php";

# Get Markdown class
#use \Michelf\MarkdownExtra;
include_once "Parsedown.php";

#######################################
# The Renderer
# Everything done in this class is to
# get stuff from the triplestore and
# display it relatively nicely.
# Also add microformats.
# TODO: Var names from camelCase to under_scored
#######################################

class Slogd_Renderer {

	public function __construct($endpoint){
		$this->ep = $endpoint;
		$this->uri_base = "http://blog.rhiaro.co.uk/";
		$this->ns = array(
				 'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'
				,'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#'
				,'foaf' =>  'http://xmlns.com/foaf/0.1/'
				,'dc' => 'http://purl.org/dc/elements/1.1/'
				,'dct' => 'http://purl.org/dc/terms/'
				,'sioc' => 'http://rdfs.org/sioc/types#'
				,'blog' => 'http://vocab.amy.so/blog#'
				,'as2' => 'http://www.w3.org/ns/activitystreams#'
				,'mf2' => 'http://microformats.org/profile/'
			);

		$this->output = "";
		$this->title = null;
		$this->count_triples = 0;
		$this->raw = array();
		$this->errors = array();
		$this->query = "";
		$this->known_authors = array();
	}

	#############################
	# Getting stuff from outside
	#############################

	public function get_output(){
		return $this->output;
	}

	public function get_title(){
		return $this->title;
	}

	public function get_count(){
		return $this->count_triples;
	}

	public function get_errors(){
		return $this->errors;
	}

	public function get_raw(){
		return $this->raw;
	}

	public function get_query($encoded=false){
		if($encoded){
			return htmlentities($this->query);
		}else{
			return $this->query;
		}
	}

	public function render_list($list=array()){
		
		$lists = $this->_get_posts_by_list($list);
		if(!$this->get_errors()){
			$this->_make_html_post_list($lists);
			return true;
		}
	
		return false;
	}
	
	public function render_expanded_list($list=array()){
		
		$lists = $this->_get_posts_by_list($list);
		if(!$this->get_errors()){
			$this->_make_html_expanded_list($lists);
			return true;
		}
	
		return false;
	}
	
	public function render_activities(){
		
		$lists = $this->_get_posts_by_list(array());
		if(!$this->get_errors()){
			$this->_make_html_activity_list($lists);
			return true;
		}

		return false;
	}

	public function render_post($postId){
		// TODO: validate id and type
		$postUri = $this->_make_uri_from_id($postId);
		$post = $this->_get_post($postUri);
		if($post){
			$this->_make_html_post($post);
		}else{
			return false;
		}
	}

	public function render_post_and_replies($page){
		$post = $this->_get_post_and_replies($page);
		if(!$this->get_errors()){
			$this->_make_html_expanded_list($post);
			return true;
		}
	}

	public function render_tags(){
		$tags = $this->_get_tags();
		if($tags){
			$this->_make_html_tags($tags);
			return true;
		}else{
			return false;
		}
	}

	public function render_checkins($location="?l"){
	  // HERENOW
		$checkins = $this->_get_by_location($location);
		$events_pre = $this->_get_posts_by_list(array("view"=>"calendar"));
		
		$locations = array();
		$vague = array();
		$specific = array();
		$events = array();
		
		$vague_pre = array();
		$specific_pre = array();
		$posts = array();
		
		if(!$this->get_errors()){
			//$this->_make_html_checkin($checkins);
			foreach($checkins as $uri => $post){
			  
			  // Get locations metadata
			  if(!isset($post[$this->ns['as2']."location"])){
  			  $locations[$uri]['slug'] = $post[$this->ns['rdfs']."label"][0]['value'];
  		    $locations[$uri]['past'] = $post[$this->ns['blog']."pastLabel"][0]['value'];
  		    $locations[$uri]['present'] = $post[$this->ns['blog']."presentLabel"][0]['value'];
			  }else{
			    $posts[$uri] = $post;
			  }
			}
			
			// Sort vague and specific
		  foreach($posts as $uri => $post){
		    foreach($post[$this->ns['as2']."location"] as $location){
		      // Check if it's one of my locations (assumed to be vague)
		      if(stripos($location['value'], "rhiaro.co.uk/location/")){
		        $vague_pre[$uri] = $post;
		      }else{
		        $specific_pre[$uri] = $post;
		      }
		    }
		  }
		  
		  // Prep vague for display
		  $prev = time();
		  foreach($vague_pre as $uri => $post){
  		  $cur = strtotime($post[$this->ns['dct']."created"][0]['value']);
  			$vague[$uri]['location'] = $post[$this->ns['as2']."location"][0]['value'];
  			$vague[$uri]['published'] = new DateTime($post[$this->ns['dct']."created"][0]['value']);
  			$vague[$uri]['duration'] = $this->hours_and_minutes($this->time_difference($cur, $prev));
  		  $vague[$uri]['d'] = $this->time_difference($cur, $prev);
  		  if(isset($post[$this->ns['sioc']."content"])){
  		    $vague[$uri]['content'] = $post[$this->ns['sioc']."content"][0]['value'];
  		  }
  		  $prev = $cur;
		  }
		  
		  // Prep specific and events for display
		  // TODO: Exclude events
		  $prev_s = time();
		  foreach($specific_pre as $uri => $post){
		    if(!isset($post[$this->ns['as2']."startTime"])){
  		    $cur = strtotime($post[$this->ns['dct']."created"][0]['value']);
  		    $specific[$uri]['location'] = $post[$this->ns['as2']."location"][0]['value'];
    			$specific[$uri]['published'] = $cur;
    			if(isset($post[$this->ns['sioc']."content"])){
    		    $specific[$uri]['content'] = $post[$this->ns['sioc']."content"][0]['value'];
    		  }
    		  if(isset($post[$this->ns['as2']."latitude"]) && isset($post[$this->ns['as2']."longitude"])){
    		    $lat = $post[$this->ns['as2']."latitude"][0]['value'];
    		    $lon = $post[$this->ns['as2']."longitude"][0]['value'];
    		    $zoom = 12;
    		    $specific[$uri]['x'] = floor((($lon + 180) / 360) * pow(2, $zoom));
            $specific[$uri]['y'] = floor((1 - log(tan(deg2rad($lat)) + 1 / cos(deg2rad($lat))) / pi()) /2 * pow(2, $zoom));
    		  }
    		  $specific[$uri]['d'] = $this->time_difference($cur, $prev_s)-128;
    		  $prev_s = $cur;
		    }
		  }
		  
		  $now = time();
		  $prev_ev = time();
		  $prev_d = 0;
		  foreach($events_pre as $uri => $post){
		    $st = strtotime($post[$this->ns['as2']."startTime"][0]['value']);
		    if($st > strtotime("2015-01-01")){
		      $et = strtotime($post[$this->ns['as2']."endTime"][0]['value']);
		    
  		    if(isset($post[$this->ns['as2']."location"])){
  		      $events[$uri]['location'] = $post[$this->ns['as2']."location"][0]['value'];
  		    }else{
  		      if(isset($post[$this->ns['blog']."startLocation"])){
    		      $events[$uri]['startLocation'] = $post[$this->ns['blog']."startLocation"][0]['value'];
  		      }
  		      if(isset($post[$this->ns['blog']."endLocation"])){
    		      $events[$uri]['endLocation'] = $post[$this->ns['blog']."endLocation"][0]['value'];
    		    }
    		    if(isset($post[$this->ns['as2']."origin"])){
    		      $events[$uri]['startLocation'] = $post[$this->ns['as2']."origin"][0]['value'];
  		      }
  		      if(isset($post[$this->ns['as2']."target"])){
    		      $events[$uri]['endLocation'] = $post[$this->ns['as2']."target"][0]['value'];
    		    }
  		    }
    			$events[$uri]['published'] = $post[$this->ns['dct']."created"][0]['value'];
    			$events[$uri]['start'] = $st;
    			$events[$uri]['end'] = $et;
    			
    			if(isset($post[$this->ns['dct']."title"])){
    			  $events[$uri]['name'] = $post[$this->ns['dct']."title"][0]['value'];
    			}elseif(isset($post[$this->ns['as2']."name"])){
    			  $events[$uri]['name'] = $post[$this->ns['as2']."name"][0]['value'];
    			}elseif(isset($post[$this->ns['sioc']."content"])){
    		    $events[$uri]['content'] = $post[$this->ns['sioc']."content"][0]['value'];
    		  }
    		  
    		  if($et > $now){
    		    $et = $now;
    		  }
  		    $events[$uri]['d'] = $this->time_difference($st, $et);
    		  $events[$uri]['gap'] = $this->time_difference($st, $prev_ev)-$events[$uri]['d'];
    		  $events[$uri]['gaphrs'] = $this->hours_and_minutes($events[$uri]['gap']);
    		  
    		  $events[$uri]['duration'] = $this->hours_and_minutes($events[$uri]['d']);
    		  $prev_ev = $st;
    		  $prev_d = $events[$uri]['d'];
		    }
		  }
			
			$this->_set_output(array("locations"=>$locations,"vague"=>$vague,"specific"=>$specific,"events"=>$events));
			return true;
		}else{
			return false;
		}
	}
	
	public function render_external($url){

		$mf = Mf2\fetch($url);

		return $mf;
	}
	
	public function json_dump_activities(){
		
		$lists = $this->_get_posts_by_list(array());
		if(!$this->get_errors()){
			$this->_make_json_activity_list($lists);
			return true;
		}

		return false;
	}
	
	############################
	# Sorting output
	############################
  public function index_by($i="uri"){
    if($i == "uri"){
      // TODO: deal
      $this->_set_output($this->get_output());
    }elseif($i == "date"){
      $sorted = array();
      foreach($this->get_output() as $uri => $data){
        if($uri != "template"){
          $data['uri'] = $uri;
          $sorted[$data['date']] = $data;
        }
      }
      ksort($sorted);
      $sorted = array_reverse($sorted);
      $this->_set_output($sorted);
    }elseif($i == "period"){
      $future = array();
      $present = array();
      $past = array();
      $now = time();
      $today = date("Y-m-d", $now);
      foreach($this->get_output() as $data){
        if($data['date'] > $now){
          $future[$data['date']] = $data;
        }elseif(($now > $data['start'] && $now <= $data['end']) || date("Y-m-d", $data['date']) == $today){
          $present[$data['date']] = $data;
        }else{
          $past[$data['date']] = $data;
        }
      }
      ksort($future);
      krsort($present);
      krsort($past);
      $this->_set_output(array("today" => $present, "coming up" => $future, "past" => $past));
    }
  }

	############################
	# Getting things from store
	############################

	public function make_query($query_name, $query_params=array()){
		# Queries array "name" => ["query", "no of params expected"]
		# Param count match is checked before query is executed to avoid weirdness.
		# If query appears valid, set $this->query to the query.
		$queries = array(

						"construct-all-by-subject" =>
							array("
CONSTRUCT { <{$query_params[0]}> ?p ?o } WHERE {
	<{$query_params[0]}> ?p ?o .
}", 1),

						"construct-post-and-replies" =>
							array("
PREFIX blog: <http://vocab.amy.so/blog#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX as2: <http://www.w3.org/ns/activitystreams#>

construct {
    ?post ?p ?o .
    ?post sioc:has_reply ?reply .
}
where {
	?post ?p ?o .
	?post foaf:isPrimaryTopicOf <{$query_params[0]}> .
	optional { <{$query_params[0]}> sioc:has_reply ?reply . }

}", 1),

						"construct-all" =>
							array("
PREFIX blog: <http://vocab.amy.so/blog#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX as2: <http://www.w3.org/ns/activitystreams#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT { ?p ?pred ?o. }
WHERE {
	OPTIONAL { ?p dct:created ?d . }
	OPTIONAL { ?p as2:endTime ?et . }
	?p ?pred ?o .
}
ORDER BY DESC(?d) DESC(?et)", 0),


						"construct-all-default" =>
							array("
PREFIX blog: <http://vocab.amy.so/blog#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX as2: <http://www.w3.org/ns/activitystreams#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT { ?p ?pred ?o. }
WHERE {
	OPTIONAL { ?p dct:created ?d . }
	OPTIONAL { ?p as2:endTime ?et . }
	?p ?pred ?o .
	
	 OPTIONAL {
      ?p rdf:type ?t .
      FILTER (?t = blog:Llogpost)
  }
  FILTER (!BOUND(?t))
}
ORDER BY DESC(?d) DESC(?et)", 0),

						"construct-by-list" =>
							array("
PREFIX blog: <http://vocab.amy.so/blog#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX as2: <http://www.w3.org/ns/activitystreams#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT { ?p ?pred ?o. }
WHERE {
	OPTIONAL { ?p dct:created ?d . }
	OPTIONAL { ?p as2:endTime ?et . }
	?p sioc:topic {$query_params[0]} .
	?p ?pred ?o .
}
ORDER BY DESC(?d) DESC(?et)", 1),

						"construct-by-type" =>
							array("
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX as2: <http://www.w3.org/ns/activitystreams#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT { ?p ?pred ?o . }
WHERE {
	?p rdf:type {$query_params[0]} .
	OPTIONAL { ?p as2:endTime ?et . }
	OPTIONAL { ?p dct:created ?d . }
	?p ?pred ?o .
}
ORDER BY DESC(?d) DESC(?et)", 1),

            "construct-by-date" =>
							array("
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX as2: <http://www.w3.org/ns/activitystreams#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX blog: <http://vocab.amy.so/blog#>
CONSTRUCT {
  ?p ?pred ?o .
  ?p sioc:has_reply ?reply .
} WHERE {
	?p dct:created ?d .
	?p ?pred ?o .
	
	FILTER (?d > {$query_params[0]} && ?d < {$query_params[1]} ) .
	?p foaf:isPrimaryTopicOf ?pto .
	
	  OPTIONAL { ?pto sioc:has_reply ?reply . }
	OPTIONAL {
      ?p rdf:type ?t .
      FILTER (?t = blog:Llogpost)
  }
  FILTER (!BOUND(?t))
  FILTER(REGEX(STR(?p), \"^http://blog.rhiaro.co.uk/\"))
}
ORDER BY DESC(?d)", 2),

            "construct-replies-by-date" =>
							array("
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX as2: <http://www.w3.org/ns/activitystreams#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX blog: <http://vocab.amy.so/blog#>
CONSTRUCT {
  ?reply ?pred ?o .
  ?reply sioc:reply_of ?post .
} WHERE {
	optional { ?reply ?pred ?o . }
	optional { ?reply dct:created ?d . }
	?post sioc:has_reply ?reply .
	
	FILTER(!REGEX(STR(?reply), \"^http://blog.rhiaro.co.uk/\"))
}
ORDER BY DESC(?d)", 2),

						"construct-by-property" =>
							array("PREFIX dct: <http://purl.org/dc/terms/>
PREFIX as2: <http://www.w3.org/ns/activitystreams#>
PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT {
	?p {$query_params[0]} ?o .
	?p ?pred ?o .
	?p dct:created ?d .
	?p as2:endTime ?et .
	?p foaf:isPrimaryTopicOf ?pto .
	?p sioc:topic ?tags .
}
WHERE {
	?p {$query_params[0]} ?o .
	?p ?pred ?o .
	?p dct:created ?d .
	optional { ?p as2:endTime ?et . }
	optional { ?p foaf:isPrimaryTopicOf ?pto . }
	optional { ?p sioc:topic ?tags . }
}
ORDER BY DESC(?d) DESC(?et)", 1),

						"select-tags" =>
						array("
PREFIX sioc: <http://rdfs.org/sioc/types#>
SELECT DISTINCT ?tag COUNT(?s) AS ?c WHERE {
 ?s sioc:topic ?tag .
}
GROUP BY ?tag
ORDER BY DESC(?c)", 0),

						"construct-next-prev" =>
							array("
PREFIX blog: <http://vocab.amy.so/blog#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>

SELECT ?next ?nexttitle ?prev ?prevtitle
WHERE {
	<{$query_params[0]}> dct:created ?date .
    ?nexturi foaf:isPrimaryTopicOf ?next .
    ?nexturi dct:created ?nextdate .
    ?nexturi rdf:type ?nexttype .
    ?nexturi dct:title ?nexttitle .
    FILTER(?nextdate > ?date) .
    FILTER(?nexttype != blog:LlogPost) .

    ?prevuri foaf:isPrimaryTopicOf ?prev .
    ?prevuri dct:created ?prevdate .
    ?prevuri rdf:type ?prevtype .
    ?prevuri dct:title ?prevtitle .
    FILTER(?prevdate < ?date) .
    FILTER(?prevtype != blog:LlogPost) .

}

ORDER BY ASC(?nextdate) DESC(?prevdate)
LIMIT 1", 1),
              "get_creator" =>
                array("
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT { <{$query_params[0]}> foaf:name ?name ; foaf:depiction ?dp ; foaf:homepage ?url . }
WHERE { <{$query_params[0]}> foaf:name ?name . OPTIONAL { <{$query_params[0]}> foaf:depiction ?dp . }
OPTIONAL { <{$query_params[0]}> foaf:homepage ?url . }}
", 1),
							"uri_where_pto" =>
								array("
PREFIX foaf: <http://xmlns.com/foaf/0.1/>

SELECT ?uri WHERE {
	?uri foaf:isPrimaryTopicOf <{$query_params[0]}> .
}
LIMIT 1", 1),
							"pto_where_uri" =>
								array("
PREFIX foaf: <http://xmlns.com/foaf/0.1/>

SELECT ?pto WHERE {
	<{$query_params[0]}> foaf:isPrimaryTopicOf ?pto .
}
LIMIT 1", 1),
							"construct-where-location" =>
								array(
"PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX as2: <http://www.w3.org/ns/activitystreams#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX blog: <http://vocab.amy.so/blog#>

CONSTRUCT {
  ?p ?pr ?o .
  {$query_params[0]} ?lpr ?lo .
} WHERE {
?p dct:created ?d .
?p ?pr ?o .
OPTIONAL { {$query_params[0]} ?lpr ?lo . }
?p as2:location {$query_params[0]} .

} ORDER BY DESC(?d)", 1),
          
              "construct-travel" =>
                array("PREFIX blog: <http://vocab.amy.so/blog#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX as2: <http://www.w3.org/ns/activitystreams#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT { ?p ?pred ?o. }
WHERE {
	{
	  ?p sioc:topic \"travel\" .
    ?p ?pred ?o .
  } UNION {
  ?p as2:tag \"travel\" .
    ?p ?pred ?o .
  } UNION {
    ?p as2:location ?l .
    ?p ?pred ?o .
  } UNION {
    ?p sioc:topic \"rsvp\" .
    ?p ?pred ?o .
  } UNION {
    ?p as2:tag \"rsvp\" .
    ?p ?pred ?o .
  }
	
}", 0),
              "construct-calendar" =>
                array("PREFIX blog: <http://vocab.amy.so/blog#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX sioc: <http://rdfs.org/sioc/types#>
PREFIX as2: <http://www.w3.org/ns/activitystreams#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT { ?p ?pred ?o. }
WHERE {
	{
	  ?p as2:startTime ?st .
	  ?p as2:endTime ?et .
    ?p ?pred ?o .
  }
}
ORDER BY DESC(?st) DESC(?et)", 0),

              "construct-acquisitions" =>
                array("prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
prefix blog: <http://vocab.amy.so/blog#> .
prefix dct: <http://purl.org/dc/terms/> .

construct { ?s ?p ?o . } where {
  ?s rdf:type blog:Acquisition .
  ?s dct:created ?d .
  ?s ?p ?o .
}
order by desc(?d)", 0)
			);
		
		$q = $queries[$query_name];
		if($q[1] != count($query_params)){
			$this->_set_errors("Mismatched dynamic query.", "You didn't pass enough parameters for `$query_name`. Passed ".count($query_params).", requires ".$q[1].".");
			return false;
		}else{
			$this->_set_query($q[0]);
			return true;
		}

	}

	private function _get_posts_by_list($list){
    if(empty($list)){
      $q = "construct-all-default";
    }else{
  		foreach($list as $param => $val){
  		  if($param == "all" && $val == "true"){
  		    $q = "construct-all";
  		  }elseif($param == "list"){
  				$get = array("<".get_list_uri(addslashes($val)).">");
  				$q = "construct-by-list";
  			}elseif($param == "tag"){
  				$get = array("\"\"\"".addslashes($val)."\"\"\"");
  				$q = "construct-by-list";
  			}elseif($param == "type"){
  				$get = array("<".get_property_uri(addslashes(strtolower($val))).">");
  				$q = "construct-by-property";
  			}elseif($param == "extype"){
  				$get = array("<".get_type_uri(addslashes($val)).">");
  				$q = "construct-by-type";
  			}elseif($param == "mentions"){
  				$get = array("\"".$val["after"]."\"", "\"".$val["before"]."\"");
  				$q = "construct-replies-by-date";
  			}elseif($param == "after"){
  			  $get = array("\"".$val."\"", "\"".$list["before"]."\"");
  			  $q = "construct-by-date";
  			}elseif($param == "view"){
  			  $q = "construct-".$val;
  			}
  		}
	  }
		if(!$this->make_query($q, $get)){
			return false;
		}
		$res = $this->ep->query($this->get_query());
		if(!$this->ep->getErrors()){
			
			$this->_set_count(count($res['result']));
			$this->raw = $res['result'];
			return $res['result'];

		}else{
			$this->_set_errors("Problem getting posts list", $this->ep->getErrors());
			return false;
		}
	}

	private function _get_post($uri){
		if(!$this->make_query("construct-all-by-subject", array($uri))){
			return false;
		}
		$res = $this->ep->query($this->get_query());
		if(!$this->ep->getErrors()){
			$posts = array();

			foreach($res['result'] as $uri=>$post){
				$posts[$uri]['uri'] = $uri;
				$posts[$uri]['title'] = $post[$this->ns['dct'].'title'][0]['value'];
				$posts[$uri]['pubdate'] = $post[$this->ns['dct'].'created'][0]['value'];
				$posts[$uri]['moddate'] = $post[$this->ns['dct'].'modified'][0]['value'];
				$posts[$uri]['content'] = $post[$this->ns['sioc'].'content'][0]['value'];
				$posts[$uri]['pto'] = $post[$this->ns['foaf'].'primaryTopicOf'][0]['value'];
				$posts[$uri]['tags'] = array();
				if($post[$this->ns['sioc'].'topic']){
					foreach($post[$this->ns['sioc'].'topic'] as $tags){
						if($tags['type'] == "literal"){
							array_push($posts[$uri]['tags'], $tags['value']);
						}elseif($tags['type'] == "uri"){
							$posts[$uri]['list'] = $tags['value'];
						}
					}
				}
			}
			return $posts;
		}else{
			$this->_set_errors("Problem with post getting.", $this->ep->getErrors());
			return false;
		}

	}

	private function _get_replies_by_post($post_uri){
		// TODO: Better way of prefixes.
		// Note: out of use, not consistent behaviour with other functions.
		$q = "PREFIX blog: <http://vocab.amy.so/blog#>
			  PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			  PREFIX sioc: <http://rdfs.org/sioc/types#>";
		$q .= "CONSTRUCT { <".$post_uri."> sioc:has_reply ?comment }
				WHERE {
				  <".$post_uri."> blog:bloggerid ?id .
				  <".$post_uri."> rdf:type ?type .
				  ?bn blog:bloggerid ?id .
				  ?comment sioc:reply_of ?bn .
				}";
		$res = $this->ep->query($q);
		if(!$this->ep->getErrors()){
			$reply_uris = array();
			foreach($res['result'] as $post=>$replies){
				foreach($replies["http://rdfs.org/sioc/types#has_reply"] as $reply){
					$reply_uris[] = $reply["value"];
				}
			}
			return $reply_uris;
		}else{
			return $this->ep->getErrors();
		}
	}

	private function _get_post_and_replies($uri){

		$posts = array();

		// Paging (separate query is faster)
		if(!$this->make_query("construct-next-prev", array($uri))){
			return false;
		}
		$res = $this->ep->query($this->get_query());
		if(!$this->ep->getErrors()){
			$posts[$uri]['next'] = $res['result']['rows'][0]['next'];
			$posts[$uri]['nexttitle'] = $res['result']['rows'][0]['nexttitle'];
			$posts[$uri]['prev'] = $res['result']['rows'][0]['prev'];
			$posts[$uri]['prevtitle'] = $res['result']['rows'][0]['prevtitle'];
		}else{
			$this->_set_errors("Problem with post prev/next getting.", $this->ep->getErrors());
			return false;
		}

		// Only get list of reply URIs, get content separately (because content not necessarily coming from my store).
		if(!$this->make_query("construct-post-and-replies", array($uri))){
			return false;
		}
		$res = $this->ep->query($this->get_query());
		if(!$this->ep->getErrors()){
			return $res['result'];

		}else{
			$this->_set_errors("Problem with post and reply getting.", $this->ep->getErrors());
			return false;
		}
	}

	private function _get_tags(){
		if(!$this->make_query("select-tags")){
			return false;
		}
		$res = $this->ep->query($this->get_query());
		if(!$this->ep->getErrors()){
			$tags = array();
			foreach($res['result']['rows'] as $tag){
				if($tag['tag type'] == "literal"){
					array_push($tags, array($tag['tag'], $tag['c']));
				}
			}
			$this->_set_count(count($tags));
			return $tags;
		}else{
			$this->_set_errors("Problem with tag getting.", $this->ep->getErrors());
			return false;
		}
	}
	
	private function _get_creator($uri){
	  if(!$this->make_query("get_creator", array($uri))){
	    return false;
	  }
	  $res = $this->ep->query($this->get_query());

	  if(!$this->ep->getErrors()){
	    if(!empty($res['result'])){
  	    foreach($res['result'] as $uri => $c){
  	      $this->known_authors[$uri]['name'] = $c[$this->ns['foaf']."name"][0]['value'];
  	      $this->known_authors[$uri]['photo'] = $c[$this->ns['foaf']."depiction"][0]['value'];
  	      $this->known_authors[$uri]['url'] = $c[$this->ns['foaf']."homepage"][0]['value'];
  	    }
	    }else{
	      //$this->known_authors[$uri]['name'] = "(unnamed)";
	      //$this->known_authors[$uri]['photo'] = "img/anon.png";
	    }
	    return $this->known_authors;
	  }else{
	    $this->_set_errors("Problem getting creator.", $this->ep->getErrors());
	    return false;
	  }
	}

	private function _get_by_location($l="?l"){
		if($l == "?l"){
			$param = array($l);
		}else{ // Must be URI
			$param = array("<$l>");
		}
		if(!$this->make_query("construct-where-location", $param)){
			return false;
		}
		$res = $this->ep->query($this->get_query());
		if(!$this->ep->getErrors()){
			$this->_set_count(count($res['result']));
			return $res['result'];
		}else{
			$this->_set_errors("Problem getting posts by location $l", $this->ep->getErrors());
			return false;
		}
	}
	
	private function _get_by_subject($s){
	  if(!isset($s)){ return false; }
	  if(!is_array($s)) { $s = array($s); }
	  if(!$this->make_query("construct-all-by-subject", $s)){
	    return false;
	  }
	  $res = $this->ep->query($this->get_query());
	  if(!$this->ep->getErrors()){
	    return $res['result'];
	  }else{
	    $this->set_errors("Problem getting a reply: $s", $this->ep->getErrors());
	    return false;
	  }
	}
	
	
	##########################
	##########################

  // deprecate?
	private function _make_html_post_list($list){

		if(empty($list)){
			$output = array("template"=>"list_fail");
		}else{
			$output['template'] = "list_timegroup";
			foreach($list as $uri=>$post){

				if(isset($post[$this->ns['foaf']."isPrimaryTopicOf"][0]['value'])){
					$output[$uri]['url'] = $post[$this->ns['foaf']."isPrimaryTopicOf"][0]['value'];
				}else{
					$output[$uri]['url'] = $uri;
				}

				if(isset($post[$this->ns['dct']."created"])){
				  $output[$uri]['published'] = strtotime($post[$this->ns['dct']."created"][0]['value']);
				}else{
				  $output[$uri]['published'] = strtotime($post[$this->ns['as2']."startTime"][0]['value']);
				}
				
				if(isset($post[$this->ns['as2']."endTime"])){ $output[$uri]['end'] = strtotime($post[$this->ns['as2']."endTime"][0]['value']); }
				if(isset($post[$this->ns['as2']."startTime"])) { $output[$uri]['start'] = strtotime($post[$this->ns['as2']."startTime"][0]['value']); }
				if(isset($post[$this->ns['blog']."startLocation"])) { $output[$uri]['startLocation'] = $post[$this->ns['blog']."startLocation"][0]['value']; }
				if(isset($post[$this->ns['blog']."endLocation"])) { $output[$uri]['endLocation'] = $post[$this->ns['blog']."endLocation"][0]['value']; }

				if(isset($post[$this->ns['sioc']."topic"])){
					foreach($post[$this->ns['sioc']."topic"] as $tag){
						if($tag['type'] != "uri"){
							$output[$uri]['tags'][] = $tag['value'];
						}
					}
				}else{
					$output[$uri]['tags'] = array();
				}

				if(isset($post[$this->ns['sioc']."reply_of"])){
					if(in_array("rsvp", $output[$uri]['tags'])){
						$output[$uri]['icon'] = "calendar";
						if(isset($post[$this->ns['blog']."rsvp"][0]['value'])) $rsvp = $post[$this->ns['blog']."rsvp"][0]['value'];
						else $rsvp = "yes";
						$output[$uri]['content'] = "RSVP ".$rsvp;
					}else{
						$output[$uri]['icon'] = "reply";
					}
				}elseif(isset($post[$this->ns['as2']."location"])){
				  $output[$uri]['content'] = "Checkin";
					$output[$uri]['icon'] = "map-marker";
					$output[$uri]['location'] = $post[$this->ns['as2']."location"][0]['value'];
				}
				if(!isset($post[$this->ns['dct']."title"]) && !isset($post[$this->ns['as2']."name"])){

          if((isset($post[$this->ns['sioc']."content"][0]['value']) && $this->_extract_content($post[$this->ns['sioc']."content"][0]['value']) != "")){
					  $output[$uri]['content'] = $post[$this->ns['sioc']."content"][0]['value'];
          }

					if(isset($post[$this->ns['blog']."like_of"])){
						$output[$uri]['icon'] = "star-o";
						$output[$uri]['content'] = "<a class=\"u-like-of wee\" property=\"as2:object\" href=\"".$post[$this->ns['blog']."like_of"][0]['value']."\">".$post[$this->ns['blog']."like_of"][0]['value']."</a>";

					}elseif(isset($post[$this->ns['blog']."share_of"])){
						$output[$uri]['icon'] = "bullhorn";
						$repost = "<a class=\"u-repost-of wee\" property=\"as2:object\" href=\"".$post[$this->ns['blog']."share_of"][0]['value']."\">".$post[$this->ns['blog']."share_of"][0]['value']."</a>";
						if(!isset($output[$uri]['content'])){
							$output[$uri]['content'] = $repost;
						}else{
							$output[$uri]['content'] .= "<p>".$repost."</p>";
						}

					}elseif(isset($post[$this->ns['blog']."bookmark_of"])){
						$output[$uri]['icon'] = "bookmark-o";
						$output[$uri]['content'] = "<a class=\"u-bookmark-of wee\" property=\"as2:object\" href=\"".$post[$this->ns['blog']."bookmark_of"][0]['value']."\">".$post[$this->ns['blog']."bookmark_of"][0]['value']."</a>";

					}elseif(isset($post[$this->ns['blog']."follow_of"])){
						$output[$uri]['icon'] = "user";
						$output[$uri]['content'] = "<span class=\"h-card\"><a class=\"u-follow-of u-url wee\" property=\"as2:object\" href=\"".$post[$this->ns['blog']."follow_of"][0]['value']."\"><span class=\"p-name\">".$post[$this->ns['blog']."follow_of"][0]['value']."</span></a></span>";

					}elseif($post[$this->ns['rdf']."type"][0]['value'] == $this->ns['blog']."LlogPost"){
						
						$output[$uri]['icon'] = "line-chart";
						$output[$uri]['contentmf'] = "e-content p-name wee";
						if(in_array("food", $output[$uri]['tags'])){
							$output[$uri]['icon'] = "cutlery";
							unset($output[$uri]['start']);
							unset($output[$uri]['end']);
						}
						if(in_array("sleep", $output[$uri]['tags'])){
							$output[$uri]['icon'] = "bed";
						}
						if(in_array("exercise", $output[$uri]['tags'])){
							$output[$uri]['icon'] = "heartbeat";
						}

					}else{
						$output[$uri]['contentmf'] = "e-content p-name wee";
					}

				}else{
				  if(isset($post[$this->ns['as2']."name"])){
  				  $name = $post[$this->ns['as2']."name"][0]['value'];
				  }elseif(isset($post[$this->ns['dct']."title"])){
				    $name = $post[$this->ns['dct']."title"][0]['value'];
				  }
					 $output[$uri]['content'] = "<a href=\"".$output[$uri]['url']."\" class=\"u-url u-uid\">".$name."</a>";
					$output[$uri]['contentmf'] = "e-content p-name";
				}
				
				if(isset($post[$this->ns['blog']."startLocation"]) || isset($post[$this->ns['as2']."origin"])) {
				  if(in_array("ferry", $output[$uri]['tags']) || in_array("boat", $output[$uri]['tags']) || in_array("ship", $output[$uri]['tags'])){
				    $output[$uri]['icon'] = "ship";
				  }elseif(in_array("train", $output[$uri]['tags'])){
				    $output[$uri]['icon'] = "train";
				  }elseif(in_array("roadtrip", $output[$uri]['tags'])){
				    $output[$uri]['icon'] = "car";
				  }elseif(in_array("plane", $output[$uri]['tags'])){
				    $output[$uri]['icon'] = "plane";
				  }elseif(in_array("bus", $output[$uri]['tags']) || in_array("coach", $output[$uri]['tags'])){
				    $output[$uri]['icon'] = "bus";
				  }else{
				    $output[$uri]['icon'] = "road";
				  }
				}

				if(isset($post[$this->ns['sioc']."topic"])){
					unset($output[$uri]['tags']);
					foreach($post[$this->ns['sioc']."topic"] as $tag){
						if($tag['type'] != "uri"){
							$output[$uri]['tags'][] = "<a href=\"/tag/".urlencode($tag['value'])."\" class=\"p-category\">".$tag['value']."</a>";
						}
					}
				}

			}
		}
		$this->_set_output($output);
	}
	
	private function _make_html_expanded_list($list, $return=false){

		if(empty($list)){
			$output = array("template"=>"list_fail");
		}else{
			$output['template'] = "list_timegroup";
			foreach($list as $uri=>$post){

				if(isset($post[$this->ns['foaf']."isPrimaryTopicOf"][0]['value'])){
					$output[$uri]['url'] = $post[$this->ns['foaf']."isPrimaryTopicOf"][0]['value'];
				}else{
					$output[$uri]['url'] = $uri;
				}

				$d = new DateTime($post[$this->ns['dct']."created"][0]['value']);
				$output[$uri]['published'] = $d;
				
				if(isset($post[$this->ns['dct']."title"])){
				  $this->_set_title($post[$this->ns['dct']."title"][0]['value']);
				  $output[$uri]['name'] = $post[$this->ns['dct']."title"][0]['value'];
				}elseif(isset($post[$this->ns['as2']."name"])){
				  $this->_set_title($post[$this->ns['as2']."name"][0]['value']);
				  $output[$uri]['name'] = $post[$this->ns['as2']."name"][0]['value'];
				}else{
				  $this->_set_title("post");
				}
	
  			unset($authoruri);
				if(isset($post[$this->ns['dc']."creator"])) {
  				
				  $authoruri = $post[$this->ns['dc']."creator"][0]['value'];
				  if(!array_key_exists($authoruri, $this->known_authors)){
  				  $this->_get_creator($authoruri);
  				}
				} // else no creator stored for post
				
				$output[$uri]['author'] = $this->known_authors[$authoruri];

        $output[$uri]['tags'] = array();
				if(isset($post[$this->ns['sioc']."topic"])){
					foreach($post[$this->ns['sioc']."topic"] as $tag){
						if($tag['type'] != "uri"){
							$output[$uri]['tags'][] = $tag['value'];
						}
					}
				}
				if(isset($post[$this->ns['as2']."tag"])){
					foreach($post[$this->ns['as2']."tag"] as $tag){
						if($tag['type'] != "uri"){
							$output[$uri]['tags'][] = $tag['value'];
						}
					}
				}
				
				if(isset($post[$this->ns['as2']."endTime"])) { $output[$uri]['end'] = new DateTime($post[$this->ns['as2']."endTime"][0]['value']); }
				if(isset($post[$this->ns['as2']."startTime"])) { $output[$uri]['start'] = new DateTime($post[$this->ns['as2']."startTime"][0]['value']); }
				if(isset($post[$this->ns['as2']."startTime"])){
				  $output[$uri]['date'] = strtotime($post[$this->ns['as2']."startTime"][0]['value']);
				}else{
				  $output[$uri]['date'] = strtotime($post[$this->ns['dct']."created"][0]['value']);
				}
				if(isset($post[$this->ns['blog']."startLocation"]) || isset($post[$this->ns['as2']."origin"])) {
				  if(in_array("ferry", $output[$uri]['tags']) || in_array("boat", $output[$uri]['tags']) || in_array("ship", $output[$uri]['tags'])){
				    $output[$uri]['icon'] = "ship";
				  }elseif(in_array("train", $output[$uri]['tags'])){
				    $output[$uri]['icon'] = "train";
				  }elseif(in_array("roadtrip", $output[$uri]['tags'])){
				    $output[$uri]['icon'] = "car";
				  }elseif(in_array("plane", $output[$uri]['tags'])){
				    $output[$uri]['icon'] = "plane";
				  }elseif(in_array("bus", $output[$uri]['tags']) || in_array("coach", $output[$uri]['tags'])){
				    $output[$uri]['icon'] = "bus";
				  }else{
				    $output[$uri]['icon'] = "road";
				  }
				}
				if(isset($post[$this->ns['blog']."startLocation"])) { $output[$uri]['startLocation'] = $post[$this->ns['blog']."startLocation"][0]['value']; }
				if(isset($post[$this->ns['blog']."endLocation"])) { $output[$uri]['endLocation'] = $post[$this->ns['blog']."endLocation"][0]['value']; }
				if(isset($post[$this->ns['as2']."origin"])) { $output[$uri]['startLocation'] = $post[$this->ns['as2']."origin"][0]['value']; }
				if(isset($post[$this->ns['as2']."target"])) { $output[$uri]['endLocation'] = $post[$this->ns['as2']."target"][0]['value']; }

				if(isset($post[$this->ns['sioc']."reply_of"]) || isset($post[$this->ns['as2']."inReplyTo"])){
          if(isset($post[$this->ns['sioc']."reply_of"])){
				    $output[$uri]['reply-to'] = $post[$this->ns['sioc']."reply_of"][0]['value'];
          }elseif(isset($post[$this->ns['as2']."inReplyTo"])){
            $output[$uri]['reply-to'] = $post[$this->ns['as2']."inReplyTo"][0]['value'];
          }
					if(in_array("rsvp", $output[$uri]['tags'])){
						$output[$uri]['icon'] = "calendar";
						if(isset($post[$this->ns['blog']."rsvp"][0]['value'])) $rsvp = $post[$this->ns['blog']."rsvp"][0]['value'];
						else $rsvp = "yes";
            if(isset($post[$this->ns['sioc']."content"])){
              $content = $post[$this->ns['sioc']."content"][0]['value'];
            }elseif(isset($post[$this->ns['as2']."content"])){
              $content = $post[$this->ns['as2']."content"][0]['value'];
            }else{
              $content = "RSVP ".$rsvp;
            }
						$output[$uri]['content'] = "<data class=\"p-rsvp\" value=\"".$rsvp."\">".$content."</data>";
						//$output[$uri]['contentmf'][] = "p-rsvp";
					}else{
						$output[$uri]['icon'] = "reply";
					}
				}elseif(isset($post[$this->ns['as2']."location"])){
					$output[$uri]['icon'] = "map-marker";
					$output[$uri]['location'] = $post[$this->ns['as2']."location"][0]['value'];
				}
				
				if(isset($post[$this->ns['blog']."state"])) { $output[$uri]['state'] = $post[$this->ns['blog']."state"][0]["value"]; }
				if(isset($post[$this->ns['dct']."isReplacedBy"])) { $output[$uri]['isReplacedBy'] = $post[$this->ns['dct']."isReplacedBy"][0]["value"]; }

        if((isset($post[$this->ns['sioc']."content"][0]['value']) && $this->_extract_content($post[$this->ns['sioc']."content"][0]['value']) != "")){
				  $output[$uri]['content'] = $this->_markdown_to_html($this->_extract_content($post[$this->ns['sioc']."content"][0]['value']));
        }

				if(isset($post[$this->ns['blog']."like_of"])){
					$output[$uri]['icon'] = "star-o";
					if(stripos($uri, "rhiaro.co.uk") > 0){
  					$output[$uri]['content'] = "<blockquote><a class=\"u-like-of\" property=\"as2:object\" href=\"".$post[$this->ns['blog']."like_of"][0]['value']."\">".$post[$this->ns['blog']."like_of"][0]['value']."</a></blockquote>";
  				}else{
  				  $output[$uri]['content'] = "likes <a class=\"u-like-of\" href=\"".$post[$this->ns['blog']."like_of"][0]['value']."\">this</a>.";
  				}

				}elseif(isset($post[$this->ns['blog']."share_of"])){
					$output[$uri]['icon'] = "bullhorn";
					if(stripos($uri, "rhiaro.co.uk") > 0){

					  $repost = "<a class=\"u-repost-of\" property=\"as2:object\" href=\"".$post[$this->ns['blog']."share_of"][0]['value']."\">".$post[$this->ns['blog']."share_of"][0]['value']."</a>";
						if(!isset($output[$uri]['content'])){
							$output[$uri]['content'] = "<blockquote>".$repost."</blockquote>";
						}else{
							$output[$uri]['content'] .= "<p class=\"wee\">(".$repost.")</p>";
						}
					}else{
					  $output[$uri]['content'] = "reposted <a class=\"u-repost-of\" href=\"".$post[$this->ns['blog']."share_of"][0]['value']."\">this</a>.";
					}

				}elseif(isset($post[$this->ns['blog']."bookmark_of"])){
					$output[$uri]['icon'] = "bookmark-o";
					if(stripos($uri, "rhiaro.co.uk") > 0){
					  $output[$uri]['content'] = "<blockquote><a class=\"u-bookmark-of\" property=\"as2:object\" href=\"".$post[$this->ns['blog']."bookmark_of"][0]['value']."\">".$post[$this->ns['blog']."bookmark_of"][0]['value']."</a></blockquote>";
					}else{
					  $output[$uri]['content'] = "bookmarked <a class=\"u-bookmark-of\" href=\"".$post[$this->ns['blog']."bookmark_of"][0]['value']."\">this</a>.";
					}

				}elseif(isset($post[$this->ns['blog']."follow_of"])){
					$output[$uri]['icon'] = "user";
					$output[$uri]['content'] = "<p class=\"p-name\">Subscribed to: <span class=\"h-card p-url\"><a class=\"u-follow-of u-url\" property=\"as2:object\" href=\"".$post[$this->ns['blog']."follow_of"][0]['value']."\">".$post[$this->ns['blog']."follow_of"][0]['value']."</a></span></p>";

				}elseif($post[$this->ns['rdf']."type"][0]['value'] == $this->ns['blog']."LlogPost"){
					
					$output[$uri]['icon'] = "line-chart";
					if(in_array("food", $output[$uri]['tags'])){
						$output[$uri]['icon'] = "cutlery";
						unset($output[$uri]['end']);
						unset($output[$uri]['start']);
					}
					if(in_array("sleep", $output[$uri]['tags'])){
						$output[$uri]['icon'] = "bed";
					}
					if(in_array("exercise", $output[$uri]['tags'])){
						$output[$uri]['icon'] = "heartbeat";
					}
				}
				
				if(isset($post[$this->ns['blog']."read-of"])){
					$output[$uri]['icon'] = "book";
					$output[$uri]['content'] = "<p class=\"p-name\">Read: <span class=\"h-card p-url\"><a class=\"u-read-of u-url\" property=\"as2:object\" href=\"".$post[$this->ns['blog']."read-of"][0]['value']."\">".$post[$this->ns['blog']."read-of"][0]['value']."</a></span></p>";
				}
				
				if(isset($post[$this->ns['blog']."mentions"])){
					$output[$uri]['icon'] = "link";
					if(!isset($output[$uri]['content'])){
  					$output[$uri]['content'] = "linked to <a class=\"u-mentions\" href=\"".$post[$this->ns['blog']."mentions"][0]['value']."\" property=\"as2:object\">this</a> from <a class=\"u-url\" href=\"".$uri."\">".$uri."</a> .";
					}
				}
				
				if(isset($post[$this->ns['as2']."summary"])){
					$output[$uri]['summary'] = $post[$this->ns['as2']."summary"][0]['value'];
				}
				
				if(isset($post[$this->ns['as2']."image"])){
				  foreach($post[$this->ns['as2']."image"] as $img){
					  $output[$uri]['image'][] = $post[$this->ns['as2']."image"][0]['value'];
				  }
				}
				
				if(isset($post[$this->ns['blog']."cost"])){
					$output[$uri]['cost'] = $post[$this->ns['blog']."cost"][0]['value'];
				}
				
				if(isset($post[$this->ns['rdf']."type"])){
				  foreach($post[$this->ns['rdf']."type"] as $type){
				    if($type['value'] == $this->ns['blog']."Acquisition"){
				      $output[$uri]['icon'] = "shopping-basket";
				    }
				  }
				}
				
				unset($replylist);
				if(isset($post[$this->ns['sioc']."has_reply"])){
					foreach($post[$this->ns['sioc']."has_reply"] as $r){
					  $reply = $this->_get_by_subject($r['value']);
					  $replylist[$r['value']] = $reply[$r['value']];
					}
					$output[$uri]['replies'] = $this->_make_html_expanded_list($replylist, true);
					unset($output[$uri]['replies']['template']);
				}
				
				// Remove vague location posts (ie. has location but no content).
				if(isset($output[$uri]['location']) && !isset($output[$uri]['content'])){
				  unset($output[$uri]);
				}

			}
		}
		if($return){
		   return $output;
		}else{
		  $this->_set_output($output);
		}
	}


	private function _make_html_activity_list($list){
	  if(empty($list)){
			$output = array("template"=>"list_fail");
		}else{
			$output['template'] = "list_activities";
			foreach($list as $uri=>$post){
			  $tags = array();
			  if(isset($post[$this->ns['sioc']."topic"])){
					foreach($post[$this->ns['sioc']."topic"] as $tag){
						if($tag['type'] != "uri"){
							$tags[] = $tag['value'];
						}
					}
				}
				
				$output[$uri]['date'] = strtotime($post[$this->ns['dct']."created"][0]['value']);
			  if(isset($post[$this->ns['foaf']."isPrimaryTopicOf"][0]['value'])){
					$output[$uri]['url'] = $post[$this->ns['foaf']."isPrimaryTopicOf"][0]['value'];
				}else{
					$output[$uri]['url'] = $uri;
				}
				if(isset($post[$this->ns['dc']."creator"][0]['value'])){
				  $output[$uri]['a_url'] = $post[$this->ns['dc']."creator"][0]['value'];
				}else{
				  $output[$uri]['a_url'] = "http://rhiaro.co.uk/about#me";
				}
				if(!array_key_exists($output[$uri]['a_url'], $this->known_authors)){
				  $this->_get_creator($output[$uri]['a_url']);
				}
				$output[$uri]['author'] = $this->known_authors[$output[$uri]['a_url']]['name'];
			
			  $output[$uri]['content'] = " ";
			  if(isset($post[$this->ns['dct']."title"][0]['value'])){
			    $output[$uri]['content'] .= "wrote '<a href=\"".$output[$uri]['url']."\" class=\"u-url\">".$post[$this->ns['dct']."title"][0]['value']."</a>'";
			  }elseif(isset($post[$this->ns['as2']."name"][0]['value'])){
			    $output[$uri]['content'] .= "wrote '<a href=\"".$output[$uri]['url']."\" class=\"u-url\">".$post[$this->ns['as2']."name"][0]['value']."</a>'";
			  }elseif($post[$this->ns['rdf']."type"][0]['value'] == $this->ns['blog']."LlogPost"){
			    if(in_array("food", $tags)){
			      $output[$uri]['content'] .= "<a href=\"".$output[$uri]['url']."\" class=\"u-url\">consumed</a> ".$post[$this->ns['sioc']."content"][0]['value'];
			    }elseif(in_array("sleep", $tags)){
			      $mins = $this->hours_and_minutes($this->time_difference(strtotime($post[$this->ns['as2']."startTime"][0]['value']), strtotime($post[$this->ns['as2']."endTime"][0]['value'])));
			      $output[$uri]['content'] .= "<a href=\"".$output[$uri]['url']."\" class=\"u-url\">slept</a> for ".$mins.".";
			      //from ".$post[$this->ns['as2']."startTime"][0]['value']." to ".$post[$this->ns['as2']."endTime"][0]['value'].".";
			    }
			  }elseif(isset($post[$this->ns['as2']."location"]) && !isset($post[$this->ns['sioc']."content"])){
			    $output[$uri]['content'] .= $this->_checkin_activity_sentence($post[$this->ns['as2']."location"][0]['value'])."";
			    
			  }elseif(isset($post[$this->ns['blog']."like_of"])){
			    $output[$uri]['content'] .= "liked <a class=\"u-like-of\" href=\"".$post[$this->ns['blog']."like_of"][0]['value']."\">".$post[$this->ns['blog']."like_of"][0]['value']."</a>";
			    
			  }elseif(isset($post[$this->ns['blog']."bookmark_of"])){
			    $output[$uri]['content'] .= "bookmarked <a class=\"u-bookmark-of\" href=\"".$post[$this->ns['blog']."bookmark_of"][0]['value']."\">".$post[$this->ns['blog']."bookmark_of"][0]['value']."</a>";
			    
			  }elseif(isset($post[$this->ns['blog']."share_of"])){
			    $output[$uri]['content'] .= "reposted <a class=\"u-repost-of\" href=\"".$post[$this->ns['blog']."share_of"][0]['value']."\">".$post[$this->ns['blog']."share_of"][0]['value']."</a>";
			    
			  }else{
			    $output[$uri]['content'] .= "wrote <a href=\"".$output[$uri]['url']."\" class=\"u-url\">a note</a>";
			  }
			  if(isset($post[$this->ns['sioc']."reply_of"])){
			    if(in_array("rsvp", $tags)){
			      $output[$uri]['content'] .= " as an RSVP to <a class=\"u-reply-of\" href=\"".$post[$this->ns['sioc']."reply_of"][0]['value']."\">".$post[$this->ns['sioc']."reply_of"][0]['value']."</a>";
			    }else{
			      $output[$uri]['content'] .= " in reply to <a class=\"u-reply-of\" href=\"".$post[$this->ns['sioc']."reply_of"][0]['value']."\">".$post[$this->ns['sioc']."reply_of"][0]['value']."</a>";
			    }
			  }
			  
			}
		}
		$this->_set_output($output);
	}

  // Deprecate....?
	/*private function _make_html_post($postAr){
		if(empty($postAr)){
			$output = array("template"=>"list_fail");
		}else{
			$output['template'] = "list_timegroup";
			foreach($postAr as $uri => $post){

        $output[$uri]['url'] = $post[$this->ns['foaf']."isPrimaryTopicOf"][0]['value'];
				$output[$uri]['date'] = strtotime($post[$this->ns['dct']."created"][0]['value']);
				if(isset($post[$this->ns['dct']."modified"])){ $output[$uri]['moddate'] = strtotime($post[$this->ns['dct']."modified"][0]['value']); }
				if(isset($post[$this->ns['as2']."start"])){ $output[$uri]['start'] = strtotime($post[$this->ns['as2']."startTime"][0]['value']); }
				if(isset($post[$this->ns['as2']."end"])){ $output[$uri]['end'] = strtotime($post[$this->ns['as2']."endTime"][0]['value']); }
				
				if(isset($post[$this->ns['dc']."creator"]) && $post[$this->ns['dc']."creator"][0]['value'] != "http://rhiaro.co.uk/about#me" && $post[$this->ns['dc']."creator"][0]['value'] != "http://www.blogger.com/profile/12227954801080178130"){
					// TODO: handle it not being me (fetch infoz)
					$output[$uri]['creator'] = $post[$this->ns['dc']."creator"][0]['value'];
					$output[$uri]['name'] = $post[$this->ns['dc']."creator"][0]['value'];
					$output[$uri]['dp'] = "http://rhiaro.co.uk/stash/tigooutline.jpg";
				}else{
					$output[$uri]['creator'] = "http://rhiaro.co.uk/about#me";
					$output[$uri]['name'] = "Amy";
					$output[$uri]['dp'] = "http://rhiaro.co.uk/stash/dp.png";
				}

				if(isset($post[$this->ns['sioc']."topic"])){
					foreach($post[$this->ns['sioc']."topic"] as $tag){
						$output[$uri]['tags'][] = $tag['value'];
					}
				}else{
					$output[$uri]['tags'] = array();
				}
				
				if(isset($post[$this->ns['as2']."to"])){
				  $output[$uri]['to'] = $post[$this->ns['as2']."to"][0]['value'];
				}
				if(isset($post[$this->ns['as2']."cc"])){
				  $output[$uri]['cc'] = $post[$this->ns['as2']."cc"][0]['value'];
				}

				if(isset($post[$this->ns['sioc']."reply_of"])){
					foreach($post[$this->ns['sioc']."reply_of"] as $reply){
						if($reply['type'] != "bnode"){
							$mf2 = "u-in-reply-to";
							if(in_array("rsvp", $output[$uri]['tags'])){
								$icon = "calendar";
								$slug = "RSVP";
							}else{
								$icon = "reply";
								$slug = "in reply to";
							}

							$output[$uri]['replyto'][$reply['value']]['icon'] = $icon;
							$output[$uri]['replyto'][$reply['value']]['slug'] = $slug;
							$output[$uri]['replyto'][$reply['value']]['mf2'] = $mf2;
						}
					}
				}
				if(isset($post[$this->ns['blog']."like_of"])){
					foreach($post[$this->ns['blog']."like_of"] as $reply){

						$output[$uri]['replyto'][$reply['value']]['icon'] = "star-o";
						$output[$uri]['replyto'][$reply['value']]['slug'] = "like of";
						$output[$uri]['replyto'][$reply['value']]['mf2'] = "u-like-of";
					}
				}
				if(isset($post[$this->ns['blog']."bookmark_of"])){
					foreach($post[$this->ns['blog']."bookmark_of"] as $reply){

						$output[$uri]['replyto'][$reply['value']]['icon'] = "bookmark-o";
						$output[$uri]['replyto'][$reply['value']]['slug'] = "bookmark of";
						$output[$uri]['replyto'][$reply['value']]['mf2'] = "u-bookmark-of";
					}
				}
				if(isset($post[$this->ns['blog']."share_of"])){
					foreach($post[$this->ns['blog']."share_of"] as $reply){

						$output[$uri]['replyto'][$reply['value']]['icon'] = "bullhorn";
						$output[$uri]['replyto'][$reply['value']]['slug'] = "repost of";
						$output[$uri]['replyto'][$reply['value']]['mf2'] = "u-repost-of";
						$output[$uri]['replyto'][$reply['value']]['content'] = $this->_get_external($reply['value']);
					}
				}
				if(isset($post[$this->ns['blog']."follow_of"])){
					foreach($post[$this->ns['blog']."follow_of"] as $reply){

						$output[$uri]['replyto'][$reply['value']]['icon'] = "user";
						$output[$uri]['replyto'][$reply['value']]['slug'] = "now following";
						$output[$uri]['replyto'][$reply['value']]['mf2'] = "u-x-follow-of u-url p-name";
						$output[$uri]['replyto'][$reply['value']]['totype'] = " h-card";
					}
				}

				if(in_array($this->ns['blog']."Todo", $output[$uri]['tags'])){
					$output[$uri]['listicon'] = "tasks";
					$output[$uri]['listslug'] = "Todo";
				}elseif(in_array($this->ns['blog']."Doing", $output[$uri]['tags'])){
					$output[$uri]['listicon'] = "spinner";
					$output[$uri]['listslug'] = "Doing";
				}elseif(in_array($this->ns['blog']."Done", $output[$uri]['tags'])){
					$output[$uri]['listicon'] = "check-square-o";
					$output[$uri]['listslug'] = "Done";
				}

				$output[$uri]['prev'] = "#todo";
				$output[$uri]['next'] = "#todo";
				
				$rawcontent = $this->_extract_content($post[$this->ns['sioc']."content"][0]['value']);

				if(isset($post[$this->ns['dct']."title"])){
					$output[$uri]['title'] = $post[$this->ns['dct']."title"][0]['value'];
					$output[$uri]['rawcontent'] = $output[$uri]['title'];
					$this->_set_title($output[$uri]['title']);
				}else{
					$this->_set_title("note");
					$output[$uri]['rawcontent'] = strip_tags(substr($rawcontent,0,248));
				}

				$output[$uri]['content'] = $this->_markdown_to_html($rawcontent);

				if(isset($post[$this->ns['sioc']."has_reply"])){
					foreach($post[$this->ns['sioc']."has_reply"] as $r){
						$output[$uri]['replies'][] = $r['value'];
					}
				}
				
			}
		}
		$this->_set_output($output);
	}
*/

	private function _make_html_tags($tags){
		$html = "<ul class=\"unlist bloblist\">";
		foreach($tags as $tag){
			$html .= "<li><a href=\"/tag/".urlencode($tag[0])."\">{$tag[0]}</a> ({$tag[1]})</li>";
		}
		$html .="</ul>";
		$this->_set_output($html);
	}

	private function _make_html_checkin($checkins){
	  // HERENOW
		if(empty($checkins)){
			$output = array("template"=>"checkin_fail");
		}else{

			$prev = time();
			$output = array("template"=>"checkin_where");
			$output['posts'] = array();
			$output['locations'] = array();
			$output['cal'] = array();
			$output['now'] = array();

			foreach($checkins as $uri=>$post){
			  if(isset($post[$this->ns['as2']."location"])){
  				
  				if(!isset($post[$this->ns['as2']."startTime"])){ // Ie if it's not an event or journey. Need to filter out not-vague checkins.
    				$cur = strtotime($post[$this->ns['dct']."created"][0]['value']);
    				$output['posts'][$uri]['location'] = $post[$this->ns['as2']."location"][0]['value'];
    				$output['posts'][$uri]['published'] = new DateTime($post[$this->ns['dct']."created"][0]['value']);
    				$output['posts'][$uri]['duration'] = $this->hours_and_minutes($this->time_difference($cur, $prev));
  				  $output['posts'][$uri]['d'] = $this->time_difference($cur, $prev);
  				  if(isset($post[$this->ns['sioc']."content"])){
  				    $output['posts'][$uri]['content'] = $post[$this->ns['sioc']."content"][0]['value'];
  				  }
  				  $prev = $cur;
				  }else{
				    $output['cal'][$uri]['location'] = $post[$this->ns['as2']."location"][0]['value'];
				    $output['cal'][$uri]['published'] = $post[$this->ns['dct']."created"][0]['value'];
				    $output['cal'][$uri]['start'] = $post[$this->ns['as2']."startTime"][0]['value'];
				    $output['cal'][$uri]['end'] = $post[$this->ns['as2']."endTime"][0]['value'];
				    if(isset($post[$this->ns['sioc']."content"])){
  				    $output['cal'][$uri]['content'] = $post[$this->ns['sioc']."content"][0]['value'];
  				  }
				    $output['cal'][$uri]['duration'] = $this->hours_and_minutes($this->time_difference($end, $start));
				    $output['cal'][$uri]['d'] = $this->time_difference($end, $start);
				    
				    //$now = time();
				    //$now = strtotime("2015-08-20T12:00+01:00");
				    if(strtotime($output['cal'][$uri]['start']) < $now && strtotime($output['cal'][$uri]['end']) > $now){
				      $output['now'][$uri] = $output['cal'][$uri];
				    }
				  }
				
			  }else{
			    $output['locations'][$uri]['slug'] = $post[$this->ns['rdfs']."label"][0]['value'];
			    $output['locations'][$uri]['past'] = $post[$this->ns['blog']."pastLabel"][0]['value'];
			    $output['locations'][$uri]['present'] = $post[$this->ns['blog']."presentLabel"][0]['value'];
			  }
			}

			$this->_set_output($output);
		}
	}
	
	
	private function _make_json_activity_list($list){
	  if(empty($list)){
			$output = array("template"=>"list_fail");
		}else{
			$output['template'] = "list_activities";
			foreach($list as $uri=>$post){
			  $tags = array();
			  if(isset($post[$this->ns['sioc']."topic"])){
					foreach($post[$this->ns['sioc']."topic"] as $tag){
						if($tag['type'] != "uri"){
							$tags[] = $tag['value'];
						}
					}
				}
				
				$output[$uri]['date'] = strtotime($post[$this->ns['dct']."created"][0]['value']);
			  if(isset($post[$this->ns['foaf']."isPrimaryTopicOf"][0]['value'])){
					$output[$uri]['url'] = $post[$this->ns['foaf']."isPrimaryTopicOf"][0]['value'];
				}else{
					$output[$uri]['url'] = $uri;
				}
				if(isset($post[$this->ns['dc']."creator"][0]['value'])){
				  $output[$uri]['a_url'] = $post[$this->ns['dc']."creator"][0]['value'];
				}else{
				  $output[$uri]['a_url'] = "http://rhiaro.co.uk/about#me";
				}
				if(!array_key_exists($output[$uri]['a_url'], $this->known_authors)){
				  $this->_get_creator($output[$uri]['a_url']);
				}
				$output[$uri]['author'] = $this->known_authors[$output[$uri]['a_url']]['name'];
			
			  if(isset($post[$this->ns['dct']."title"][0]['value']) || isset($post[$this->ns['as2']."name"][0]['value'])){
			    $output[$uri]['type'] = "Article";
				  $output[$uri]['object'] = $uri;
			  }elseif($post[$this->ns['rdf']."type"][0]['value'] == $this->ns['blog']."LlogPost"){
			    if(in_array("food", $tags)){
			      $output[$uri]['type'] = "me:Consume";
				    $output[$uri]['object'] = $uri;
			    }elseif(in_array("sleep", $tags)){
			      $output[$uri]['type'] = "me:Sleep";
				    $output[$uri]['object'] = $uri;
			    }
			  }elseif(isset($post[$this->ns['as2']."location"])){
		      $output[$uri]['type'] = "Arrive";
			    $output[$uri]['object'] = $post[$this->ns['as2']."location"][0]['value'];
			    
			  }elseif(isset($post[$this->ns['blog']."like_of"])){
			    $output[$uri]['type'] = "Like";
				  $output[$uri]['object'] = $post[$this->ns['blog']."like_of"][0]['value'];
			    
			  }elseif(isset($post[$this->ns['blog']."bookmark_of"])){
			    $output[$uri]['type'] = "Save";
				  $output[$uri]['object'] = $post[$this->ns['blog']."bookmark_of"][0]['value'];
			    
			  }elseif(isset($post[$this->ns['blog']."share_of"])){
			    $output[$uri]['type'] = "Share";
				  $output[$uri]['object'] = $post[$this->ns['blog']."share_of"][0]['value'];
			    
			  }else{
			    $output[$uri]['type'] = "Note";
				  $output[$uri]['object'] = $uri;
			  }
			  if(isset($post[$this->ns['sioc']."reply_of"])){
			    if(in_array("rsvp", $tags)){
			      $output[$uri]['type'] = "Accept";
				    $output[$uri]['object'] = $post[$this->ns['sioc']."reply_of"][0]['value'];
			    }else{
			      $output[$uri]['type'] = "Respond";
				    $output[$uri]['object'] = $post[$this->ns['sioc']."reply_of"][0]['value'];
			    }
			  }
			  
			}
		}
		$this->_set_output($output);
	}
	
	/* deprecate (needs removing from activities) */
	private function _checkin_activity_sentence($checkin){
	  $checkin = str_replace("http://rhiaro.co.uk/","",$checkin);
	  $sentences = array(
	    "office" => "arrived at her office"
			,"home" => "got home"
			,"volunteer" => "went to help out with something"
			,"event" => "arrived at an event"
			,"restaurant" => "went out for food"
			,"exercise" => "went out exercising"
			,"meeting" => "went to a meeting"
			,"seminar" => "went to a talk or seminar"
			,"transit" => "was in transit"
			,"other" => "went on an adventure (probably)"
    );
	  if(array_key_exists($checkin, $sentences)) return $sentences[$checkin];
	  else return "checked in at ".str_replace("http://dbpedia.org/resource/","",str_replace("http://rhiaro.co.uk/","",$checkin));
	}

	private function _markdown_to_html($md){
		//$html = MarkdownExtra::defaultTransform($md);
		//$html = $this->_do_strikethrough($html);
		$Parsedown = new Parsedown();
    $html = $Parsedown->text($md);
		return $html;
	}

	function time_difference($cur, $prev){
		return ($prev-$cur)/60;
	}

	function hours_and_minutes($minutes){
		$m = $minutes % 60;
		if($m > 0 && $m < 1) $mt = "less than 1 minute";
		if($m > 1) $ms = "s"; else $ms = "";
		if($m > 0) $mt = "$m minute$ms"; else $mt = "";
		$h = floor($minutes / 60);
		if($h > 1) $hs = "s"; else $hs = "";
		if($h > 0) $ht = "$h hour$hs"; else $ht = "";
		return trim("$ht $mt");
	}

	private function _get_icon($post, $size=null){

		$icon = "";
		
		if($post['type'] == $this->ns['sioc']."MicroblogPost"){ $icon = "pencil-square-o"; }
		if($post['type'] == $this->ns['blog']."LlogPost"){
			$icon = "line-chart";
			if(in_array("run", $post['tags']) || in_array("yoga", $post['tags']) || in_array("swim", $post['tags'])){ $icon = "heartbeat"; }
			if(in_array("sleep", $post['tags'])){ $icon = "bed"; }
			if(in_array("food", $post['tags'])){ $icon = "cutlery"; }
		}
		if(!empty($post['reply-to'])) { $icon = "reply"; }
		//if(!empty($post['like-of'])) { $icon = "star-o"; }
		if($post['type'] == $this->ns['as2']."Like"){ $icon = "star-o"; }
		//if(!empty($post['share-of'])) { $icon = "bullhorn"; }
		if($post['type'] == $this->ns['as2']."Share"){ $icon = "bullhorn"; }
		//if(!empty($post['bookmark-of'])) { $icon = "bookmark-o"; }
		if($post['type'] == $this->ns['as2']."Save"){ $icon = "bookmark-o"; }
		if(in_array("rsvp", $post['tags'])){ $icon = "calendar"; }
		if(in_array("checkin", $post['tags'])){ $icon = "map-marker"; }

		return $icon;
	}

	##########################
	# General useful things
	##########################

	private function _make_uri_from_id($id){
		if(strlen($id) > 7){
			return uri_from_slug($id, 0, true);
		}else{
			return uri_from_shortlink($this->ep, $id);
		}
	}

	private function _get_post_from_page($page){
		if(!$this->make_query("uri_where_pto", array($page))){
			return false;
		}
		$res = $this->ep->query($this->get_query());
		if(!$this->ep->getErrors()){
			$uri = $res['result']['rows'][0]['uri'];
		}else{
			$this->_set_errors("Can't get URI from isPrimaryTopicOf", $this->ep->getErrors());
			return false;
		}
		return $uri;
	}
	
	private function _extract_content($post_md){
	  $start = strpos($post_md, "-->");
	  if(!$start){ $start = 0; }
	  else { $start = $start + 3; }
		return trim(substr($post_md, $start));
	}

	// Fix PHP Markdown some day so that this isn't omitted. Meanwhile, quick and dirty fix.
	private function _do_strikethrough($text)
	{
		return preg_replace('/~~(.*?)~~/', '<del>\\1</del>', $text);
	}

	##########################
	# Setting class vars
	##########################

	private function _set_output($output){
		$this->output = $output;
	}

	private function _set_title($title){
		$this->title = $title;
	}

	private function _set_count($count){
		$this->count_triples = $count;
	}

	private function _set_errors($key, $error){
		$this->errors[$key] = $error;
	}


	private function _set_query($q){
		$this->query = $q;
	}

}

?>