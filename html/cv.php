<?
session_start();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Amy Guy's CV</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="pingback" href="https://webmention.io/rhiaro.co.uk/xmlrpc" />
        <link rel="webmention" href="https://webmention.io/rhiaro.co.uk/webmention" />
        <link rel="authorization_endpoint" href="https://indieauth.com/auth" />
        <link rel="token_endpoint" href="https://tokens.indieauth.com/token" />
        <link rel="micropub" href="http://rhiaro.co.uk/micropub.php" />
        <link rel="stylesheet" href="/css/normalize.min.css"/>
        <link rel="stylesheet alternate" href="/css/main.css" title="rhiaro"/>
        <link rel="stylesheet" href="/css/font-awesome.min.css"/>
        <link rel="stylesheet" media="all" title="LNCS" href="http://linked-research.270a.info/media/css/lncs.css"/>
        <link rel="stylesheet alternate" media="all" title="ACM" href="http://linked-research.270a.info/media/css/acm.css"/>
        <link rel="stylesheet alternate" href="http://www.w3.org/StyleSheets/TR/W3C-REC.css" media="all" title="W3C-REC"/>
        <link rel="stylesheet" media="all" href="http://linked-research.270a.info/media/css/lr.css"/>
        <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="http://linked-research.270a.info/scripts/html.sortable.min.js"></script>
        <script src="http://linked-research.270a.info/scripts/lr.js"></script>

        <!--[if lt IE 9]>
            <script src="js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
        <![endif]-->
    </head>
    <body class="h-resume" about="[this:]" typeof="foaf:Document sioc:Post biblio:Paper prov:Entity" prefix="rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns# rdfs: http://www.w3.org/2000/01/rdf-schema# owl: http://www.w3.org/2002/07/owl# xsd: http://www.w3.org/2001/XMLSchema# dcterms: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ v: http://www.w3.org/2006/vcard/ns# pimspace: http://www.w3.org/ns/pim/space# cc: http://creativecommons.org/ns# skos: http://www.w3.org/2004/02/skos/core# prov: http://www.w3.org/ns/prov# schema: http://schema.org/ rsa: http://www.w3.org/ns/auth/rsa# cert: http://www.w3.org/ns/auth/cert# cal: http://www.w3.org/2002/12/cal/ical# wgs: http://www.w3.org/2003/01/geo/wgs84_pos# org: http://www.w3.org/ns/org# biblio: http://purl.org/net/biblio# bibo: http://purl.org/ontology/bibo/ book: http://purl.org/NET/book/vocab# ov: http://open.vocab.org/terms/ doap: http://usefulinc.com/ns/doap# dbr: http://dbpedia.org/resource/ dbp: http://dbpedia.org/property/ sio: http://semanticscience.org/resource/ opmw: http://www.opmw.org/ontology/ deo: http://purl.org/spar/deo/ doco: http://purl.org/spar/doco/ cito: http://purl.org/spar/cito/ fabio: http://purl.org/spar/fabio/ sro: http://salt.semanticauthoring.org/ontologies/sro#">
      <data class="p-name" value="CV of Amy Guy" />
      <data class="p-url p-uid" value="http://rhiaro.co.uk/<?=$_SERVER['REQUEST_URI']?>" />
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <main class="w1of1 clearfix">
          <div class="w1of1 color3-bg clearfix">
            <header id="header" class="w1of5">
              <? include 'templates/h-card.php'; ?>
            </header>
            <div class="w4of5 lighter-bg"><div class="inner"><article class="w3of4">
            
              <div class="h-card p-contact"><h1 class="p-name">Amy Guy</h1>
              <div>
              <p>Contact: <span class="p-email">amy@rhiaro.co.uk</span></p>
              <p><em>See <a href="http://rhiaro.co.uk/tag/cv">rhiaro.co.uk/tag/cv</a> for a more detailed, dynamic version of my CV.</em></p>
              <data class="u-url" value="http://rhiaro.co.uk"></data>
              </div>
              
              <section id="current-foci">
                <h2>Current foci</h2>
                <div>
                  <section id="phd-informatics" class="p-education h-event">
                    <h3 class="p-name">PhD in Informatics</h3>
                    <div class="h-card p-affiliation">
                      <h4 class="unpad p-org p-name">University of Edinburgh</h4>
                      <p><time class="dt-start" datetime="2012">2012</time>&mdash;<time class="dt-end" datetime="2016">2016 (expected)</time></p>
                      <ul class="p-description">
                        <li>Context-aware decentralised social profiles.</li>
                        <li>Supervisors: Professor Ewan Klein, Professor Dave Robertson</li>
                      </ul>
                    </div>
                  </section>
                  <section id="socialwg" class="p-experience h-event">
                    <h3 class="h-card p-org p-name">W3C Social Web Working Group</h3>
                    <div>
                      <p><time class="dt-start" datetime="2014">2014</time>&mdash;present. <span class="p-description">Working to understand and implement different technologies and protocols for <span class="p-skill">decentralised social interactions</span> in order to contribute to Working Group recommendations. Blog posts at <a href="http://rhiaro.co.uk/tag/socialwg">rhiaro.co.uk/tag/socialwg</a>.</span></p>
                    </div>
                  </section>
                  <section id="rhiaro" class="p-experience">
                    <h3 class="p-name">rhiaro.co.uk</h3>
                    <div>
                      <p class="p-description">My personal site is grounds for experimentation with moving all of my social data from different centralised services to a space that I own. Uses <span class="p-skill">linked data</span>, and <span class="p-skill">decentralised social web</span> technologies, in particular Indieweb protocols. <span class="p-skill">HTML</span>, <span class="p-skill">CSS</span>, <span class="p-skill">PHP</span>, <span class="p-skill">MySQL</span>, <span class="p-skill">Microformats</span>, <span class="p-skill">RDF</span>, <span class="p-skill">SPARQL</span>. Code at <a href="http://bitbucket.org/rhiaro/slogd">bitbucket.org/rhiaro/slogd</a>.</p>
                    </div>
                  </section>
                  <section id="prewired" class="p-experience h-event">
                    <h3 class="h-card p-name p-affiliation">Prewired</h3>
                    <div>
                      <h4 class="p-role">Founder and organiser</h4>
                      <p><time class="dt-start" datetime="2013-09-01">2013</time>&mdash;present. <span class="p-description">A programming club for under 19s, I co-run weekly sessions, <span class="p-skill">communicate with kids and parents</span>, <span class="p-skill">manage sponsors</span>, recruit mentors, <span class="p-skill">organise special events</span>, maintain the website.</span></p>
                    </div>
                  </section>
                </div>
              </section>
              
              <section id="education">
                <h2>Education</h2>
                <div>
                  <section id="msc-icp">
                    <h3>MSc by Research, Interdisciplinary Creative Practices</h3>
                    <div>
                      <h4 class="unpad">College of Art, University of Edinburgh</h4>
                      <p>2011&mdash;2012</p>
                      <ul>
                        <li>Thesis: <em>Location-aware literature</em></li>
                        <li>Core classes taught concepts from architecture, philosophy, art, sociology, media theory, political sciences, choreography, design, history, literature. Additional courses in Natural Language Processing and Semantic Web Systems.</li>
                        <li>Individual and group projects using HTML, CSS, JavaScript, Python, Google AppEngine, Java, RDF, SPARQL.</li>
                      </ul>
                    </div>
                  </section>
                  <section id="bsc-web-tech">
                    <h3>BSc (Hons) Web Technology</h3>
                    <div>
                      <h4 class="unpad">School of Computer Science, University of Lincoln</h4>
                      <p>2008&mdash;2011</p>
                      <ul>
                        <li>First class honors.</li>
                        <li>Award for Outstanding Contribution to the School of Computer Science.</li>
                        <li>Dissertation: <em>CakeBomb, an online collaborative community</em>.</li>
                        <li>Projects using HTML, CSS, JavaScript, PHP, MySQL, C#, ASP.NET, Java, Prolog, Matlab.</li>
                      </ul>
                    </div>
                  </section>
                </div>
              </section>
              
              <section id="publications">
                <h2>Publications and presentations</h2>
                <div>
                  <ul>
                    <li><a href="http://rhiaro.co.uk/pub?p=websci15">Self Curation, Social Partitioning, Escaping from Prejudice and Harassment: the Many Dimensions of Lying Online</a> (Max van Kleek, Dave Murray-Rust, Amy Guy, Daniel Smith and Nigel Shadbolt), ACM WebSci 2015</li>
                    <li>Poster: Self Curation, Social Partitioning, Escaping from Prejudice and Harassment: the Many Dimensions of Lying Online (Max van Kleek, Dave Murray-Rust, Amy Guy, Daniel Smith and Nigel Shadbolt), WWW 2015</li>
                    <li><a href="http://rhiaro.co.uk/pub?p=microposts15">Connections between Twitter Spammer Categories</a> (Gordon Edwards and Amy Guy), Making Sense of Microposts, WWW 2015</li>
                    <li><a href="http://rhiaro.co.uk/pub?p=socm15">Social Personal Data Stores: the Nuclei of Decentralised Social Machines</a> (Max van Kleek, Daniel Smith, Dave Murray-Rust, Amy Guy, Kieron O'Hara, Laura Dragan, Nigel Shadbolt), SOCM, WWW 2015</li>
                    <li>Talk: "Welcome to the IndieWeb", TechMeetup Edinburgh, February 2015.</li>
                    <li><a href="http://rhiaro.co.uk/pub?p=cim14">Roles and relationships as context-aware properties</a> (Amy Guy), Context, Interpretation and Meaning, ISWC 2014</li>
                    <li><a href="http://rhiaro.co.uk/pub?p=socm14">Constructed Identity and Social Machines: A Case Study in Creative Media Production</a> (Amy Guy and Ewan Klein), SOCM, WWW 2014</li>
                    <li>Poster: "Active Digital Content Creators and the Semantic Web" (Amy Guy), Summer School for Ontology Engineering and the Semantic Web, 2013</li>
                    <li>Pecha Kucha: "Digital Media on the Semantic Web", Digital Methods as a Mainstream Methodology, 2012.</li>
                    <li>Speaker and Panellist: Location aware electronic literature, Writing Different Together, Remediating the Social 2012, Edinburgh College of Art.</li>
                    <li>Poster: "CakeBomb: An online collaborative community" (Amy Guy), BCS Lovelace Colloquium 2011 <em>(Winner: Original Project Prize)</em></li>
                    <li>Poster: "Google Writer: A Mashup" (Amy Guy), BCS Lovelace Colloquium 2010 <em>(Runner-up: People's Choice Prize)</em></li>
                  </ul>
                </div>
              </section>
              
              <section id="professional">
                <h2>Professional experience</h2>
                <div>
                  <section id="bbc">
                    <h3>Junior Data Architect, Linked Data Platform</h3>
                    <div>
                      <h4 class="unpad">BBC</h4>
                      <p>June&mdash;October 2014</p>
                      <p>Experiencing Linked Data in the 'real world'. Python, RDF, SPARQL, requirements gathering, data modelling, data quality.</p>
                    </div>
                  </section>
                  <section id="eca">
                    <h3>Developer and Research Assistant, Community Hacking Project</h3>
                    <div>
                      <h4 class="unpad">Edinburgh College of Art</h4>
                      <p>January&mdash;July 2012</p>
                      <p>Created a location-constrained community noticeboard as part of a hyperlocal digital newspaper. HTML, CSS, JavaScript, PHP, MySQL, Wordpress plugin.</p>
                    </div>
                  </section>
                  <section id="google">
                    <h3>University Programmes Intern</h3>
                    <div>
                      <h4 class="unpad">Google</h4>
                      <p>June&mdash;September 2010</p>
                      <p>Worked on internal administration tools for recruiting, awards, and managing outreach events to universities, and trained non-technical staff to use them. HTML, CSS, JavaScript, Python, extreme spreadsheets.</p>
                    </div>
                  </section>
                  <section id="ulsu">
                    <h3>Casual Staff in Marketing &amp; Comms</h3>
                    <div>
                      <h4 class="unpad">University of Lincoln Student's Union</h4>
                      <p>October 2009&mdash;August 2012</p>
                      <p>Publicity for events, and internal adminstration systems, including training staff to use them. Photoshop, InDesign, HTML, CSS, JavaScript, PHP, MS Access, VB.</p>
                    </div>
                  </section>
                  <section id="freelance">
                    <h3>Freelance Web Development</h3>
                    <div>
                      <p>2004&mdash;2015, on and off. Sites for charities, conventions and events, artists, YouTubers, products. Including online stores and ticketing systems, forums, static sites and portfolios, but mostly now defunct or replaced. Occasional consultation, training and social media management. HTML, CSS, JavaScript, PHP, MySQL.</p>
                    </div>
                  </section>
                </div>
              </section>
              
              <section id="teaching">
                <h2>Teaching</h2>
                <div>
                  <section id="sws">
                    <h3>TA and Marker, Semantic Web Systems</h3>
                    <div>
                      <h4 class="unpad">School of Informatics, University of Edinburgh</h4>
                      <p>Jan&mdash;May 2015. Nominated for Teaching Award.</p>
                    </div>
                  </section>
                  <section id="masws">
                    <h3>TA and Marker, Multi-Agent Semantic Web Systems</h3>
                    <div>
                      <h4 class="unpad">School of Informatics, University of Edinburgh</h4>
                      <p>Jan&mdash;May 2014</p>
                    </div>
                  </section>
                  <section id="dwd">
                    <h3>Tutor and demonstrator, Dynamic Web Design</h3>
                    <div>
                      <h4 class="unpad">MSc Design & Digital Media, University of Edinburgh</h4>
                      <p>Jan&mdash;May 2013, Jan&mdash;May 2012</p>
                    </div>
                  </section>
                  <section id="dmsp">
                    <h3>Project Supervisor, Digital Media Studio Project</h3>
                    <div>
                      <h4 class="unpad">MSc Design & Digital Media, University of Edinburgh</h4>
                      <p>Jan&mdash;May 2013</p>
                    </div>
                  </section>
                </div>
              </section>
            
              <section id="volunteer">
                <h2>Outreach, events and volunteering</h2>
                <div>
                  <section id="yrs">
                    <h3>Young Rewired State</h3>
                    <div>
                      <h4 class="unpad">Edinburgh Centre</h4>
                      <p>2015 (Centre Lead), 2014 (co-organiser), 2013 (co-organiser &amp; mentor), 2012 (mentor)</p>
                    </div>
                  </section>
                  <section id="indiewebcamp">
                    <h3>IndieWebCamp Edinburgh</h3>
                    <div>
                      <h4 class="unpad">Organiser</h4>
                      <p>July 2015</p>
                    </div>
                  </section>
                  <section id="bhci">
                    <h3>Hack the Magna Carta</h3>
                    <div>
                      <h4 class="unpad">BritishHCI Hackathon</h4>
                      <p>July 2015</p>
                    </div>
                  </section>
                  <section id="lovelace">
                    <h3>BCSWomen Lovelace Colloquium</h3>
                    <div>
                      <h4 class="unpad">Local organiser</h4>
                      <p>April 2015</p>
                    </div>
                  </section>
                  <section id="smartdatahack">
                    <h3>Smart Data Hack, School of Informatics</h3>
                    <div>
                      <h4 class="unpad">Co-organiser</h4>
                      <p>2015, 2014, 2013</p>
                    </div>
                  </section>
                  <section id="okfn">
                    <h3>Community Coordinator for Edinburgh</h3>
                    <div>
                      <h4 class="unpad">Open Knowledge Foundation</h4>
                      <p>December 2012&mdash;present</p>
                      <ul>
                        <li>Co-organised monthly Open Data Maker Nights, 2014&mdash;2015</li>
                        <li>Co-organised Open Data Day Edinburgh, 2013</li>
                      </ul>
                    </div>
                  </section>
                  <section id="shrub">
                    <h3>Edinburgh Swap &amp; Reuse Hub (SHRUB)</h3>
                    <div>
                      <h4 class="unpad">Swap Shop volunteer</h4>
                      <p>October 2013&mdash;June 2014</p>
                    </div>
                  </section>
                  <section id="prewired">
                    <h3>Prewired, under 19s programming club</h3>
                    <div>
                      <h4 class="unpad">Co-founder and organiser</h4>
                      <p>2013&mdash;present</p>
                    </div>
                  </section>
                  <section id="freegle">
                    <h3>Edinburgh Freegle</h3>
                    <div>
                      <h4 class="unpad">Volunteer Moderator</h4>
                      <p>September 2013&mdash;June 2015</p>
                    </div>
                  </section>
                  <section id="socieTea">
                    <h3>University of Edinburgh SocieTea</h3>
                    <div>
                      <h4 class="unpad">President</h4>
                      <p>2012&mdash;2014</p>
                    </div>
                  </section>
                  <section id="lincoln-computing">
                    <h3>University of Lincoln Computing Society</h3>
                    <div>
                      <h4 class="unpad">Founder and President</h4>
                      <p>2010&mdash;2011</p>
                    </div>
                  </section>
                </div>
              </section>
              
            </div></article></div></div>
          </div>
  <?
  include("templates/end.php");
  ?>
  