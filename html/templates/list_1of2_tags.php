<?
$tagslist = new Slogd_Renderer($ep);
if($tagslist->render_tags()){ 
    $tags = $tagslist->get_output(); 
    $count = $tagslist->get_count();
}
?>
    <div class="w1of2"><div class="inner">
            <h3>Tags (<?=$count?>)</h3>
            
            <?if($tagslist->get_errors()):?>
                <div class="fail"><h3>There was a problem..</h3>
                    <?foreach($tagslist->get_errors() as $t => $e):?>
                        <p><strong><?=$t?>:</strong> <? var_dump($e); ?></p>
                    <?endforeach?>
                </div>
            <?endif?>

            <?=($tags) ? $tags : ""?>

            <pre class="color1-bg lighter showhide"><span class="trigger">&gt; sparql</span><span class="contents"><?=$tagslist->get_query(true)?></span></pre>
        </div></div>
</div>