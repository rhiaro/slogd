<? $sortdate = $post['published']; ?>
<p class="w3of4">
  <?if($sortdate != $trackdate):?>
    <strong><?=$post['published']->format("jS F Y")?></strong>
  <?endif?>
</p>
<div class="w3of4<?=isset($post['icon']) ? " color3-bg":""?>">
  <?=isset($post['reply-to']) ? "<div class=\"color2-bg inner\"><p>in reply to <a href=\"".$post['reply-to']."\" class=\"u-in-reply-to\">".$post['reply-to']."</a> <i class=\"fa fa-reply fa-3x lighter right\"></i></p></div>":""?>
  <div class="inner">
  <?if(isset($post['icon']) && $post['icon'] != "reply"):?>
    <i class="fa fa-<?=$post['icon']?> fa-3x lighter right"></i>
  <?endif?>

  <? // If it's a post from my site with author not set, it's me. But could a post on my site with a different author. ?>
  <?if((!isset($post['author']) && stripos($post['url'], "rhiaro.co.uk") <= 0) || (isset($post['author']) && $post['author']['url'] != "http://rhiaro.co.uk/about#me" && $post['author']['url'] != "http://rhiaro.co.uk" && $post['author']['url'] != "http://www.blogger.com/profile/12227954801080178130")):?>
    <p><?=isset($post['author']['photo']) ? "<img src=\"".$post['author']['photo']."\" class=\"u-photo midicon\" />" : "<i class=\"fa fa-user fa-2x color1\"></i>"?> <a href="<?=isset($post['author']['url']) ? $post['author']['url'] : "#"?>" class="h-card p-name u-url u-author"><?=isset($post['author']['name']) ? $post['author']['name'] : "Anon"?></a></p>
  <?endif?>
  
  <div class="e-content p-name<?=isset($post['contentmf']) ? " ".implode(" ",$post['contentmf']) : ""?>">
  
    <?if(isset($post['content'])):?>
      <?=$post['content']?>
    <?endif?>
  
    <?if(!isset($post['content']) && isset($post['reply-to'])):?>
      <blockquote><?=$post['url']?></blockquote>
    <?endif?>

  </div>
  
  <p class="align-right unpad"><a href="<?=$post['url']?>" class="u-url u-uid"><time class="dt-published wee" datetime="<?=$post['published']->format(DATE_ATOM)?>"><?=$post['published']->format("H:i (T)")?></time></a></p>
</div></div>
<? $trackdate = $sortdate; $taglinks = []; ?>