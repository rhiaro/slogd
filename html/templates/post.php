<? $sortdate = $post['published']; ?>

<p class="w3of4">
  <?if(!isset($trackdate) || $trackdate == 0 || ($sortdate->format("Ymd") != $trackdate->format("Ymd"))):?>
    <strong><?=$post['published']->format("jS F Y")?></strong>
  <?endif?>
</p>
<div class="w3of4<?=isset($post['icon']) ? " color3-bg\" style=\"margin-bottom: 1em;":""?>">
  <?=isset($post['reply-to']) ? "<div class=\"color2-bg inner\"><p>in reply to <a href=\"".$post['reply-to']."\" class=\"u-in-reply-to\" property=\"as2:inReplyTo sioc:reply_of\">".$post['reply-to']."</a> <i class=\"fa fa-reply fa-3x lighter right\"></i></p></div>":""?>
  <div class="inner">
  <?if(isset($post['icon']) && $post['icon'] != "reply"):?>
    <i class="fa fa-<?=$post['icon']?> fa-3x lighter right"></i>
  <?endif?>

  <? // If it's a post from my site with author not set, it's me. But could a post on my site with a different author. ?>
  <?if((!isset($post['author']) && stripos($post['url'], "rhiaro.co.uk") <= 0) || (isset($post['author']) && $post['author']['url'] != "http://rhiaro.co.uk/about#me" && $post['author']['url'] != "http://rhiaro.co.uk" && $post['author']['url'] != "http://www.blogger.com/profile/12227954801080178130")):?>
    <p><?=isset($post['author']['photo']) ? "<img src=\"".$post['author']['photo']."\" class=\"u-photo midicon\" />" : "<i class=\"fa fa-user fa-2x color1\"></i>"?> <a href="<?=isset($post['author']['url']) ? $post['author']['url'] : "#"?>" class="h-card p-name u-url u-author" property="as2:attributedTo dc:creator sioc:has_creator prov:wasAttributedTo"><?=isset($post['author']['name']) ? $post['author']['name'] : "Anon"?></a></p>
  <?endif?>
  
  <?if(isset($post['name'])):?>
    <h1 class="p-name" property="as2:name dct:title"><?=$post['name']?></h1>
  <?endif?>
  
  <div class="e-content p-name<?=isset($post['contentmf']) ? " ".implode(" ",$post['contentmf']) : ""?>" property="as2:content sioc:content">
  
    <?if(isset($post['content'])):?>
      <?=$post['content']?>
    <?endif?>
  
    <?if(!isset($post['content']) && isset($post['reply-to'])):?>
      <blockquote property="as2:inReplyTo sioc:reply_of"><?=$post['url']?></blockquote>
    <?endif?>
    
    <?if(isset($post['location']) && !isset($post['content'])):?>
      <p class="p-location" property="as2:location"><?=str_replace("http://dbpedia.org/resource/","",str_replace("http://rhiaro.co.uk/","",$post['location']))?></p>
    <?endif?>
    
    <?if(isset($post['startLocation']) && isset($post['endLocation'])):?>
      <p>
        <span class="<?=(isset($post['state']) && ($post['state'] == "cancelled" || $post['state'] == "rescheduled" || $post['state'] == "missed")) ? " strike" : ""?>"><strong>Travel plan:</strong> <?=str_replace("http://dbpedia.org/resource/","",str_replace("http://rhiaro.co.uk/","",$post['startLocation']))?> to <?=str_replace("http://dbpedia.org/resource/","",str_replace("http://rhiaro.co.uk/","",$post['endLocation']))?></span>
        <?=(isset($post['state'])) ? "<strong class=\"upp\">".$post['state']."</strong>" : ""?>
        <?=(isset($post['isReplacedBy'])) ? " (<a href=\"".$post['isReplacedBy']."\">replacement</a>)" : ""?>
      </p>
    <?endif?>
    
    <?if(isset($post['start']) && isset($post['end'])):?>
      <p class="unpad<?=(isset($post['state']) && ($post['state'] == "cancelled" || $post['state'] == "rescheduled" || $post['state'] == "missed")) ? " strike" : ""?>"><time class="dt-start" property="as2:startTime" datetime="<?=$post['start']->format(DATE_ATOM)?>"><?=$post['start']->format("Ymd") == $post['published']->format("Ymd") ? "Today, ".$post['start']->format("H:i") : $post['start']->format("jS F Y H:i (T)")?></time> - <time class="dt-end" property="as2:endTime" datetime="<?=$post['end']->format(DATE_ATOM)?>"><?=$post['end']->format("Ymd") == $post['start']->format("Ymd") ? $post['end']->format("H:i") : $post['end']->format("jS F Y H:i (T)")?></time></p>
    <?endif?>
    
    <?if(in_array("rsvp", $post['tags'])):?>
      <data value="yes" class="p-rsvp"></data>
    <?endif?>

  </div>
  
  <p class="wee unpad">
    <?=(isset($post['location'])) ? "<i class=\"fa fa-street-view\"></i> <span class=\"p-location\"><a href=\"".$post['location']."\" property=\"as:location\">".str_replace("http://dbpedia.org/resource/","",str_replace("http://rhiaro.co.uk/","",$post['location']))."</a></span>" : ""?>
    <?if(isset($post['tags']) && count($post['tags']) > 0 && $post['tags'][0] != ""):?>
      <?foreach($post['tags'] as $tag):?>
        <? $taglinks[] = "<a href=\"/tag/".urlencode($tag)."\" property=\"as2:tag sioc:topic\" class=\"p-category\">".$tag."</a>"; ?>
      <?endforeach?>
    <?endif?>
    <?=(!empty($taglinks)) ? "<i class=\"fa fa-tags\"></i> ".implode(", ",$taglinks) : "" ?>
    <?=isset($post['replies']) ? "<i class=\"fa fa-comments-o\"></i> <a href=\"".$post['url']."#mentions\">".count($post['replies'])." mentions</a>" : "" ?>
  </p>
  <p class="wee left"><a title="Post this on twitter" href="http://twitter.com/home?status=RT @rhiaro: <?=isset($post['to']) ? str_replace("http://twitter.com/", "@", $post['to'])." " : ""?><?=urlencode(strip_tags($post['content']))?> <?=urlencode("#".implode(" #", $post['tags']))?> <?=isset($post['to']) ? "(cc ".str_replace("http://twitter.com/", "@", $post['cc']).") " : ""?><?=$post['url']?>"><i class="fa fa-twitter"></i></a></p>
  
  <p class="align-right unpad"><a href="<?=$post['url']?>" class="u-url u-uid"><time class="dt-published wee" property="as2:published dct:created" datetime="<?=$post['published']->format(DATE_ATOM)?>"><?=$post['published']->format("H:i (T)")?></time></a></p>
</div></div>
<? $trackdate = $sortdate; $taglinks = []; ?>