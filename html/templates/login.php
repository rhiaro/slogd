<?
//$base = "localhost";
$base = "http://rhiaro.co.uk";
if(isset($_GET['code'])){
  $params = "code=".$_GET['code']."&redirect_uri=".urlencode($_GET['state'])."&state=".urlencode($_GET['state'])."&client_id=http://rhiaro.co.uk";
  $ch = curl_init("https://indieauth.com/auth");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
  $response = Array();
  parse_str(curl_exec($ch), $response);
  $_SESSION['me'] = $response['me'];
  $info = curl_getinfo($ch);
  curl_close($ch);
}
?>
<?if(isset($response) && ($response === false || $info['http_code'] != 200)):?>
  <div class="fail">
    <p><strong></strong><?=$info['http_code']?></strong> Something went wrong...</p>
    <?if(curl_error($ch)):?>
      <p><?=curl_error($ch)?></p>
    <?endif?>
  </div>
<?endif?>

<?if(isset($_SESSION['me'])):?>
  <p>You are logged in as <strong><?=$_SESSION['me']?></strong></p>
  <p><a href="/?logout=1">Logout</a></p>
<?else:?>
  <form action="https://indieauth.com/auth" method="get">
    <label for="indie_auth_url" class="neat">Web Address:</label>
    <input id="indie_auth_url" type="text" name="me" placeholder="yourdomain.com" class="neat" />
    <p><button type="submit">Sign In</button></p>
    <input type="hidden" name="client_id" value="http://rhiaro.co.uk" />
    <input type="hidden" name="redirect_uri" value="<?=$base?><?=$_SERVER["REQUEST_URI"]?>" />
    <input type="hidden" name="state" value="<?=$base?><?=$_SERVER["REQUEST_URI"]?>" />
  </form>
<?endif?>
