<? $sortdate = $post['published']; ?>

<?if(!$trackdate || $sortdate->format("Ymd") != $trackdate->format("Ymd")):?>
  <p style="display: inline-block; width:100%"><strong><?=$post['published']->format("jS F Y")?></strong></p>
<?endif?>
<div class="w1of4" style="height: 300px;">
  
  <div class="color2-bg w1of1 align-center" style="height: 164px; overflow:hidden;">
    <?if(isset($post['image'])):?>
      <img src="<?=$post['image'][0]?>"/>
    <?else:?>
      <i class="fa fa-2x fa-camera-retro lighter"></i>
    <?endif?>
  </div>
  
  <div class="inner<?=isset($post['icon']) ? " color3-bg":""?>">
    <?if(isset($post['icon']) && $post['icon'] != "reply"):?>
      <i class="fa fa-<?=$post['icon']?> fa-3x lighter right"></i>
    <?endif?>
    
    <p>
      <?=(isset($post['summary'])) ? $post['summary'] : "" ?><?=(isset($post['cost'])) ? " (".$post['cost'].")" : "" ?>
    </p>
    <p class="wee unpad">
      <?=(isset($post['location'])) ? "<i class=\"fa fa-street-view\"></i> <span class=\"p-location\"><a href=\"".$post['location']."\" property=\"as:location\">".str_replace("http://dbpedia.org/resource/","",str_replace("http://rhiaro.co.uk/","",$post['location']))."</a></span>" : ""?>
      <?if(isset($post['tags']) && count($post['tags']) > 0 && $post['tags'][0] != ""):?>
        <?foreach($post['tags'] as $tag):?>
          <? $taglinks[] = "<a href=\"/tag/".urlencode($tag)."\" property=\"as2:tag sioc:topic\" class=\"p-category\">".$tag."</a>"; ?>
        <?endforeach?>
      <?endif?>
      <?=(!empty($taglinks)) ? "<i class=\"fa fa-tags\"></i> ".implode(", ",$taglinks) : "" ?>
      <?=isset($post['replies']) ? "<i class=\"fa fa-comments-o\"></i> <a href=\"".$post['url']."#mentions\">".count($post['replies'])." mentions</a>" : "" ?>
    </p>
    <p class="align-right unpad"><a href="<?=$uri?>"><time class="dt-published wee" property="as2:published dct:created" datetime="<?=$post['published']->format(DATE_ATOM)?>"><?=$post['published']->format("H:i (T)")?></time></a></p>
  </div>
</div>
  
<? $trackdate = $sortdate; $taglinks = []; ?>