<?
if(isset($post['start'])) { $sortdate = $post['start']; }
else { $sortdate = $post['published']; }
?>
<p class="w3of4">
  <?if(!$trackdate || $sortdate->format("Ymd") != $trackdate->format("Ymd")):?>
    <strong><?=$sortdate->format("jS F Y")?></strong>
  <?endif?>
</p>
<div class="w3of4<?=isset($post['icon']) ? " color3-bg":""?>">
  <?=isset($post['reply-to']) ? "<div class=\"color2-bg inner\"><p>in reply to <a href=\"".$post['reply-to']."\" class=\"u-in-reply-to\">".$post['reply-to']."</a> <i class=\"fa fa-reply fa-3x lighter right\"></i></p></div>":""?>
  <div class="inner">
  <?if(isset($post['icon']) && $post['icon'] != "reply"):?>
    <i class="fa fa-<?=$post['icon']?> fa-3x lighter right"></i>
  <?endif?>
  
  <?if(isset($post['name'])):?>
    <h1 class="p-name" property="as2:name dct:title"><?=$post['name']?></h1>
  <?endif?>
  
  <div class="e-content<?=isset($post['contentmf']) ? " ".implode(" ",$post['contentmf']) : ""?>">
    <?if(isset($post['content'])):?>
      <?=$post['content']?>
    <?endif?>
    
    <?if(isset($post['location']) && !isset($post['content'])):?>
      <p class="p-location"><?=str_replace("http://dbpedia.org/resource/","",str_replace("http://rhiaro.co.uk/","",$post['location']))?></p>
    <?endif?>
    
    <?if(isset($post['startLocation']) && isset($post['endLocation'])):?>
      <p>
        <span class="<?=(isset($post['state']) && ($post['state'] == "cancelled" || $post['state'] == "rescheduled" || $post['state'] == "missed")) ? " strike" : ""?>"><strong>Travel plan:</strong> <?=str_replace("http://dbpedia.org/resource/","",str_replace("http://rhiaro.co.uk/","",$post['startLocation']))?> to <?=str_replace("http://dbpedia.org/resource/","",str_replace("http://rhiaro.co.uk/","",$post['endLocation']))?></span>
        <?=(isset($post['state'])) ? "<strong class=\"upp\">".$post['state']."</strong>" : ""?>
        <?=(isset($post['isReplacedBy'])) ? " (<a href=\"".$post['isReplacedBy']."\">replacement</a>)" : ""?>
      </p>
    <?endif?>
    
    <?if(isset($post['start']) && isset($post['end'])):?>
      <p class="unpad<?=(isset($post['state']) && ($post['state'] == "cancelled" || $post['state'] == "rescheduled" || $post['state'] == "missed")) ? " strike" : ""?>"><time class="dt-start" datetime="<?=$post['start']->format(DATE_ATOM)?>"><?=$post['start']->format("H:i")?></time> - <time class="dt-end" datetime="<?=$post['end']->format(DATE_ATOM)?>"><?=$post['end']->format("Ymd") == $post['start']->format("Ymd") ? $post['end']->format("H:i") : $post['end']->format("jS F Y H:i (T)")?></time></p>
    <?endif?>
  </div>
  
  <p class="wee unpad">
    <?=(isset($post['location'])) ? "<i class=\"fa fa-street-view\"></i> <span class=\"p-location\">".str_replace("http://dbpedia.org/resource/","",str_replace("http://rhiaro.co.uk/","",$post['location']))."</span>" : ""?>
    <?foreach($post['tags'] as $tag):?>
      <? $taglinks[] = "<a href=\"/tag/".urlencode($tag)."\" class=\"p-category\">".$tag."</a>"; ?>
    <?endforeach?>
    <?=(!empty($post['tags'])) ? "<i class=\"fa fa-tags\"></i> ".implode(", ",$taglinks) : "" ?>
  </p>
  
  <p class="align-right unpad"><a href="<?=$post['url']?>" class="u-url u-uid"><time class="dt-published wee" datetime="<?=$post['published']->format(DATE_ATOM)?>"><?=($post['published'] != $sortdate) ? $post['published']->format("jS F Y H:i (T)") : $post['published']->format("H:i (T)")?></time></a></p>
</div></div>
<? $trackdate = $sortdate; $taglinks = []; ?>