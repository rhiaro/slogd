<? if(!isset($template)) $template = "post"; ?>
<?foreach($items as $uri => $post):?>
  <?if($uri != "template"):?>
  <article<?=$h != "h-entry" && !(isset($hchild)) ? " class=\"h-entry\"" : " class=\"".$hchild."\""?> typeof="as2:Article" about="<?=$uri?>">
    <? include 'templates/'.$template.'.php'; ?>
  </article>
  <?endif?>
<?endforeach?>

<div class="w3of4">
  <?if(isset($prevy) && !isset($prevm)):?>
    <p class="left"><a href="/<?=$prevy?>/"><i title="Previous year" class="fa fa-arrow-circle-left fa-2x"></i></a></p>
  <?elseif(isset($prevy) && isset($prevm)):?>
    <p class="left"><a href="/<?=$prevy?>/<?=$prevm?>"><i title="Previous month" class="fa fa-arrow-circle-left fa-2x"></i></a></p>
  <?endif?>
  
  <?if((isset($nexty) && !isset($nextm)) && strtotime($nexty."-01-01") <= time()):?>
    <p class="right"><a href="/<?=$nexty?>/"><i title="Next year" class="fa fa-arrow-circle-right fa-2x"></i></a></p>
  <?elseif((isset($nexty) && isset($nextm)) && strtotime($nexty."-".$nextm."-01") <= time()):?>
    <p class="right"><a href="/<?=$nexty?>/<?=$nextm?>"><i title="Next month" class="fa fa-arrow-circle-right fa-2x"></i></a></p>
  <?endif?>
</div>

<?if($postlist->get_errors()):?>
  <div class="fail"><h4>There was a problem..</h4>
    <?foreach($postlist->get_errors() as $t => $e):?>
      <p><strong><?=$t?>:</strong> <? var_dump($e); ?></p>
    <?endforeach?>
  </div>
<?endif?>
