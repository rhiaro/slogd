<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title><?=(isset($title)) ? $title : "What Amy Does"?></title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="pingback" href="https://webmention.io/rhiaro.co.uk/xmlrpc" />
        <link rel="webmention" href="https://webmention.io/rhiaro.co.uk/webmention" />
        <link rel="mentions" href="https://webmention.io/api/links?format=jf2" />
        <link rel="authorization_endpoint" href="https://indieauth.com/auth" />
        <link rel="token_endpoint" href="https://tokens.indieauth.com/token" />
        <link rel="micropub" href="http://rhiaro.co.uk/activitypub.php" />
        <link rel="stylesheet" href="/css/normalize.min.css" />
        <link rel="stylesheet" href="/css/main.css" />
        <link rel="stylesheet" href="/css/font-awesome.min.css" />
        <link rel="icon" href="/img/favicon.png" />

        <!--[if lt IE 9]>
            <script src="js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
        <![endif]-->
    </head>
    <body class="<?=isset($h) ? $h : "h-feed"?>" prefix="rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns# rdfs: http://www.w3.org/2000/01/rdf-schema# owl: http://www.w3.org/2002/07/owl# xsd: http://www.w3.org/2001/XMLSchema# dct: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ v: http://www.w3.org/2006/vcard/ns# prov: http://www.w3.org/ns/prov# wgs: http://www.w3.org/2003/01/geo/wgs84_pos# dbr: http://dbpedia.org/resource/ dbp: http://dbpedia.org/property/ cito: http://purl.org/spar/cito/ sioc: http://rdfs.org/sioc/ns# as2: http://www.w3.org/ns/activitystreams#" typeof="<?=isset($h) ? "as2:Article sioc:Post" : "as2:Collection"?> prov:Entity" about="http://rhiaro.co.uk<?=$_SERVER['REQUEST_URI']?>">
      <data class="p-name" value="<?=$title?>"></data>
      <data class="u-url u-uid" value="http://rhiaro.co.uk<?=$_SERVER['REQUEST_URI']?>"></data>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <main class="w1of1 clearfix">
