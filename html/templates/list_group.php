<? if(!isset($template)) $template = "post"; ?>
<h1><?=$listheader?> (<?=$c?> posts)</h1>
<?foreach($lists as $n => $items):?>
  <h2 class="w3of4"><?=$n?></h2>
  <? $trackdate = 0; ?>
    <?foreach($items as $uri => $post):?>
      <?if($uri != "template"):?>
        <article<?=$h != "h-entry" ? " class=\"h-entry\"" : ""?>>
        <? include 'templates/'.$template.'.php'; ?>
        </article>
      <?endif?>
    <?endforeach?>
<?endforeach?>