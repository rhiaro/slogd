<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>What Amy Does<?=(isset($title)) ? $title : ""?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="pingback" href="https://webmention.io/rhiaro.co.uk/xmlrpc" />
        <link rel="webmention" href="https://webmention.io/rhiaro.co.uk/webmention" />
        <link rel="mentions" href="https://webmention.io/api/links?format=jf2" />
        <link rel="authorization_endpoint" href="https://indieauth.com/auth" />
        <link rel="token_endpoint" href="https://tokens.indieauth.com/token" />
        <link rel="micropub" href="http://rhiaro.co.uk/micropub.php" />
        <link rel="stylesheet" href="/css/normalize.min.css"/>
        <link rel="stylesheet" href="/css/main.css"/>
        <link rel="stylesheet" href="/css/font-awesome.min.css"/>
        <link href='http://fonts.googleapis.com/css?family=Source+Code+Pro:200,300,400,500,600,700,900' rel='stylesheet' type='text/css'>
        <link rel="icon" href="/img/favicon.png"/>

        <!--[if lt IE 9]>
            <script src="js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <main class="w1of1 clearfix">
            <div class="w1of1 clearfix">
                <div class="w1of3">
                    <h1>What Amy Does</h1>
                </div>
                <nav class="w2of3"><div class="inner">
                    <ul>
                        <!--<li><strike><a href="xmpp://amy@amy.so"><i class="icon-comments"></i> XMPP: amy@amy.so</a></strike> pending repair of xmpp server</li>-->
                        <li title="lastfm"><a href="http://last.fm/user/theringleader" rel="me"><i class="fa fa-lastfm-square fa-2x"></i></a></li>
                        <li title="irc"><a href="irc://irc.imaginarynet.org.uk:6667/rhiaro" rel="me"><i class="fa fa-comments fa-2x"></i> irc</a></li>
                        <li title="linked in"><a href="http://www.linkedin.com/profile/view?id=80896287" rel="me"><i class="fa fa-linkedin-square fa-2x"></i></a></li>
                        <li title="bitbucket"><a href="http://bitbucket.org/rhiaro" rel="me"><i class="fa fa-bitbucket fa-2x"></i></a></li>
                        <li title="twitter"><a href="http://twitter.com/rhiaro" rel="me"><i class="fa fa-twitter fa-2x"></i></a></li>
                        <li title="calendar"><a href="https://www.google.com/calendar/embed?src=bl2ob1u3v24rh3tanb6l219e04%40group.calendar.google.com&ctz=Europe/Rome"><i class="fa fa-calendar fa-2x"></i></a></li>
                        <li title="email"><a href="mailto:amy[at]rhiaro.co.uk" rel="me"><i class="fa fa-envelope fa-2x"></i></a></li>
                    </ul>
                </div></nav>
            </div>