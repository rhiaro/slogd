<div class="w1of1 clearfix">
    <div class="w1of2 center"><div class="inner">
        <h3><?=$listheader?> (<?=$c?>)</h3>
        <p><?=$listintro?></p>
        <ul class="plist">
            <div class="h-feed">
            <?foreach($items as $uri => $item):?>
                <?if($uri != "template"):?>
                    <li class="h-entry box<?=!isset($item['icon']) ? "" : " color3-bg"?>">
                      
                      <p class="p-name e-content">
                        <span class="h-card p-name"><a href="<?=$item['a_url']?>" class="u-url"><?=$item['author']?></a></span>
                        <?=$item['content']?>
                      </p>
                      <p class="right wee"><a class="u-url u-uid" href="<?=$item['url']?>">
                            <time class="dt-published" datetime="<?=date(DATE_ATOM, $item['date'])?>"><?=date(DATE_ATOM, $item['date'])?></time>
                        </a></p>
                        
                    </li>
                <?endif?>
            <?endforeach?>
            </div>
        </ul>
        <?if($postlist->get_errors()):?>
            <div class="fail"><h4>There was a problem..</h4>
                <?foreach($postlist->get_errors() as $t => $e):?>
                    <p><strong><?=$t?>:</strong> <? var_dump($e); ?></p>
                <?endforeach?>
            </div>
        <?endif?>
