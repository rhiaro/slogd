<nav>
    <ul>
        <!--<li><strike><a href="xmpp://amy@amy.so"><i class="icon-comments"></i> XMPP: amy@amy.so</a></strike> pending repair of xmpp server</li>-->
        <li title="posts"><a href="/"><i class="fa fa-pencil-square-o fa-2x"></i></a></li>
        <li title="irc"><a href="irc://irc.imaginarynet.org.uk:6667/rhiaro" rel="me"><i class="fa fa-comments fa-2x"></i></a></li>
        <li title="bitbucket"><a href="http://bitbucket.org/rhiaro" rel="me"><i class="fa fa-bitbucket fa-2x"></i></a></li>
        <li title="calendar"><a href="/calendar"><i class="fa fa-calendar fa-2x"></i></a></li>
        <li title="email"><a href="mailto:amy@rhiaro.co.uk" rel="me" class="u-email"><i class="fa fa-envelope fa-2x"></i></a></li>
        <li title="linked in"><a href="http://www.linkedin.com/profile/view?id=80896287" rel="me"><i class="fa fa-linkedin-square fa-2x"></i></a></li>
        <li title="travel"><a href="/travel"><i class="fa fa-road fa-2x"></i></a></li>
        <li title="twitter"><a href="http://twitter.com/rhiaro" rel="me"><i class="fa fa-twitter fa-2x"></i></a></li>
        <li title="lastfm"><a href="http://last.fm/user/theringleader" rel="me"><i class="fa fa-lastfm-square fa-2x"></i></a></li>
    </ul>
</nav>