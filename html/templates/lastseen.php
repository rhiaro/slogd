<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');


$wherelist = new Slogd_Renderer($ep);
if($wherelist->render_checkins()){
    $wheres = $wherelist->get_output();
}

$locations = $wheres['locations'];
$currently = reset($wheres['vague']);
$checkin = reset($wheres['specific']);
foreach($wheres['events'] as $uri => $post){
  if($post['start'] > time()){
    $post['uri'] = $uri;
    $next = $post;
    unset($wheres['events'][$uri]);
  }else{
    $event = reset($wheres['events']);
    $event['uri'] = $uri;
    break;
  }
}
if($event['end'] > $checkin['published']){
  $last = "";
  if(isset($event['name'])){ $last = "at <a href=\"".$event['uri']."\">".$event['name']."</a> "; }
  if(isset($event['startLocation'])) { $last .= "in transit between ".str_replace("http://dbpedia.org/resource/","",$event['startLocation'])." and ".str_replace("http://dbpedia.org/resource/","",$event['endLocation']); }
  elseif(isset($event['location'])){ $last .= "in ".str_replace("http://dbpedia.org/resource/","",$event['location']); }
}else{
  $last = "";
  if(isset($checkin['name'])){ $last = "at ".$checkin['name']." "; }
  $last .= "in ".str_replace("http://dbpedia.org/resource/","",$checkin['location']);
}

if(isset($next['name'])){
  $next['label'] = $next['name'];
}else{
  if(isset($next['startLocation'])) { $next['label'] .= "course set from ".str_replace("http://dbpedia.org/resource/","",$next['startLocation'])." to ".str_replace("http://dbpedia.org/resource/","",$next['endLocation']); }
  elseif(isset($next['location'])){ $next['label'] .= "in ".str_replace("http://dbpedia.org/resource/","",$next['location']); }
}

$current = $locations[$currently['location']]['present'];

?>
<hr/>
<div class="wee align-left inner">
  <p><i class="fa fa-street-view"></i> Currently: <strong><a href="/where"><?=$current?></a></strong></p>
  <p><i class="fa fa-map-marker"></i> Last spotted: <strong><?=$last?></strong></p>
  <p><i class="fa fa-calendar"></i> Next: <strong><a href="<?=$next['uri']?>"><?=$next['label']?></a></strong></p>
</div>