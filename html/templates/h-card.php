<div class="h-card p-author contactbox" rel="dc:creator as2:actor" about="http://rhiaro.co.uk/about#me" typeof="as2:Actor" id="amy">
  <img src="http://rhiaro.co.uk/stash/dp.png" alt="Photo" class="midicon u-photo" />
  <data class="p-name" property="as2:name foaf:name" value="Amy Guy"></data>
  <data class="p-url" property="foaf:homepage" value="http://rhiaro.co.uk"></data>
  <ul class="bloblist tagcloud lighter">
    <li><a href="/tag/indieweb" class="p-category">indieweb</a></li>
    <li><a href="/tag/linked+data" class="p-category">linked data</a></li>
    <li><a href="/tag/phd" class="p-category">PhD</a></li>
    <li><a href="/tag/travel" class="p-category">travel</a></li>
    <li><a href="/tag/hacking" class="p-category">hacking</a></li>
    <li><a href="/tag/socialwg" class="p-category">socialwg</a></li>
  </ul>
  <? include("templates/contactbuttons.php"); ?>
  <? include("templates/lastseen.php"); ?>
</div>