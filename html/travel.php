<?
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');

$tag = "travel";
$postlist = new Slogd_Renderer($ep);
if($postlist->render_expanded_list(array("view"=>"travel"))){
  $postlist->index_by("date");
  $postlist->index_by("period");
	$lists = $postlist->get_output();
	$c = $postlist->get_count();
}

$title = "$c travel posts";
include("templates/home_top.php");
$listheader = "Travel & events";
$template = "post_actualtime";

?>
<div class="w1of1 color3-bg clearfix">
  <div class="w1of5">
    <? include 'templates/h-card.php'; ?>
  </div>
  <div class="w4of5 lighter-bg"><div class="inner">
  <? include("templates/list_group.php"); ?>
  </div></div>
</div>
<?
include("templates/end.php");
?>