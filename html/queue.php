<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
include_once 'lib/Slogd_Tripler.php';

function get_paths($dir, $date){
    $foundpaths = scandir($dir, 1);
    for($i=0;$i<count($foundpaths);$i++){
        $tmp = $dir . "/" . $foundpaths[$i];
        if($foundpaths[$i] != "." && $foundpaths[$i] != ".." && !is_dir($tmp)){
            if(filemtime($tmp) > $date){
                $paths[] = $tmp;
            }
        }
    }
    return $paths;
}


/*$trip = new Slogd_Tripler($ep, "posts");*/

if(isset($_GET['insert']) && $_GET['insert'] == "manual"){

    if(isset($_POST['sub'])){
        $subpath = $_POST['subpath'];
        $trip = new Slogd_Tripler($ep, $subpath);
        $trip->insert(0);
    }elseif(isset($_POST['updsub'])){
        $uri = $_POST['upd'];
        $subpath = $_POST['subpath'];
        $trip = new Slogd_Tripler($ep, $subpath, $uri);
        $trip->insert(0);
    }

    if(isset($_POST['date'])){
        update_last_updated($ep);
    }

    $d = get_last_updated($ep);
    $paths = get_paths("posts", $d);
}

?>
<html>
<head><title>Slog'd posts...</title></head>
<body>
	<h1>Blog or whatever</h1>
    <p><strong>Last updated: <?=date("Y-m-d h:i", $d)?></strong></p>
    <hr/>
    
    <form method="post">
        <input type="submit" name="date" value="Update blog last updated date" />
    </form>
    <hr/>

	<h2>Queued posts</h2>
    
    <?if($paths):?>
        <?foreach($paths as $path):?>
            <p><strong><?=$path?></strong></p>
            <?
            $ts = new Slogd_Tripler($ep, $path);
            $t = $ts->get_triples();
            ?>
            <p><em><?=$ts->get_title()?></em></p>
            <p><em><?=$ts->get_url()?></em></p>
            <pre style="height:200px;overflow:scroll;">
                <?=htmlentities($t[0])?>
            </pre>
            <form method="post">
                <input type="hidden" value="<?=$path?>" name="subpath" />
                <p><input type="submit" name="sub" value="Insert" /></p>
            </form>
            <form method="post">
                <input type="hidden" value="<?=$path?>" name="subpath" />
                <p><input type="text" name="upd" value="http://blog.rhiaro.co.uk/"/> <input type="submit" name="updsub" value="Update" /></p>
            </form>
            
            <hr/>
        <?endforeach?>
    <?else:?>
        <p>No pending posts</p>
    <?endif?>
	
</body>
</html>