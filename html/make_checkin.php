<?
echo htmlentities("
PREFIX as2: <http://www.w3.org/ns/activitystreams#>
PREFIX dct: <http://purl.org/dc/terms/>
INSERT INTO <http://blog.rhiaro.co.uk#> {
");

$w = $_GET['w'];
if(isset($_GET['d'])){
    $d = strtotime(date("Y-m-d ").$_GET['d']);
}else{
    $d = time();
}
echo htmlentities("<http://blog.rhiaro.co.uk/".$d."-".urlencode(strtolower($w))."> as2:location <http://rhiaro.co.uk/location/".$w."> ; dct:created \"".date(DATE_ATOM, $d)."\"^^xsd:dateTime . }");
?>