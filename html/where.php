<?
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');


$postlist = new Slogd_Renderer($ep);
if($postlist->render_checkins()){
    $items = $postlist->get_output();
    $c = $postlist->get_count();
}

$locations = $items['locations'];
$posts = $items['vague'];
$events = $items['events'];
$checkins = $items['specific'];

$j = 0;

$currently = reset($posts);
if(isset($_GET['plain'])){
  echo "Amy ".$locations[$currently['location']]['present']." (as of ".$currently['duration']." ago) - rhiaro.co.uk/where";
}else{
$title = "... where?";
include("templates/home_top.php");
?>
<div class="w1of1 align-center where-header clearfix <?=$locations[$currently['location']]['slug']?>">
  <div class="key">
    <div class="block office" title="Office"><i class="fa fa-laptop fa-2x"></i></div>
    <div class="block home" title="Home"><i class="fa fa-home fa-2x"></i></div>
    <div class="block volunteer" title="Helping out with something"><i class="fa fa-gift fa-2x"></i></div>
    <div class="block event" title="At an event"><i class="fa fa-calendar fa-2x"></i></div>
    <div class="block food" title="Out for food"><i class="fa fa-cutlery fa-2x"></i></div>
    <div class="block exercise" title="Exercising"><i class="fa fa-heartbeat fa-2x"></i></div>
    <div class="block meeting" title="In a meeting"><i class="fa fa-pencil-square-o fa-2x"></i></div>
    <div class="block seminar" title="Talk, class or seminar"><i class="fa fa-graduation-cap fa-2x"></i></div>
    <div class="block transit" title="In transit"><i class="fa fa-spinner fa-2x"></i></div>
    <div class="block other" title="On an adventure"><i class="fa fa-question-circle fa-2x"></i></div>
  </div>
  
  <h2 class="lighter">rhiaro <?=$locations[$currently['location']]['present']?> (as of <?=$currently['duration']?> ago)</h2>
</div>
<div class="where-wrapper clearfix">
  <?foreach($posts as $uri => $post):?>
    <?if($j < 2520):?>
      <? $title = $post['published']->format("D jS M")." ".$locations[$post['location']]['present'].", from ".$post['published']->format("H:i")." to ";
         $post['published']->add(new DateInterval("PT".round($post['d'])."M"));
         $title .= $post['published']->format("H:i T");
      ?>
      <?for($i=0;$i<$post['d']/16; $i++):?>
        <div style="width: 16px; height: 16px; display: inline-block" class="<?=$locations[$post['location']]['slug']?>" title="<?=$title?>" id="#<?=str_replace("http://blog.rhiaro.co.uk/", "", $uri)?>"></div>
      <?endfor?>
      <? $j++; ?>
    <?endif?>
  <?endforeach?>

</div>
<?

include("templates/end.php");
}
?>