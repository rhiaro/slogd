<article class="h-entry" prefix="this: http://rhiaro.co.uk/pub/socm15/">
  <h1 class="p-name" property="dcterms:title">Social Personal Data Stores: the Nuclei of Decentralised Social Machines</h2>
  <div id="authors">
      <dl id="author-name">
          <dt>Authors</dt>
          <dd id="author-1"><span about="[this:]" rel="dcterms:creator dcterms:publisher dcterms:contributor"><a about="http://rhiaro.co.uk/about#me" typeof="foaf:Person" rel="foaf:homepage" property="foaf:name" href="http://rhiaro.co.uk">Amy Guy</a></span><sup><a href="#author-org-1">1</a></sup><sup><a href="#author-email-1">*</a></sup></dd>
      </dl>

      <ul id="author-org" class="plist">
          <li id="author-org-1"><sup>1</sup><a about="[dbr:University_of_Edinburgh]" typeof="foaf:Organization" property="foaf:name" rel="foaf:homepage" href="http://inf.ed.ac.uk/">School of Informatics</a>, University of Edinburgh, UK</li>
      </ul>

      <ul id="author-email" class="plist">
        <li id="author-email-1"><sup>*</sup><a about="http://rhiaro.co.uk/about#me" rel="foaf:mbox" href="mailto:Amy.Guy@ed.ac.uk">Amy.Guy@ed.ac.uk</a></li>
      </ul>
      <span about="http://rhiaro.co.uk/about#me" rel="org:memberOf" resource="[dbr:University_of_Edinburgh]"></span>
  </div>
  
  <dl id="document-identifier">
      <dt>Document ID</dt>
      <dd><a href="http://rhiaro.co.uk/pub/socm15">http://rhiaro.co.uk/pub/socm15</a></dd>
  </dl>

  <dl id="document-published">
      <dt>Published</dt>
      
  </dl>
  
  <dl id="document-appeared">
      <dt>Appeared In</dt>
      
  </dl>

  <div id="content" class="e-content">
    <section id="abstract" about="[this:]">
      <h2>Abstract</h2>
      <div property="dcterms:abstract" class="p-summary">
      <p>Coming soon...</p>
      </div>
    </section>
  
  </div>
</article>