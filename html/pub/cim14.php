<article class="h-entry" prefix="this: http://rhiaro.co.uk/pub/cim14/">
  <h1 class="p-name" property="dcterms:title">Roles &amp; Relationships as Context-Aware Properties</h1>
  <div id="authors">
      <dl id="author-name">
          <dt>Authors</dt>
          <dd id="author-1"><span about="[this:]" rel="dcterms:creator dcterms:publisher dcterms:contributor"><a about="http://rhiaro.co.uk/about#me" typeof="foaf:Person" rel="foaf:homepage" property="foaf:name" href="http://rhiaro.co.uk">Amy Guy</a></span><sup><a href="#author-org-1">1</a></sup><sup><a href="#author-email-1">*</a></sup></dd>
      </dl>

      <ul id="author-org" class="plist">
          <li id="author-org-1"><sup>1</sup><a about="[dbr:University_of_Edinburgh]" typeof="foaf:Organization" property="foaf:name" rel="foaf:homepage" href="http://inf.ed.ac.uk/">School of Informatics</a>, University of Edinburgh, UK</li>
      </ul>

      <ul id="author-email" class="plist">
        <li id="author-email-1"><sup>*</sup><a about="http://rhiaro.co.uk/about#me" rel="foaf:mbox" href="mailto:Amy.Guy@ed.ac.uk">Amy.Guy@ed.ac.uk</a></li>
      </ul>
      <span about="http://rhiaro.co.uk/about#me" rel="org:memberOf" resource="[dbr:University_of_Edinburgh]"></span>
  </div>
  
  <dl id="document-identifier">
      <dt>Document ID</dt>
      <dd><a href="http://rhiaro.co.uk/pub/cim14">http://rhiaro.co.uk/pub/cim14</a></dd>
  </dl>

  <dl id="document-published">
      <dt>Published</dt>
      <dd><time datetime="2014-10-19" property="dcterms:issued" content="2014-10-19T00:00:00Z" datatype="xsd:dateTime">2014-10-19</time></dd>
  </dl>
  
  <dl id="document-appeared">
      <dt>Appeared In</dt>
      <dd about="[this:]" rel="bibo:citedBy" resource="[http://www.macs.hw.ac.uk/~fm206/cim14/]">
          <span about="[http://www.macs.hw.ac.uk/~fm206/cim14/]" typeof="bibo:Document">
              <span property="bibo:shortTitle">CIM'14</span> (<a rel="foaf:page" href="http://www.macs.hw.ac.uk/~fm206/cim14/" property="dcterms:title">Context, Interpretation and Meaning workshop</a> at the International Semantic Web Conference),
              <span property="bibo:issue" xml:lang="">
                  <span property="dcterms:issued" xml:lang="">2014</span>
              </span>
          </span>
      </dd>
  </dl>

  <div id="content" class="e-content">
    <section id="abstract" about="[this:]">
      <h2>Abstract</h2>
      <div property="dcterms:abstract" class="p-summary">
      <p>Many concepts in ontologies are ambiguous and have multiple valid interpretations depending on the circumstances in which they are to be interpreted. This paper suggests that rather than relying on the annotation given at the time of data publication, determining the meaning of data can take place at an application level at the time of data use, when additional information about the circumstances of its use is available. Creative media production on the web is used as a real-world scenario through which to explore how ambiguity of concepts might be utilised -- rather than overcome -- for the benefit of the end users of the data.</p>
      </div>
    </section>
  <section id="introduction" rel="dcterms:hasPart" resource="[this:#introduction]">
    <h2 about="[this:#introduction]" property="dcterms:title">Introduction</h2>
    <div about="[this:#introduction]" property="dcterms:description" typeof="deo:Introduction">
      <p>Ambiguity is inherent and unavoidable when things are described in natural language. A goal of the semantic web is to enable global and unambiguous reference to anything [<a href="#ref-hayes07">hayes07</a>]. To this end, concepts are assigned unique identifiers (URIs) and information is attached that will allow an agent (human or machine) to <em>understand</em> what that concept <em>is</em> (how it can be used, what properties it has). This data is structured through the use of ontologies: sets of terms that are pre-decided and (supposedly) documented, which one can refer to in order to learn more about a particular entity and its relationships with others.</p>
      <p>It is these relationships, captured by concepts which belong to the class <code>rdf:Property</code> and which occupy the predicate position of a triple, on which this discussion is focused.</p>
      <p>Ontology terms, including properties, are concepts with assigned URIs and natural language descriptions. Ontologies often also include rules about how the terms can and cannot be used. These rules (such as whether a relationship is by default symmetric) can allow agents to infer further information that was not otherwise explicit in a dataset --- bringing to light many exciting possibilities when globally linking data from different sources and domains. But in practice these rules are not enforced, only used for inference, so concepts are readily 'misused', or used in a way other than their original creator intended, and in turn incorrect inferences may be made. Reuse of existing ontology terms is promoted as best practice [<a href="#ref-bizer07">bizer07</a>], which is crucial for effectively expanding the potential of global linked data, but compounds the problem of misuse of terms; someone may be persuaded to use a term that they consider 'close enough' rather than coin a more suitable alternative themselves [<a href="#ref-halpin10">halpin10</a>]. And if they do coin their own term, there are other properties (like <code>owl:equivalentProperty</code>) that they could potentially use --- or misuse --- to map their own vocabulary to another.</p>
      <section id="interpreting-owlsameas" rel="dcterms:hasPart" about="[this:#introduction]" resource="[this:#interpreting-owlsameas]">
        <h3 about="[this:#interpreting-owlsameas]" property="dcterms:title">Interpreting <code>owl:sameAs</code> </h3>
        <div>
          <p>This potential misuse of properties is well demonstrated in [<a href="#ref-halpin10">halpin10</a>]: different possible interpretations of <code>owl:sameAs</code>, are described and examples of their use in existing linked data is given. As further evidence of this, other significant publishers of ontologies have coined their own versions of <code>sameAs</code> with definitions that vary slightly from that of OWL.</p>
          <p><strong>OWL</strong> <code>sameAs</code>: <em>&quot;...indicates that two URI references actually refer to the same thing: the individuals have the same 'identity'.&quot;</em> [<a href="#ref-bechofer04">bechofer04</a>]</p>
          <p><strong>Schema.org</strong> <code>sameAs</code>: <em>&quot;URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.&quot;</em> [<a href="#ref-schema14">schema14</a>]</p>
          <p><strong>BBC Core</strong> <code>sameAs</code>: <em>&quot;Indicates that something is the same as something else, but in a way that is slightly weaker than <code>owl:sameAs</code>. Its purpose is to connect separate identities of the same thing, whilst keeping separation between the original statements of each.&quot;</em><a href="#fn1" class="footnoteRef" id="fnref1"><sup>1</sup></a> [<a href="#ref-bbcld">bbc12</a>]</p>
          <p>Whilst this is better than simply reusing <code>owl:sameAs</code> in contexts where the original definition doesn't really fit, if domain-specific re-definition of terms in this way becomes widespread, datasets will become less compatible with each other, revert back to an era of dataset-specific schemas, and lose the core advantages that linked data had in the first place.</p>
          <p>The purpose of <code>owl:sameAs</code> is to unify multiple URIs which refer to the same thing, so much of the discussion is around what a URI truly does refer to in order to judge whether two connected URIs really <em>are</em> the same thing [<a href="#ref-halpin10">halpin10</a>]. One possibility is that in certain contexts they <em>may as well</em> refer to the same thing; if I'm explaining where I live, Edinburgh (the city centre) and Edinburgh Central (the Scottish Parliament constituency) are near enough the same --- their geographic boundaries are similar, which is what matters regarding where my home is --- but if I want to talk about the Scottish independence referendum, the other properties each holds means it becomes important to distinguish them.</p>
          <p>Allowing use of data in this way doesn't seem very rigorous but, specialist communities aside, data is rarely useful to human beings in its raw form. At a minimum, there is a service transforming the data into something visually appealing. Often, a software application with a particular purpose, is there to process the data and give some useful output to a human at the other end, as shown in <a href="#fig-app">fig 1</a>.</p>
          <div class="figure">
            <p id="fig-app"><img src="/figs/cim14_fig_dataappuser.png" /></p><p class="caption">Fig 1: Users typically interact with data through an application.</p>
          </div>
          <p>Imagine that the application is a tool that allows me to share a map of all the places I have lived with my friends. For the application to receive as input the URI for <em>Edinburgh (the city centre)</em> or <em>Edinburgh Central (the Scottish Parliament constituency)</em> would make no difference to the output, as the resulting map would have the same area highlighted. The properties of each which are relevant for the task (geographical coordinates) mean that the application's interpretation of <code>owl:sameAs</code> can be looser.</p>
          <p>Instead, imagine that the application is educational, providing historical summaries of particular areas. The difference between the two URIs in this case would be the difference between learning about local residents or about politics. Different properties are relevant, and the interpretation of <code>owl:sameAs</code> must be stricter.</p>
          <p>I wish to shift the discussion from <code>owl:sameAs</code> (and thus the meaning of URIs) and instead consider that <em>all</em> properties may have alternative and equally valid interpretations, depending upon the circumstances in which they are used. A further example is given by [<a href="#ref-pohl14">pohl14</a>], in which different institutions might use the same set of properties in subtly different ways: different library catalogues use <code>dcterms:alternative</code> to mean different things. This indicates that who the data is published by could be critical to interpreting it. In this scenario, an application using library catalogue data from various sources might use the publication context of the data to better understand what to expect of the objects of triples containing <code>dcterms:alternative</code>.</p>
          <p>Determining the meaning of a statement described with linked data currently happens at the lower levels of the Semantic Web stack (<a href="#fig-sw">fig 2</a>) [<a href="#ref-timbl00">timbl00</a>] (<em>Identifiers</em>, <em>Syntax</em>, <em>Data interchange</em>, <em>Ontologies</em>, <em>Rules</em>), but moving this up to <em>User interface and applications</em> emphasises the importance of the end use of data, as well as providing scope for the layers in between (such as <em>Trust</em>) to come into play.</p>
          <div class="figure">
            <p id="fig-sw"><img src="/figs/cim14_fig_swstack.png" alt="The Semantic Web stack." /></p><p class="caption">Fig 2: The Semantic Web stack.</p>
          </div>
        </div>
      </section>
    </div>
  </section>
  
  <section id="context">
    <h2>Context</h2>
    <div>
      <p>I suggest that a concept of type <code>rdf:Property</code> may have a different description, set of properties of its own, or rules for its use, depending on the context in which it is to be used. What do I mean by context?</p>
      <p>When considering how data is to be used in a practical sense, consider that there is some entity being <em>observed</em> by some entity which is an <em>observer</em>, with an <em>application</em> as the facilitator. Assume that there is data about the observed entity, which is what the observer uses to make its observations. Depending on the nature of the observed entity, there may be public and private data about it, where private data is only accessible by the entity itself or an agent whom the entity has authorised; this makes the most sense if the entity is an agent (such as a person) itself. The observed entity optionally exists in a <em>domain</em>, about which there is further data. One may also find data about the application itself: its purpose, when and by whom it was created. Similarly, provenance data about the original publication of the observed entity data may be available for the application to take into account. The observer entity, an agent, human or otherwise, is receiving the output of the application. Thus, there may also be public data about the observer, as well as private data, should the observer choose to share this with the application. See <a href="#fig-appcontext">fig 3</a>.</p>
      <p>Using these terms, <em>context</em> is defined as the data available to the application which is facilitating the observation.</p>
      <div class="figure">
        <p id="fig-appcontext"><img src="/figs/cim14_fig_appcontext.png" /></p><p class="caption">Fig 3: Context provided by the various different available data.</p>
      </div>
      <p>This means that the application takes into account <em>all available data</em> in order to interpret the properties of the observed entity. Next a real use case for this functionality is considered, followed by a discussion of where an application might actually find various interpretations of a property and how it should determine what to return to the end user as a result.</p>
    </div>
  </section>
  <section id="use-case-web-content-creation">
    <h2>Use case: Web content creation</h2>
    <div>
      <p>Significant quantities of multimedia content on the Web are creative works generated by proactive users of content-host and social networking sites. These content creators are seeking to promote their work to a global audience and extend their reach in a variety of ways and for a variety of reasons, and in the process are participating in complex socio-technical networks [<a href="#ref-guy14">guy14</a>]. These networks are situated within and across a diverse range of platforms and communities. Most social media and content-host platforms are silos and do not interoperate well, but users make <em>ad hoc</em> efforts to connect up their various profiles and contributions, so it is possible to abstract away from the capabilities of specific platforms and consider the networks of people and content that are formed across the web as a whole.</p>
      <p>This section describes some specific relationships between content creators, content consumers, and the content itself, discusses how they can be described with linked data, and uses this as a basis to argue for context-dependent interpretations of the relationships.</p>
      
      <section id="user-generated-content-on-the-web">
        <h3>User-generated content on the Web</h3>
        <div>
          <p>Content creators have many different ways of connecting themselves to the media they create. For some, detailed attribution is important; others wish their contributions to be related to specific pseudonyms; some are explicitly anonymous, and yet others couldn't care less so long as their content is out there. From the other side, audience members have varying levels of interest in who created the content they are consuming, and the details of the creators' roles. In addition, how credit is given, particularly for collaborative projects, is often heavily mediated by the site which hosts the content. For example, a video published on one YouTUbe channel may contain contributions from several creators, each with their own channels, but YouTUbe does not provide a facility to automatically connect them. It may alternatively depend on the communities in which the content is shared; for example, derivative works on the Scratch site are automatically linked to the original, but users prefer non-automated means of giving credit, and users of the site can be dissatisfied with restrictions in place [<a href="#ref-luther10">luther10</a>, <a href="#ref-monroy11">monroy11</a>].</p>
          <p>So of course creators work to circumvent the restrictions of the systems they use to add credits to their work as best they can, or so far as they care to. They also distribute their attribution across different platforms, including to websites or social networking sites completely separate from where the content itself is hosted, with only human-readable descriptions or common-sense associations to join things up.</p>
          <p>Similarly, the relationships of content creators with each other (and their personas) are nuanced and complex, and describing them is constrained by ambiguous, one-dimensional notions of <em>Facebook friends</em>, <em>Twitter followers</em>, <em>YouTube subscribers</em>, and so on.</p>
        </div>
      </section>
      <section id="user-generated-content-on-the-semantic-web">
        <h3>User-generated content on the Semantic Web</h3>
        <div>
          <p>We can link this disparate data together by using the APIs of the content host and social network sites, and by scraping the more unstructured information from the free text fields often (ab)used by content creators, but there is only so much semantic information we can extract.</p>
          <p>A straightforward way to associate a content creator with content, using a YouTube video as an example, is shown in <a href="#fig-annotated">fig 4</a>. What is available from the YouTube API is merely the channel ID associated with a particular video. To abstract away from YouTube (the same relationships would apply were the video published on Vimeo or DailyMotion, for example) we have chosen the generic Dublin Core <code>creator</code> property<a href="#fn2" class="footnoteRef" id="fnref2"><sup>2</sup></a> to link them here. Any other ontology that deals with creative works would contain suitable properties, likely at different levels of granularity (more domain specific terms might include <code>director</code> or <code>animator</code>).</p>
          <div class="figure">
          <p id="fig-annotated"><img src="/figs/cim14_fig_annotatedcontent.png" /></p><p class="caption">Fig 4: A simple way to connect a content creator with content created using RDF and existing ontologies. </p>
          </div>
          <p>Similarly we can determine <code>foaf:knows</code> associations between content creators by extracting relationships from the various methods they use to connect with each other.</p>
          <p>But <code>dc:creator</code> and <code>foaf:knows</code> are potentially subject to interpretation. Rather than insist upon the creation and use of additional, perhaps more rigorously specified, predicates for expressing these relations, for reasons mentioned in section , I posit that applications that use the data should have the ability to determine alternative interpretations, and make use of the one that will give the most value to the application user in that particular moment.</p>
        </div>
      </section>
      <section id="contexts-for-user-generated-content">
        <h3>Contexts for user-generated content</h3>
        <div>
          <p>For this scenario, I introduce Paul, an animator with some presence on YouTube; his girlfriend Sabrina; Tom, a YouTuber with a significant presence; and Sky, a non-content-creator and fan of Paul's work.</p>
          <p>Assume there is an RDF dataset that has been generated about Paul's work.<a href="#fn3" class="footnoteRef" id="fnref3"><sup>3</sup></a> It might include the following triples:</p>
          <pre><code>paul:Paul#id
              a foaf:Person ;
              foaf:account &lt;http://youtube.com/PaulterVoorde&gt; ;
              foaf:account &lt;http://twitter.com/paultervoorde&gt; ;
              foaf:homepage &lt;http://paulswebsite.asdf&gt; .
          
          paul:SkeffsSaturday
              a ir:InformationObject ;
              ir:isRealizedbr:University_of_Edinburghy &lt;http://www.youtube.com/v/Hsjm5tNZBdA&gt; ;
              dc:creator paul:Paul#id ;
              dc:title &quot;Skeff&#39;s Saturday&quot; ;
              dc:published &quot;23rd June 2014&quot; ;
              dc:description &quot;The world famous Skeff returns to the big screen. Enjoy.&quot; .</code></pre>
          <p>Sabrina and Paul keep in touch using an app that stores their data and messages in vcard format, eg:</p>
          <pre><code>contact:Sabrina#id vcard:has_related contact:Paul .
          
          contact:Paul
              vcard:has_url &lt;http://paulswebsite.asdf&gt; ;
              vcard:has_telephone &quot;############&quot; .
          
          contact:message6495
              contact:to contact:Paul ;
              contact:subject &quot;Hello&quot; ;
              contact:contents &quot;Where are you today?&quot; .</code></pre>
          <p>Tom, a very organised individual, uses a things-to-do app which stores his input in RDF, eg:</p>
          <pre><code>todo:project
              dc:title &quot;New Animation&quot; ;
              dc:description &quot;Produce an animation of that script I wrote.&quot;
              todo:deadline &quot;2nd August 2014&quot; ;
              todo:hasTask todo:task1 ;
              todo:hasTask todo:task2 .
          
          todo:task1
              dc:title &quot;Recruit animator&quot; ;
              dc:description &quot;Find someone who will work for £100/minute of animation with a clean animation style.&quot;
              todo:status todo:Doing .
          
          todo:task2
              dc:title &quot;Recruit voice actor&quot; ;
              dc:description &quot;Someone squeaky for main character.&quot; ;
              todo:status todo:Done .
          </code></pre>
          <p>Sky uses a bookmarklet to save her favourite media from all over the Web; conveniently, this stores her data as RDF as well:<a href="#fn4" class="footnoteRef" id="fnref4"><sup>4</sup></a></p>
          <pre><code>fav:fav495
              a fav:Favourite ;
              dc:date &quot;25th June 2014&quot; ;
              fav:rating fav:Five ;
              fav:url &lt;http://www.youtube.com/v/Hsjm5tNZBdA&gt; ;
              dc:description &quot;Best Skeff animation lol!!&quot; .</code></pre>
          <p>We can already get the idea from the data held about each individual that Sabrina is close to Paul, Tom doesn't know Paul, but is looking to hire someone with Paul's skills, and Sky is a big fan of Paul. Augmenting this with data from social media would confirm this: Paul and Sabrina are mutual connections on every network they're signed up to; Sky is subscribed to Paul's updates on various sites but Paul does not reciprocate; they all post updates about what they're doing or interested in, and who they're with. Sabrina, Tom and Sky all clearly have different relationships with Paul.</p>
          <p>As such, Paul's <code>dc:creator</code> relationship with his animation &quot;Skeff's Saturday&quot; holds different meaning to each of the three.</p>
          <p>Sabrina might know that Paul laboured for hours over the drawings, and that it is the artistic work he is most proud of. She might also know that someone else helped Paul to write the script, and actually did most of the work in that regard, and yet another person provided the background music. Perhaps <code>creator</code> to her really means <code>artist</code>.</p>
          <p>Tom understands the online animation industry, and is aware that longer works are typically a collaborative effort, with a lead animator --- the one who has uploaded the work --- working on the main art and other animators help out with in-between frames and background art. To him, <code>creator</code> means <code>leadAnimator</code> and <code>projectManager</code>.</p>
          <p>Sky likes cartoons. Paul is her favourite maker of cartoons. <code>creator</code> to Sky means <code>entertainer</code>.</p>
          <p>Sabrina, Tom and Sky each use an application to view Paul's profile and portfolio of work. The application can access all public information about Paul: the linked data he has published himself, and his various online profiles, and collate this information for display to the user. In addition, imagine that each user can authorise the application to access their private data too --- data stored for them by the other applications they use. This additional data can help the application to understand that Sabrina is viewing Paul's profile with a very personal perspective; Tom has a professional view; Sky is a member of Paul's audience. It is then up to the application to decide how best to cater for each of these different potential interpretations of Paul's connection with his work.</p>
          <p>In the next section, I discuss how such an application might be created.</p>
        </div>
      </section>
    </div>
  </section>
  
  <section id="architecture-for-decentralised-interpretation">
    <h2>Architecture for decentralised interpretation</h2>
    <div>
      <p>Whilst Semantic Web ontologies are decentralised in that anyone can create their own to model a particular domain, from the perspective of an agent exploring the domain being modeled, the terms used to describe things are arbitrary and centrally controlled.</p>
      <p>When some concept is annotated, we currently assume that a particular property was used with a specific meaning in mind, and strive to disambiguate this meaning for all observers of the property. Instead, I propose that an annotator ought to be able to use some generic or potentially ambiguous property at the time of annotation, and rely on the ability of an application that consumes the data to interpret the property in the most appropriate way for the application's end user. The application does this through its access to any available data about the application user, the data being consumed, and the domain.</p>
      <p>Such alternative interpretations could involve different levels of granularity of information for different readers; additional metadata such as temporal or provenance information; different terminology for describing a property, different rules for its use that can influence inference. It is the application which is responsible for deciding the potential interpretations of a property according the the end user's need at the time.</p>
      <div class="figure">
      <p id="fig-process"><img src="/figs/cim14_fig_appprocess.png" /></p>
      <p class="caption">Fig 5: The process by which an application might determine its output using context-dependent data.</p>
      </div>
      <p>To create an application with this capability, a developer might carry out a process like the following:<a href="#fn5" class="footnoteRef" id="fnref5"><sup>5</sup></a></p>
      <ol style="list-style-type: decimal">
      <li>Specify the <em>purpose</em> of the application.</li>
      <li>Determine what external data would be required to carry out this purpose.</li>
      <li>Consider how the external data is annotated and which ontologies are used.</li>
      <li>Consider the different types of potential users and the situations they would be in.</li>
      <li>Create data about the relevant external vocabulary terms to describe potential alternative interpretations.</li>
      <li>Create the application, including the logic necessary to determine how the application decides to what to output based on the data that makes up a user's context.</li>
      </ol>
      <p>Of course, the original creator(s) of an ontology for a domain, or the publishers of a dataset, can facilitate this process by publishing possible interpretations as standard. An application can find these at runtime, and it would be considered good practice for applications with bundled 'interpretation heuristics' to openly publish and link up their own data as well. [<a href="#ref-pohl14">pohl14</a>] suggests a JSON-LD syntax for Dublin Core Application Profiles [<a href="#ref-heery00">heery00</a>] as a means for libraries to publish the particulars of their chosen terms, demonstrating the precedent for data providers enriching their datasets with contextual information. As more applications with specific purposes emerge within a particular domain, more interpretations for properties commonly used in that domain will become available, without increasing the number of ontology terms needed to describe it.</p>
    </div>
  </section>
  <section id="conclusion-and-future-work">
    <h2>Conclusion and future work</h2>
    <div>
      <p>Unavoidable ambiguity in ontology creation means that a single property may have different interpretations in different contexts. Taking advantage of this ambiguity to personalise end-user experience can be done through detecting additional available data to determine the context of the user. I have described a possible method for achieving this when building applications that consume linked data, but a lot is left to the imagination.</p>
      <p>An obvious next step is to determine a minimal format, most likely RDF-based, for describing context-dependent interpretations of a property. One option is to use named graphs to store different rules and labels associated with a property; an application would then contain the heuristics to decide which named graph to choose when parsing the data.</p>
      <p>Although, as previously mentioned, the ideal situation would involve application developers, ontology creators and data publishers publishing new property interpretations as new contexts arose, in practice this may be difficult to achieve on a large enough scale. Perhaps some movement towards 'Linked Open Contexts' to accompany the Linked Open Data movement would help.</p>
      <p>Ultimately we would like to see serious discussion around how ambiguity of ontology terms can be processed by Semantic Web applications and used to enhance the experience of a data consumer, as an alternative to attempting to disambiguate terms at the data level.</p>
    </div>
  </section>
  <section id="references">
    <h2>References</h2>
    <div>
      <ol>
        <li id="ref-bbcld">BBC Linked Data: <a href="http://www.bbc.co.uk/ontologies/coreconcepts">Core Concepts Ontology Version 1.6</a>. (2012) [Accessed 15th July 2014]</li>
        <li id="ref-bechofer04">Bechofer, S., Van Harmelen, F., Hendler, J., Horrocks, I., Mcguiness, D., Schneider, P. and Stein, L.: OWL Web Ontology Language Reference. (2004)</li>
        <li id="ref-timbl00">Berners-Lee, T.: <a href="http://www.w3.org/2000/Talks/1206-xml2k-tbl/">Slide 10, Semantic Web - XML2000</a> (2000) [Accessed 11th August 2014]</li>
        <li id="ref-bizer07">Bizer, C., Cyganiak, R. and Heath, T.: How to publish Linked Data on the Web. (2007)</li>
        <li id="ref-luther10">Luther, K.: Edits &amp; Credits: Exploring Integration and Attribution in Online Creative Collaboration. In CHI, pages 2823-2832 (2010)</li>
        <li id="ref-monroy11">Monroy-Hernandez, A., Mako Hill, B., Gonzalez-Rivero, J. and boyd, d.: Computers can't give credit: How automatic attribution falls short in an online remixing community. In CHI, Vancouver. ACM. (2011)</li>
        <li id="ref-guy14">Guy, A. and Klein, E.: Constructed Identity and Social Machines: A Case Study in Creative Media Production. WWW'14 Companion. (2014)</li>
        <li id="ref-halpin10">Halpin, H. Hayes, P. J., McCusker, J. P., McGuinness, D. L., and Thompson, H. S.: When owl:sameAs isn't the same: An analysis of identity in Linked Data. In International Semantic Web Conference (1), 305--320 (2010)</li>
        <li id="ref-hayes07">Hayes, P. and Halpin, H.: In Defense of Ambiguity. In Workshop on Identity, Identifiers and Identification, World Wide Web Conference. (2007)</li>
        <li id="ref-heery00">Heery, R. and Patel, M.: Application Profiles: mixing and matching metadata schemas. In Ariadne, issue 25. (2000)</li>
        <li id="ref-pohl14">Pohl, A.: <a href="https://wiki1.hbz-nrw.de/display/SEM/2013/08/01/Sharing+context+-+publishing+application+profiles+with+JSON-LD">Sharing context - publishing application profiles with JSON-LD</a>. Wiki des Hochschulbibliothekszentrums des Landes Nordrhein-Westfalen. (2014) [Accessed 18th July 2014]</li>
        <li id="ref-schema14">Schema.org: <a href="https://schema.org/sameAs">SameAs</a>. Schema Version 1.8. [Accessed 15th July 2014] (2014)</li>
      </ol>
    </div>
  </section>
  <div class="footnotes">
  <hr />
  <ol>
  <li id="fn1"><p>In other words: <code>bbc:sameAs owl:differentFrom owl:sameAs</code>. Imagine if the BBC defined their own <code>differentFrom</code> as well.<a href="#fnref1">↩</a></p></li>
  <li id="fn2"><p>http://dublincore.org/documents/2012/06/14/dcmi-terms<a href="#fnref2">↩</a></p></li>
  <li id="fn3"><p>Perhaps Paul is a linked data enthusiast in his spare time, and has annotated all of his works to expose an RDF representation of his own portfolio.<a href="#fnref3">↩</a></p></li>
  <li id="fn4"><p>The <em>Unhosted</em> (http://unhosted.org) community can provide many examples of in-use Web apps that do all of these things, storing both private and public data in the cloud using <em>remoteStorage</em> (http://remotestorage.io), which uses JSON-LD by default.<a href="#fnref4">↩</a></p></li>
  <li id="fn5"><p>A thoughtful application developer should be doing steps 1, 2, 4 and 6 anyway, so this doesn't add as much overhead to the development process as one might think.<a href="#fnref5">↩</a></p></li>
  </ol>
  </div>
  </div>
</article>