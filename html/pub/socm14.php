<article class="h-entry" prefix="this: http://rhiaro.co.uk/pub/socm14/">
  <h1 class="p-name" property="dcterms:title">Constructed Identity and Social Machines: A Case Study in Creative Media Production</h2>
  <div id="authors">
      <dl id="author-name">
          <dt>Authors</dt>
          <dd id="author-1"><span about="[this:]" rel="dcterms:creator dcterms:publisher dcterms:contributor"><a about="http://rhiaro.co.uk/about#me" typeof="foaf:Person" rel="foaf:homepage" property="foaf:name" href="http://rhiaro.co.uk">Amy Guy</a></span><sup><a href="#author-org-1">1</a></sup><sup><a href="#author-email-1">*</a></sup></dd>
          <dd id="author-2"><span about="[this:]" rel="dcterms:creator dcterms:publisher dcterms:contributor"><a about="http://homepages.inf.ed.ac.uk/ewan/#" typeof="foaf:Person" rel="foaf:homepage" property="foaf:name" href="http://homepages.inf.ed.ac.uk/ewan/">Ewan Klein</a></span><sup><a href="#author-org-1">1</a></sup><sup><a href="#author-email-2">^</a></sup></dd>
      </dl>

      <ul id="author-org" class="plist">
          <li id="author-org-1"><sup>1</sup><a about="[dbr:University_of_Edinburgh]" typeof="foaf:Organization" property="foaf:name" rel="foaf:homepage" href="http://inf.ed.ac.uk/">School of Informatics</a>, University of Edinburgh, UK</li>
      </ul>

      <ul id="author-email" class="plist">
        <li id="author-email-1"><sup>*</sup><a about="http://rhiaro.co.uk/about#me" rel="foaf:mbox" href="mailto:Amy.Guy@ed.ac.uk">Amy.Guy@ed.ac.uk</a></li>
        <li id="author-email-2"><sup>^</sup><a about="http://homepages.inf.ed.ac.uk/ewan/#" rel="foaf:mbox" href="mailto:ewan@inf.ed.ac.uk">ewan@inf.ed.ac.uk</a></li>
      </ul>
      <span about="http://rhiaro.co.uk/about#me" rel="org:memberOf" resource="[dbr:University_of_Edinburgh]"></span>
  </div>
  
  <dl id="document-identifier">
      <dt>Document ID</dt>
      <dd><a href="http://rhiaro.co.uk/pub/cim14">http://rhiaro.co.uk/pub/socm14</a></dd>
  </dl>

  <dl id="document-published">
      <dt>Published</dt>
      <dd><time datetime="2014-04-07" property="dcterms:issued" content="2014-04-07T00:00:00Z" datatype="xsd:dateTime">2014-04-07</time></dd>
  </dl>
  
  <dl id="document-appeared">
      <dt>Appeared In</dt>
      <dd about="[this:]" rel="bibo:citedBy" resource="[http://sociam.org/socm2014/]">
          <span about="[http://sociam.org/socm2014/]" typeof="bibo:Document">
              <span property="bibo:shortTitle">SOCM'14</span> (<a rel="foaf:page" href="http://sociam.org/socm2014/" property="dcterms:title">The 2nd International Workshop on the Theory and Practice of Social Machines</a> at <a href="http://www2014.org/">WWW 2014</a>),
              <span property="bibo:issue" xml:lang="">
                  <span property="dcterms:issued" xml:lang="">2014</span>
              </span>
          </span>
      </dd>
  </dl>

  <div id="content" class="e-content">
    <section id="abstract" about="[this:]">
      <h2>Abstract</h2>
      <div property="dcterms:abstract" class="p-summary">
      <p>Coming soon...</p>
      </div>
    </section>
  
  </div>
</article>