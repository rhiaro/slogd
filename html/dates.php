<?
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');

if(isset($_GET["y"]) && strlen($_GET["y"]) == 4){
	if(isset($_GET["m"]) && $_GET["m"] > 0 && $_GET["m"] < 13){
	 if(strlen($_GET["m"]) == 1) { $_GET["m"] = "0".$_GET["m"]; }
	 $after = $_GET["y"]."-".$_GET["m"]."-01T00:00:00+00:00";
	 $nextm = $_GET["m"]+1;
	 $prevm = $_GET["m"]-1;
	 if($nextm > 12){ $nextm = 1; $nexty = $_GET["y"] + 1; }else{ $nexty = $_GET["y"]; }
	 if($prevm < 1){ $prevm = 12; $prevy = $_GET["y"] - 1; }else{ $prevy = $_GET["y"]; }
	 if(strlen($nextm) == 1) { $nextm = "0".$nextm; }
	 if(strlen($prevm) == 1) { $prevm = "0".$prevm; }
	 $before = $nexty."-".$nextm."-01T00:00:00+00:00";
	}else{
	  $after = $_GET["y"]."-01-01T00:00:00+00:00";
	  $before = $_GET["y"]."-12-31T23:59:59+00:00";
	  $nexty = $_GET["y"] + 1;
	  $prevy = $_GET["y"] - 1;
	}
	$postlist = new Slogd_Renderer($ep);
	if($postlist->render_expanded_list(array("before"=>$before, "after"=>$after))){
		$items = $postlist->get_output();
		$c = $postlist->get_count();
	}
	
}else{
  header("Location: /");
}

$title = "$c posts from ".$_GET["y"];
if(isset($_GET["m"])){ $title .= "/".$_GET["m"]; }
$listheader = "Posts from ".$_GET["y"];
if(isset($_GET["m"])){ $listheader .= "/".$_GET["m"]; }

include("templates/home_top.php");
?>
<div class="w1of1 color3-bg clearfix">
  <div class="w1of5">
    <? include 'templates/h-card.php'; ?>
  </div>
  <? $trackdate = 0; ?>
  <div class="w4of5 lighter-bg"><div class="inner">
    <h1><?=$listheader?> (<?=$c?>)</h1>
      <? include 'templates/list.php'; ?>
  </div></div>
</div>
<?

include("templates/list_1of2_tags.php");
include("templates/end.php");

?>