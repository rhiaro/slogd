<? $title=": profile"; $h="h-card"; include 'templates/home_top.php'; ?>
<div class="w1of1 clearfix" id="me">
  <div class="w1of3 color3-bg">
      <img src="http://rhiaro.co.uk/stash/dp.png" class="midicon u-photo" />
      <data class="p-name" value="Amy Guy" />
      <data class="p-url" value="http://rhiaro.co.uk/about#me" />
      <ul class="bloblist tagcloud lighter">
        <li><a href="/tag/indieweb" class="p-category">indieweb</a></li>
        <li><a href="/tag/linked+data" class="p-category">linked data</a></li>
        <li><a href="/tag/phd" class="p-category">PhD</a></li>
        <li><a href="/tag/travel" class="p-category">travel</a></li>
        <li><a href="/tag/hacking" class="p-category">hacking</a></li>
        <li><a href="/tag/socialwg" class="p-category">socialwg</a></li>
      </ul>
      <? include("templates/contactbuttons.php"); ?>
  </div>
</div>
<? include 'templates/end.php'; ?>