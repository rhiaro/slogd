<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once('lib/storesetup.php');
require_once('lib/Slogd_Renderer.php');

$postlist = new Slogd_Renderer($ep);
if($postlist->json_dump_activities()){
	$items = $postlist->get_output();
	$c = $postlist->get_count();
}
// When does a result need to be different from the activity? Whend oes a result have properties attached that can't be attached to the activity?
$i = 0;
header('Content-Type: application/activity+json');
?>
{
  "@context": [
    "http://www.w3.org/ns/activitystreams",
    { "me": "http://vocab.amy.so/blog#" }
  ],
  "@type": "Collection",
  "@id": "http://rhiaro.co.uk/activities",
  "totalItems": <?=$c?>,
  "itemsPerPage": <?=$c?>,
  "items": [
    <?foreach($items as $uri => $item):?>
      <?if($uri != "template"):?>{
        "@id": "<?=$uri?>"
        "@type": "<?=$item['type']?>",
        "published": "<?=date(DATE_ATOM, $item['date'])?>",
        "actor": "<?=$item['a_url']?>",
        "object": "<?=$item['object']?>",
      }<? $i++; if($i < $c) echo ","; ?>
      <?endif?>
    <?endforeach?>
  ]
}
