<!---
published: 
last-modified: 17th March 2014
type: BlogPost
list: doing
tags: learning, hacking, triplestore, semantic web, linked data, rdf, triples, apache, marmotta, linked media
-->

# Configuring Apache Marmotta

I finally invested in a virtual cloud server on Digital Ocean. What I have set up so far has 512mb RAM, 20gb SSD Disk and is running 32bit Ubuntu 13.10. http://amy.so points to it. I've installed nginx. But that's barely relevant. Let's talk about triplestores.

I started tinkering with AllegoGraph locally a little while ago, but then I heard about [Apache Marmotta](https://marmotta.apache.org). It looks to be under very active development, is supposedly quite full featured and stable, and you can't go wrong with an Apache project, right? It's also part of the [Linked Media Framework](https://code.google.com/p/lmf/) which is relevant to my interests.

I installed it from the .jar file, and could access the admin interface on port 8080.

I changed the security profile to `standard`, [as recommended](https://marmotta.apache.org/platform/security-module.html).

I tried to change the admin password at `/user/me.html` where it says to, but clicking the Change Password button appears to do nothing. I think, despite being logged in, this form wasn't actually attached to a user. I managed to change it instead through the /users.html interface, that lets you edit all user-related settings.

I created a new user with all of the same settings as the admin one just to see. There's a place to enter some basic FOAF information about the user... It doesn't seem to store this when you click Save though :s

I had brief trouble trying to figure out how to restrict access to the admin interface. [The docs](https://marmotta.apache.org/configuration.html) tell you what configuration rules you need to set to do this, but it isn't clear how to set them. I didn't look far, but couldn't find a file to paste the rules into. The rules are listed individually under Security > Configuration, and here I was able to add new ones one at a time. Clicking 'Add Value' gives you a form with Key, Type, Parameters, Value and Comment. I guessed, and put (eg.) `security.permission.admin_ui.priority` in Key, `5` in Value, and left the rest blank. That worked.

## SPARQL Endpoint

The SPARQL endpoint is at /marmotta/sparql and uses [SNORQL](https://github.com/kurtjx/SNORQL) for the frontend.

I can't figure out how to make it publicly queryable though. That URL actually redirects to a page behind /admin/. Trying it with Python and rdflib yields `Exception: Could not load http://amy.so:8080/marmotta/sparql as either RDF/XML, N3 or NTriples`.

I'll post to the mailing list, and install Virtuoso or Allegro while I'm waiting...
