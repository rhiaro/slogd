<!---
published: 00
last-modified: 1st May 2014, 00:40
type: BlogPost
tags: hacking,learning,semantic web,linked data,json-ld,json,braindump
list: doing
-->

# Squinting blearily at JSON-LD

**Disclaimer: This is probably my inexperience talking.**

TODO: Rewrite this as a Coming at JSON-LD from a LD perspective informative type post. AFTER READING THE SPECS.

I'd actually really like some genuine feedback / answers on these things.

So JSON-LD, right.

I know a fair bit about linked data these days, and not so much about JavaScript. I got a bit excited about JSON-LD recently because I thought it might make hacking together quicky semantic web-y things really easy. But the more I look into it, the more I suspect it can't do..

**Querying**

First I was like: brill! I'll query it with SPARQL! That's my favourite thing to query with!

Nope.

Actually it looks like most of the people who work on JSON-LD aren't a fan of the whole semantic web thing. In my perusings, I saw a subtitle for JSON-LD that was 'linked data' but with 'linked' crossed out and replacd with 'linking'. Trying to increase the distance even more there, I think.

Okay, I suppose it _is_ just a JavaScript object. I'm probably just expected to query it in some longstanding, super-efficient and stable way that already exists.

Uuummm. No? I searched a lot and couldn't find any consensus on the best way to query JSON. Maybe I'm missing something obvious because I don't know much about JavaScript or efficient ways of searching data structures in general. There are a few different libraries with a few different methods, but nothing specifically graph-y, which is what I was really looking out for. On account of linked data being a graph and everything.

**Update:** [SpahQL](http://danski.github.io/spahql/) Looks pretty cool, _and_ it's pronounced the same as SPARQL...

**Contexts**

Just a quick note on how I think things are working. I seem examples with just ```"@context": "http://schema.org/"``` and then a bunch of data properties (```"name":"Bob"``` etc). And I see examples with more detailed contexts, eg.

```
"@context": { "name": "http://schema.org/name", "description": "http://schema.org/description" }
```

What I think is happening here is... Oh sod it, I'll just read [the specs](http://www.w3.org/TR/json-ld/#basic-concepts).

**Linking**

Okay, so ```@context``` turns mere strings into semantically meaningful ontology terms.

But are there any examples of context links being followed to meaningfully link data from different sources? How do you actually do federation?

Wait, is that what' he's actually implemented as a 'proof of concept'?: http://terraces.wordpress.com/2014/04/24/export-and-structure-your-musical-activity-with-schema-org/