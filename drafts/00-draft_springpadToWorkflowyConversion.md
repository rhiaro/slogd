<!---
published: 
last-modified: 15th June 2014, 18:42
type: BlogPost
tags: hacking,learning,notes,python,springpad,workflowy
list: doing
-->

# Springpad to Workflowy converter

I hate hierarchical organisation of things, but I recognise its usefulness in browsing things you've filed. So when I started using Springpad, I was thrilled to find I could put things in multiple notebooks, and take advantage of the heirarchical nature of the UI when viewing notebooks. Best of both worlds!

Following Springpad's sad departure, I'm having a go with Workflowy. It's not a drop-in replacement, and requires a slightly different mindset to your generic notetaking app. It's list-y nature is obviously hierarchical, but tagging things in free-text rather than having somewhere specific to add them really encourages tagging everywhere. I really do feel less constrained with Workflowy; I can type things wherever I want, and as long as I tag things enough (since this is done with a #, it is pretty much second nature, thanks to twitter) it's easy to find them again. Their instant search is really satisfying, although I can't seem to make it do AND searches.

Anyway, I want to dump all of my Springpad data straight into Workflowy.

Workflowy understands [OPML (Outline Processor Markup Language)](http://en.wikipedia.org/wiki/OPML) and Springpad exports JSON. OPML elements can contain any arbitrary attributes, but Workflowy supports ```text```, ```_note``` and ```_complete```. Obviously Springpad notes have many more properties, so we're going to have to find a way to cram all that data into OPML structure. What I've actually done is omit a lot of them (creation and modification dates, public/private, ..) and concatenated the rest (like tags and attachments) with a note's text.

Disclaimer: I by no means used all of Springpad's features. This converter is built around my Springpad export, which contains Notebooks, Notes and Bookmarks. Sprngpad has a bunch more types which I've never used and don't know what they look like. Please do expand the script to handle other types better.

Problem... This resulted in many more items than Workflowy's free limit (250). When trying to paste them, simply nothing happened. Pasting a small subset works fine. 

I guess I'll try Fargo before forking out for Workflowy Pro... just to see...